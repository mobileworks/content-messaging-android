package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.MessageContentModel
import uk.co.symec.domain.model.Result

interface UpdateDeviceInfoUseCase {
    suspend operator fun invoke(deviceId: String, fcm: String): Result<MessageContentModel>
}