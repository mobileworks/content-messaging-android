package uk.co.symec.domain.usecase

import uk.co.symec.domain.BaseUseCase
import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.Result

interface GetMediaUseCase : BaseUseCase<String, MediaModel> {
    override suspend operator fun invoke(mediaId: String): Result<MediaModel>
}