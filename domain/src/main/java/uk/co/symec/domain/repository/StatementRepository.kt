package uk.co.symec.domain.repository

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.StatementModel

interface StatementRepository {
    suspend fun getStatement(
        deviceId: String
    ): Result<StatementModel>
}