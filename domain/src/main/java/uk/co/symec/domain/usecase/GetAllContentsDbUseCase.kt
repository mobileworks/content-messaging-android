package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface GetAllContentsDbUseCase {
    suspend operator fun invoke(downloadFiles: Boolean): Result<List<ContentModel>>
}