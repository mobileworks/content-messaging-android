package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.UpdatedStatusModel
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetUpdatedStatusDbUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetUpdatedStatusDbUseCase {
    override suspend fun invoke(): Result<List<UpdatedStatusModel>> {
        return repository.getUpdatedStatuses()
    }
}