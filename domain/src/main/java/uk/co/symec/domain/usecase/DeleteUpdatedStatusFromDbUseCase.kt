package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result

interface DeleteUpdatedStatusFromDbUseCase {
    suspend operator fun invoke(contentId: String): Result<Boolean>
}