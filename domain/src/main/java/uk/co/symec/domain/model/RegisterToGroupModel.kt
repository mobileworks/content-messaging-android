package uk.co.symec.domain.model

data class RegisterToGroupModel(val token: String?, val groupName: String?, val message: String?)