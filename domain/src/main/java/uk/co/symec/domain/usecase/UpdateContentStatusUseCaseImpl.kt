package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class UpdateContentStatusUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    UpdateContentStatusUseCase {
    override suspend fun invoke(deviceId: String, contentId: String, status: Int): Result<ContentModel> {
        return repository.updateStatusContent(deviceId, contentId, status)
    }
}