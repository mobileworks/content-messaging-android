package uk.co.symec.domain.model

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.BindingAdapter

data class ConfirmationModel(
    val content: String?,
    val userName: String?,
    val signature: String?
) {
    companion object {

        @JvmStatic
        @BindingAdapter("app:isVisibleIcon")
        fun isVisibleIcon(
            view: View,
            boolean: Boolean
        ) {
            if (boolean) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @JvmStatic
        @BindingAdapter("app:isNewIcon")
        fun isNewIcon(
            view: View,
            status: Int
        ) {
            when (status) {
                0 -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:setLayout")
        fun setLayout(
            view: View,
            status: Int
        ) {
            val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
            when (status) {
                0 -> {
                    layoutParams.leftMargin = 16
                    view.layoutParams = layoutParams
                }
                else -> {
                    layoutParams.leftMargin = 0
                    view.layoutParams = layoutParams
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:isShowShare")
        fun isShowShare(
            view: View,
            boolean: Boolean
        ) {
            if (boolean) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @JvmStatic
        @BindingAdapter("app:iconCounter")
        fun getIconCounter(
            view: TextView,
            counter: Int
        ) {
            if (counter > 1) {
                view.visibility = View.VISIBLE
                view.text = counter.toString()
            } else {
                view.visibility = View.GONE
            }
        }

//        @JvmStatic
//        @BindingAdapter("android:layout_width")
//        fun setWidth(view: View, isMatchParent: Boolean) {
//            val params = view.layoutParams
//            params.width = if (isMatchParent) MATCH_PARENT else WRAP_CONTENT
//            view.layoutParams = params
//        }
    }
}