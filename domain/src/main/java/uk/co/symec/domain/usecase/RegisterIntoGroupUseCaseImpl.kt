package uk.co.symec.domain.usecase

import uk.co.symec.domain.repository.RegisterRepository
import javax.inject.Inject

class RegisterIntoGroupUseCaseImpl @Inject constructor(private val repository: RegisterRepository) :
    RegisterIntoGroupUseCase {

    override suspend operator fun invoke(
        assetNo: String,
        fcm: String,
        groupId: String,
        model: String,
        imei: String
    ) = repository.registerIntoGroup(assetNo, fcm, groupId, model, imei)
}