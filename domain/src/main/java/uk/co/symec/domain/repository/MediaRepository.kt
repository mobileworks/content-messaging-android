package uk.co.symec.domain.repository

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.Result

interface MediaRepository {
    suspend fun getMedias(content: ContentModel): Result<Boolean>

    suspend fun getMedia(id: String): Result<MediaModel>

    suspend fun updateSeenAt(id: String): Result<Boolean>

    suspend fun saveMedia(media: MediaModel): Result<String>
}