package uk.co.symec.domain.usecase

import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetContentUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetContentUseCase {
    override suspend operator fun invoke(contentId: String) = repository.getContent(contentId)
}