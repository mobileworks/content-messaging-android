package uk.co.symec.domain.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uk.co.symec.domain.usecase.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {

    @Provides
    @Singleton
    fun provideGetContentUseCase(contentUse: GetContentUseCaseImpl): GetContentUseCase = contentUse

    @Provides
    @Singleton
    fun provideUpdateContentUseCase(contentUse: UpdateContentUseCaseImpl): UpdateContentUseCase = contentUse

    @Provides
    @Singleton
    fun provideGetUpdatedStatusDbUseCase(contentUse: GetUpdatedStatusDbUseCaseImpl): GetUpdatedStatusDbUseCase = contentUse

    @Provides
    @Singleton
    fun provideDeleteContentUseCase(contentUse: DeleteContentUseCaseImpl): DeleteContentUseCase = contentUse

    @Provides
    @Singleton
    fun provideDeleteContentFromDbUseCase(contentUse: DeleteContentFromDbUseCaseImpl): DeleteContentFromDbUseCase = contentUse

    @Provides
    @Singleton
    fun provideDeleteUpdatedStatusFromDbUseCase(contentUse: DeleteUpdatedStatusFromDbUseCaseImpl): DeleteUpdatedStatusFromDbUseCase = contentUse

    @Provides
    @Singleton
    fun provideGetMediaUseCase(mediaUseCase: GetMediaUseCaseImpl): GetMediaUseCase = mediaUseCase

    @Provides
    @Singleton
    fun provideSaveMediaUseCase(mediaUseCase: SaveMediaUseCaseImpl): SaveMediaUseCase = mediaUseCase

    @Provides
    @Singleton
    fun provideGetContentsUseCase(contentsUse: GetContentsUseCaseImpl): GetContentsUseCase = contentsUse

    @Provides
    @Singleton
    fun provideGetContentsDbUseCase(contentsUse: GetContentsDbUseCaseImpl): GetContentsDbUseCase = contentsUse

    @Provides
    @Singleton
    fun provideGetAllContentsDbUseCase(contentsUse: GetAllContentsDbUseCaseImpl): GetAllContentsDbUseCase = contentsUse

    @Provides
    @Singleton
    fun provideLatestContentsUseCase(latestContentsUseCase: GetLatestContentsUseCaseImpl): GetLatestContentsUseCase = latestContentsUseCase

    @Provides
    @Singleton
    fun provideDownloadFilesUseCase(downloadFilesUseCase: DownloadFilesUseCaseImpl): DownloadFilesUseCase = downloadFilesUseCase

    @Provides
    @Singleton
    fun updateStatusContentUseCase(updateStatusContent: UpdateContentStatusUseCaseImpl): UpdateContentStatusUseCase = updateStatusContent

    @Provides
    @Singleton
    fun updateDeviceInfoUseCase(updateStatusContent: UpdateDeviceInfoUseCaseImpl): UpdateDeviceInfoUseCase = updateStatusContent

    @Provides
    @Singleton
    fun updateMediaSeenAtUseCase(updateMediaSeen: UpdateMediaSeenAtUseCaseImpl): UpdateMediaSeenAtUseCase = updateMediaSeen

    @Provides
    @Singleton
    fun provideRegisterIntoGroupUseCase(register: RegisterIntoGroupUseCaseImpl): RegisterIntoGroupUseCase = register

    @Provides
    @Singleton
    fun provideSendScoreUseCase(sendScore: SendScoreUseCaseImpl): SendScoreUseCase = sendScore

    @Provides
    @Singleton
    fun provideSendSignatureUseCase(sendSignature: SendSignatureUseCaseImpl): SendSignatureUseCase = sendSignature

    @Provides
    @Singleton
    fun provideUpdateSignatureUseCase(updateSignature: UpdateContentSignatureUseCaseImpl): UpdateContentSignatureUseCase = updateSignature

    @Provides
    @Singleton
    fun provideGetStatementUseCase(getStatementUseCase: GetStatementUseCaseImpl): GetStatementUseCase = getStatementUseCase
}