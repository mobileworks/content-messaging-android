package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.MediaRepository
import javax.inject.Inject

class DownloadFilesUseCaseImpl @Inject constructor(private val repository: MediaRepository) :
    DownloadFilesUseCase {
    override suspend fun invoke(content: ContentModel): Result<Boolean> {
        return repository.getMedias(content)
    }
}