package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.UpdatedStatusModel

interface GetUpdatedStatusDbUseCase {
    suspend operator fun invoke(): Result<List<UpdatedStatusModel>>
}