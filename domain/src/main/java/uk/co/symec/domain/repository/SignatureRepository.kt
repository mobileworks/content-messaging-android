package uk.co.symec.domain.repository

import uk.co.symec.domain.model.Result
import java.io.File

interface SignatureRepository {
    suspend fun sendSignature(
        file: File,
        signName: String,
        deviceId: String,
        contentId: String
    ): Result<Boolean>
}