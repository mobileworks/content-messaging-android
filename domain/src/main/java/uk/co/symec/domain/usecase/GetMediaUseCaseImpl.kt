package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.MediaRepository
import javax.inject.Inject

class GetMediaUseCaseImpl @Inject constructor(private val repository: MediaRepository) :
    GetMediaUseCase {
    override suspend fun invoke(mediaId: String): Result<MediaModel> {
        return repository.getMedia(mediaId)
    }
}