package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result

interface UpdateMediaSeenAtUseCase {
    suspend operator fun invoke(mediaId: String): Result<Boolean>
}