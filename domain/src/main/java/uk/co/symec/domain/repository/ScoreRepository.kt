package uk.co.symec.domain.repository

import uk.co.symec.domain.model.Result

interface ScoreRepository {
    suspend fun sendScore(
        score: Int,
        deviceId: String,
        mediaId: String
    ): Result<Boolean>
}