package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result

interface SendScoreUseCase {
    suspend operator fun invoke(score: Int, deviceId: String, mediaId: String): Result<Boolean>
}