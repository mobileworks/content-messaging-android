package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class UpdateContentSignatureUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    UpdateContentSignatureUseCase {
    override suspend fun invoke(contentId: String, signLink: String): Result<ContentModel> {
        return repository.updateSignatureContent(contentId, signLink)
    }
}