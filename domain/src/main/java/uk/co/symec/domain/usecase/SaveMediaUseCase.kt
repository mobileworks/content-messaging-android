package uk.co.symec.domain.usecase

import uk.co.symec.domain.BaseUseCase
import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.Result

interface SaveMediaUseCase : BaseUseCase<MediaModel, String> {
    override suspend operator fun invoke(media: MediaModel): Result<String>
}