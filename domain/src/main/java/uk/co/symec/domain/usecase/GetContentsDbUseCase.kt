package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface GetContentsDbUseCase {
    suspend operator fun invoke(isInternal: Boolean): Result<List<ContentModel>>
}