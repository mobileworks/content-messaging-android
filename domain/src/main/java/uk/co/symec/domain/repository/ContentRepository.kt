package uk.co.symec.domain.repository

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.UpdatedStatusModel

interface ContentRepository {
    suspend fun getContent(guid: String): Result<ContentModel>

    suspend fun deleteContentAndReturnList(guid: String, isInternal: Boolean): Result<List<ContentModel>>

    suspend fun updateContentAndReturnList(deviceId: String, guid: String, isInternal: Boolean): Result<List<ContentModel>>

    suspend fun getContents(deviceId: String, isInternal: Boolean, page: Int): Pair<Result<List<ContentModel>>, Boolean>

    suspend fun getContentsFromDb(isInternal: Boolean): Result<List<ContentModel>>

    suspend fun getAllContentsFromDb(): Result<List<ContentModel>>

    suspend fun deleteContentFromDb(list: List<String>): Result<Boolean>

    suspend fun getLatestContents(): Result<List<ContentModel>>

    suspend fun updateStatusContent(deviceId: String, contentId: String, status: Int): Result<ContentModel>

    suspend fun updateSignatureContent(contentId: String, signLink: String): Result<ContentModel>

    suspend fun getUpdatedStatuses(): Result<List<UpdatedStatusModel>>

    suspend fun deleteUpdatedStatusFromDb(contentId: String): Result<Boolean>
}