package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.SignatureRepository
import java.io.File
import javax.inject.Inject

class SendSignatureUseCaseImpl @Inject constructor(private val repository: SignatureRepository) :
    SendSignatureUseCase {
    override suspend fun invoke(file: File, signName: String, deviceId: String, contentId: String): Result<Boolean> {
        return repository.sendSignature(file, signName, deviceId, contentId)
    }
}