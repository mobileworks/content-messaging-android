package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.RegisterToGroupModel
import uk.co.symec.domain.model.Result

interface RegisterIntoGroupUseCase {
    suspend operator fun invoke(assetNo: String, fcm: String, groupId: String, model: String, imei: String): Result<RegisterToGroupModel>
}