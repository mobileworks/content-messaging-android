package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.StatementModel

interface GetStatementUseCase {
    suspend operator fun invoke(deviceId: String): Result<StatementModel>
}