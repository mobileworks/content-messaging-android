package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.MediaRepository
import javax.inject.Inject

class SaveMediaUseCaseImpl @Inject constructor(private val repository: MediaRepository) :
    SaveMediaUseCase {
    override suspend fun invoke(mediaModel: MediaModel): Result<String> {
        return repository.saveMedia(mediaModel)
    }
}