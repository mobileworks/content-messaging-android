package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetContentsDbUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetContentsDbUseCase {
    override suspend fun invoke(isInternal: Boolean): Result<List<ContentModel>> {
        return repository.getContentsFromDb(isInternal)
    }
}