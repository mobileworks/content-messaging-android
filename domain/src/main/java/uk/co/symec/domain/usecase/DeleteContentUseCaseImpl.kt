package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class DeleteContentUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    DeleteContentUseCase {
    override suspend fun invoke(
        contentId: String,
        isInternal: Boolean
    ): Result<List<ContentModel>> {
        return repository.deleteContentAndReturnList(contentId, isInternal)
    }
}