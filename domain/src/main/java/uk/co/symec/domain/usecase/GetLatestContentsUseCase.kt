package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface GetLatestContentsUseCase {
    suspend operator fun invoke(): Result<List<ContentModel>>
}