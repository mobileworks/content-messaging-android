package uk.co.symec.domain.usecase

import uk.co.symec.domain.BaseUseCase
import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface GetContentUseCase : BaseUseCase<String, ContentModel> {
    override suspend operator fun invoke(contentId: String): Result<ContentModel>
}