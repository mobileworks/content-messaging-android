package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class DeleteUpdatedStatusFromDbUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    DeleteUpdatedStatusFromDbUseCase {
    override suspend fun invoke(
        contentId: String
    ): Result<Boolean> {
        return repository.deleteUpdatedStatusFromDb(contentId)
    }
}