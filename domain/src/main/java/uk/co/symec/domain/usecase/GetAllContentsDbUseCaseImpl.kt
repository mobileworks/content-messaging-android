package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetAllContentsDbUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetAllContentsDbUseCase {
    override suspend fun invoke(downloadFiles: Boolean): Result<List<ContentModel>> {
        return repository.getAllContentsFromDb()
    }
}