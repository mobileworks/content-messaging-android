package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import uk.co.symec.domain.repository.MediaRepository
import javax.inject.Inject

class UpdateMediaSeenAtUseCaseImpl @Inject constructor(private val repository: MediaRepository) :
    UpdateMediaSeenAtUseCase {
    override suspend fun invoke(mediaId: String): Result<Boolean> {
        return repository.updateSeenAt(mediaId)
    }
}