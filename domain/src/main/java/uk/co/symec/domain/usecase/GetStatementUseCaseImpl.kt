package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.StatementModel
import uk.co.symec.domain.repository.ScoreRepository
import uk.co.symec.domain.repository.StatementRepository
import javax.inject.Inject

class GetStatementUseCaseImpl @Inject constructor(private val repository: StatementRepository) :
    GetStatementUseCase {
    override suspend fun invoke(deviceId: String): Result<StatementModel> {
        return repository.getStatement(deviceId)
    }
}