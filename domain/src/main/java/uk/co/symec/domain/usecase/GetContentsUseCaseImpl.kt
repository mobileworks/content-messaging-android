package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetContentsUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetContentsUseCase {
    override suspend fun invoke(
        deviceId: String,
        isInternal: Boolean,
        page: Int
    ): Pair<Result<List<ContentModel>>, Boolean> {
        return repository.getContents(deviceId, isInternal, page)
    }
}