package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import java.io.File

interface SendSignatureUseCase {
    suspend operator fun invoke(file: File, signName: String, deviceId: String, contentId: String): Result<Boolean>
}