package uk.co.symec.domain.model

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.util.*

data class MediaModel(
    val id: String,
    var contentId: String,
    val name: String?,
    val extension: String?,
    val size: String?,
    val link: String,
    val thumbnail_link: String?,
    val createdAt: Date,
    val updatedAt: Date,
    val seenAt: Date?,
    var savedPath: String,
    var score: Float = 0.0f
) {
    val getLabel: String
        get() {
            return when (extension) {
                "doc", "pdf" -> "Open document"
                "png", "jpg" -> "View image"
                "mov", "avi" -> "Watch movie"
                else -> "Open link"
            }
        }

    val isFileSaved: Boolean
        get() {
            return savedPath.isNotBlank()
        }

    companion object {
        fun getEmptyModel(): MediaModel {
            return MediaModel(
                "", "", "", "", "", "http://link", "http://link", Date(),
                Date(), Date(), ""
            )
        }

        @JvmStatic
        @BindingAdapter("app:showResultRatingResult")
        fun showResultRatingResult(
            view: TextView,
            counter: Float
        ) {
            if (counter > 0) {
                view.text = "${counter.toInt()}/5"
            } else {
                view.text = ""
            }
        }
    }
}

