package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result

interface DeleteContentFromDbUseCase {
    suspend operator fun invoke(list: List<String>): Result<Boolean>
}