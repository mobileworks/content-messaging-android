package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface DownloadFilesUseCase {
    suspend operator fun invoke(content: ContentModel): Result<Boolean>
}