package uk.co.symec.domain.repository

import uk.co.symec.domain.model.MessageContentModel
import uk.co.symec.domain.model.Result

interface UpdateDeviceInfoRepository {
    suspend fun updateDevice(
        deviceId: String,
        fcm: String
    ): Result<MessageContentModel>
}