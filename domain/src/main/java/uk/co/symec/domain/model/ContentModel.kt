package uk.co.symec.domain.model

import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

data class ContentModel(
    val id: String,
    val title: String?,
    val text: String?,
    val isInternal: Boolean,
    val sendNotification: Boolean,
    val createdAt: Date,
    val updatedAt: Date,
    val status: Int,
    val signature: String?,
    val bookmark: Int,
    val isConfirmationRequired: Boolean = false,
    val media: List<MediaModel>
) {
    val getParsedDate: String
        get() {
            val formatter = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
            return formatter.format(updatedAt)
        }

    val showConfirmation: Boolean
        get() {
            return isConfirmationRequired && signature == null
        }

    val getTimePast: String
        get() {
            return when (val minuteDifference =
                (System.currentTimeMillis() - updatedAt.time) / 60000) {
                in 0..60 -> "$minuteDifference m"
                in 61..1440 -> {
                    val x = minuteDifference / 60
                    "$x h"
                }
                else -> {
                    val x = minuteDifference / 1440
                    "$x d"
                }
            }
        }

    companion object {

        fun getEmptyModel(): ContentModel {
            return ContentModel(
                "", "", "", false, false, Date(),
                Date(), 0, null, 0, false, mutableListOf()
            )
        }

        fun getFilledModel(): ContentModel {
            return ContentModel(
                "xxxxx", "test", "test", false, false, Date(),
                Date(), 0, null, 0, false, mutableListOf(MediaModel.getEmptyModel())
            )
        }

        @JvmStatic
        @BindingAdapter("app:isVisibleIcon")
        fun isVisibleIcon(
            view: View,
            boolean: Boolean
        ) {
            if (boolean) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @JvmStatic
        @BindingAdapter("app:isNewIcon")
        fun isNewIcon(
            view: View,
            status: Int
        ) {
            when (status) {
                0 -> {
                    view.visibility = View.VISIBLE
                }
                else -> {
                    view.visibility = View.GONE
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:isBookmarkIcon")
        fun isBookmarkIcon(
            view: View,
            status: Int
        ) {
            when (status) {
                0 -> {
                    view.visibility = View.GONE
                }
                else -> {
                    view.visibility = View.VISIBLE
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:isConfirmationIcon")
        fun isConfirmationIcon(
            view: View,
            boolean: Boolean
        ) {
            if (boolean) {
                view.visibility = View.VISIBLE
            } else {
                view.visibility = View.GONE
            }
        }

        @JvmStatic
        @BindingAdapter("app:setLayout")
        fun setLayout(
            view: View,
            status: Int
        ) {
            val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
            when (status) {
                0 -> {
                    layoutParams.leftMargin = 16
                    view.layoutParams = layoutParams
                }
                else -> {
                    layoutParams.leftMargin = 0
                    view.layoutParams = layoutParams
                }
            }
        }

        @JvmStatic
        @BindingAdapter("app:isShowShare")
        fun isShowShare(
            view: View,
            boolean: Boolean
        ) {
            if (boolean) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE
            }
        }

        @JvmStatic
        @BindingAdapter("app:iconCounter")
        fun getIconCounter(
            view: TextView,
            counter: Int
        ) {
            if (counter > 1) {
                view.visibility = View.VISIBLE
                view.text = counter.toString()
            } else {
                view.visibility = View.GONE
            }
        }

        @JvmStatic
        @BindingAdapter("android:layout_width")
        fun setWidth(view: View, isMatchParent: Boolean) {
            val params = view.layoutParams
            params.width = if (isMatchParent) WRAP_CONTENT else MATCH_PARENT
            view.layoutParams = params
            view.invalidate()
        }
    }
}