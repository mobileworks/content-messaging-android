package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class DeleteContentFromDbUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    DeleteContentFromDbUseCase {
    override suspend fun invoke(
        list: List<String>
    ): Result<Boolean> {
        return repository.deleteContentFromDb(list)
    }
}