package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface GetContentsUseCase {
    suspend operator fun invoke(deviceId: String, isInternal: Boolean, page: Int = 1): Pair<Result<List<ContentModel>>, Boolean>
}