package uk.co.symec.domain.repository

import uk.co.symec.domain.model.RegisterToGroupModel
import uk.co.symec.domain.model.Result

interface RegisterRepository {
    suspend fun registerIntoGroup(
        assetNo: String,
        fcm: String,
        groupId: String,
        model: String,
        imei: String
    ): Result<RegisterToGroupModel>
}