package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class UpdateContentUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    UpdateContentUseCase {
    override suspend fun invoke(deviceId: String, contentId: String, isInternal: Boolean): Result<List<ContentModel>> {
        return repository.updateContentAndReturnList(deviceId, contentId, isInternal)
    }
}