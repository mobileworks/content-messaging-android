package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface UpdateContentUseCase {
    suspend operator fun invoke(deviceId: String, contentId: String, isInternal: Boolean): Result<List<ContentModel>>
}