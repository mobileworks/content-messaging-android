package uk.co.symec.domain.model

sealed class Result<out T : Any>
data class Success<out T : Any>(val data: T) : Result<T>()
data class SuccessDouble<out T : Any>(val data: DoubleDataPackage<T>) : Result<T>()
data class SuccessButNoInternet<out T : Any>(val data: T) : Result<T>()
data class SuccessButConnectionError<out T : Any>(val data: HttpErrorPackage<T>) : Result<T>()
data class Failure(val httpError: HttpError) : Result<Nothing>()
data class Logout(val data: Boolean) : Result<Nothing>()
data class Archived(val data: Boolean) : Result<Nothing>()

class HttpError(val throwable: Throwable, val errorCode: Int = 0)

inline fun <T : Any> Result<T>.onSuccess(action: (T) -> Unit): Result<T> {
    if (this is Success) action(data)
    return this
}

inline fun <T : Any> Result<T>.onSuccessDouble(action: (DoubleDataPackage<T>) -> Unit): Result<T> {
    if (this is SuccessDouble) action(data)
    return this
}

inline fun <T : Any> Result<T>.onSuccessButNoInternet(action: (T) -> Unit): Result<T> {
    if (this is SuccessButNoInternet) action(data)
    return this
}

inline fun <T : Any> Result<T>.onSuccessButConnectionError(action: (HttpErrorPackage<T>) -> Unit): Result<T> {
    if (this is SuccessButConnectionError) action(data)
    return this
}

inline fun <T : Any> Result<T>.onLogout(action: (Boolean) -> Unit): Result<T> {
    if (this is Logout) action(data)
    return this
}

inline fun <T : Any> Result<T>.onArchived(action: (Boolean) -> Unit): Result<T> {
    if (this is Archived) action(data)
    return this
}

inline fun <T : Any> Result<T>.onFailure(action: (HttpError) -> Unit) {
    if (this is Failure) action(httpError)
}

data class HttpErrorPackage<out T : Any>(val data: T, val httpError: HttpError)
data class DoubleDataPackage<out T : Any>(val data: T, val fromServer: List<ContentModel>)
data class UpdateListPackage<out T : Any>(val data: T, val isInternal: Boolean)