package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface UpdateContentStatusUseCase {
    suspend operator fun invoke(deviceId: String, contentId: String, status: Int): Result<ContentModel>
}