package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface UpdateContentSignatureUseCase {
    suspend operator fun invoke(contentId: String, signLink: String): Result<ContentModel>
}