package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.MessageContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.UpdateDeviceInfoRepository
import javax.inject.Inject

class UpdateDeviceInfoUseCaseImpl @Inject constructor(private val repository: UpdateDeviceInfoRepository) :
    UpdateDeviceInfoUseCase {
    override suspend fun invoke(deviceId: String, fcm: String): Result<MessageContentModel> {
        return repository.updateDevice(deviceId, fcm)
    }
}