package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ContentRepository
import javax.inject.Inject

class GetLatestContentsUseCaseImpl @Inject constructor(private val repository: ContentRepository) :
    GetLatestContentsUseCase {
    override suspend fun invoke(): Result<List<ContentModel>> {
        return repository.getLatestContents()
    }
}