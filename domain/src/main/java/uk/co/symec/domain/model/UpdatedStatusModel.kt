package uk.co.symec.domain.model

data class UpdatedStatusModel(
    val contentId: String,
    val status: Int,
    val deviceId: String
) {

    companion object {
        fun getEmptyModel(): UpdatedStatusModel {
            return UpdatedStatusModel(
                "",
                0,
                ""
            )
        }
    }
}