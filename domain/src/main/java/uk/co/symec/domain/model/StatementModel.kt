package uk.co.symec.domain.model

data class StatementModel(val statement: String?, val message: String?)