package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.Result

interface DeleteContentUseCase {
    suspend operator fun invoke(contentId: String, isInternal: Boolean): Result<List<ContentModel>>
}