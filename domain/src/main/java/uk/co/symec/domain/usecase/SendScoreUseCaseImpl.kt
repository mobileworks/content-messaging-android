package uk.co.symec.domain.usecase

import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.ScoreRepository
import javax.inject.Inject

class SendScoreUseCaseImpl @Inject constructor(private val repository: ScoreRepository) :
    SendScoreUseCase {
    override suspend fun invoke(score: Int, deviceId: String, mediaId: String): Result<Boolean> {
        return repository.sendScore(score, deviceId, mediaId)
    }
}