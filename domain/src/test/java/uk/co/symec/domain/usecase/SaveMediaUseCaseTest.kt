package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.repository.MediaRepository

class SaveMediaUseCaseTest {

    private val mediaRepository: MediaRepository = mock(MediaRepository::class.java)
    private val mediaUseCase by lazy { SaveMediaUseCaseImpl(mediaRepository) }

    @Test
    fun `test GetMediaUseCase calls MediaRepository`() {
        runBlocking {
            val model = MediaModel.getEmptyModel()
            mediaUseCase(model)
            verify(mediaRepository).saveMedia(model)
        }
    }
}