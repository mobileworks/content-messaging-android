package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.PAGE
import uk.co.symec.domain.repository.ContentRepository
import uk.co.symec.domain.repository.StatementRepository

class GetStatementUseCaseTest {

    private val statementRepository: StatementRepository = mock(StatementRepository::class.java)
    private val getStatementUseCase by lazy { GetStatementUseCaseImpl(statementRepository) }

    @Test
    fun `test GetStatementUseCase calls StatementRepository`() {
        runBlocking {
            getStatementUseCase(DEVICE_ID)
            verify(statementRepository).getStatement(DEVICE_ID)
        }
    }
}