package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.repository.ContentRepository

class GetUpdatedStatusUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getUpdatedStatusDbUseCase by lazy { GetUpdatedStatusDbUseCaseImpl(contentRepository) }

    @Test
    fun `test GetContentUseCase calls ContentRepository`() {
        runBlocking {
            getUpdatedStatusDbUseCase()
            verify(contentRepository).getUpdatedStatuses()
        }
    }
}