package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.repository.MediaRepository

class GetMediaUseCaseTest {

    private val mediaRepository: MediaRepository = mock(MediaRepository::class.java)
    private val mediaUseCase by lazy { GetMediaUseCaseImpl(mediaRepository) }

    @Test
    fun `test GetMediaUseCase calls MediaRepository`() {
        runBlocking {
            mediaUseCase(CONTENT_ID)
            verify(mediaRepository).getMedia(CONTENT_ID)
        }
    }
}