package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository

class DeleteContentFromDbUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val deleteContentFromDbUseCaseTest by lazy { DeleteContentFromDbUseCaseImpl(contentRepository) }

    @Test
    fun `test GetUpdateContentUseCase calls ContentRepository`() {
        runBlocking {
            val list = listOf<String>()
            deleteContentFromDbUseCaseTest(list)
            verify(contentRepository).deleteContentFromDb(list)
        }
    }
}