package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository
import uk.co.symec.domain.repository.ScoreRepository
import uk.co.symec.domain.repository.SignatureRepository
import java.io.File

class SendSignatureUseCaseTest {

    private val signatureRepository: SignatureRepository = mock(SignatureRepository::class.java)
    private val sendSignatureUseCase by lazy { SendSignatureUseCaseImpl(signatureRepository) }

    @Test
    fun `test SendScoreUseCase calls ScoreRepository`() {
        runBlocking {
            val file = File.createTempFile("file",".jpg")
            sendSignatureUseCase(file, "name", DEVICE_ID, CONTENT_ID)
            verify(signatureRepository).sendSignature(file, "name", DEVICE_ID, CONTENT_ID)
        }
    }
}