package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.ASSET_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.FCM
import uk.co.symec.domain.GROUP_ID
import uk.co.symec.domain.repository.RegisterRepository

class RegisterIntoGroupUseCaseTest {

    private val registerRepository: RegisterRepository = mock(RegisterRepository::class.java)
    private val registerIntoGroupUseCase by lazy { RegisterIntoGroupUseCaseImpl(registerRepository) }

    @Test
    fun `test RegisterIntoGroupUseCase calls RegisterRepository`() {
        runBlocking {
            registerIntoGroupUseCase(ASSET_ID, FCM, GROUP_ID, "SAMSUNG", DEVICE_ID)
            verify(registerRepository).registerIntoGroup(ASSET_ID, FCM, GROUP_ID, "SAMSUNG", DEVICE_ID)
        }
    }
}