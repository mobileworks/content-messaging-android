package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository

class UpdateContentUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val updateContentUseCase by lazy { UpdateContentUseCaseImpl(contentRepository) }

    @Test
    fun `test GetUpdateContentUseCase calls ContentRepository`() {
        runBlocking {
            updateContentUseCase(DEVICE_ID, CONTENT_ID, IS_INTERNAL)
            verify(contentRepository).updateContentAndReturnList(DEVICE_ID, CONTENT_ID, IS_INTERNAL)
        }
    }
}