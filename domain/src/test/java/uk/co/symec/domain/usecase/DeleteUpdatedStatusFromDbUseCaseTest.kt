package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.repository.ContentRepository

class DeleteUpdatedStatusFromDbUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val deleteUpdatedStatusFromDbUseCase by lazy { DeleteUpdatedStatusFromDbUseCaseImpl(contentRepository) }

    @Test
    fun `test GetDeleteUpdatedStatusUseCase calls ContentRepository`() {
        runBlocking {
            deleteUpdatedStatusFromDbUseCase(CONTENT_ID)
            verify(contentRepository).deleteUpdatedStatusFromDb(CONTENT_ID)
        }
    }
}