package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.repository.ContentRepository

class GetLatestContentUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getLatestContents by lazy { GetLatestContentsUseCaseImpl(contentRepository) }

    @Test
    fun `test GetLatestContentsUseCase calls ContentRepository`() {
        runBlocking {
            getLatestContents()
            verify(contentRepository).getLatestContents()
        }
    }
}