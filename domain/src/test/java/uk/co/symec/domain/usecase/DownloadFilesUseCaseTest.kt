package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.repository.MediaRepository

class DownloadFilesUseCaseTest {

    private val mediaRepository: MediaRepository = mock(MediaRepository::class.java)
    private val downloadFilesUseCase by lazy { DownloadFilesUseCaseImpl(mediaRepository) }

    @Test
    fun `test DownloadFilesUseCase calls MediaRepository`() {
        runBlocking {
            val emptyModel = ContentModel.getEmptyModel()
            downloadFilesUseCase(emptyModel)
            verify(mediaRepository).getMedias(emptyModel)
        }
    }
}