package uk.co.symec.domain

const val IS_INTERNAL = false
const val PAGE = 1
const val DEVICE_ID = "1234"
const val GROUP_ID = "fdsfd-fdsfdsf"
const val ASSET_ID = "fdsfd-fdsfdsf"
const val FCM = "fdsfd-fdsfdsf"
const val CONTENT_ID = "fdsfd-fdsfdsf"
const val MEDIA_ID = "abcd"