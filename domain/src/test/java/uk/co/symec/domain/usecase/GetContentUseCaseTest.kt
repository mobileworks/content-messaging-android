package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.repository.ContentRepository

class GetContentUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getContent by lazy { GetContentUseCaseImpl(contentRepository) }

    @Test
    fun `test GetContentUseCase calls ContentRepository`() {
        runBlocking {
            getContent(CONTENT_ID)
            verify(contentRepository).getContent(CONTENT_ID)
        }
    }
}