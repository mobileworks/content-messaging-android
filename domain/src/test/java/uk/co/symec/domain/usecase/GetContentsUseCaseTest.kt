package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.PAGE
import uk.co.symec.domain.repository.ContentRepository

class GetContentsUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getContents by lazy { GetContentsUseCaseImpl(contentRepository) }

    @Test
    fun `test GetContentsUseCase calls ContentRepository`() {
        runBlocking {
            getContents(DEVICE_ID, IS_INTERNAL, PAGE)
            verify(contentRepository).getContents(DEVICE_ID, IS_INTERNAL, PAGE)
        }
    }
}