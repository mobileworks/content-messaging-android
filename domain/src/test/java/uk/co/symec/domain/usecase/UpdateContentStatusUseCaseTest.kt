package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.MEDIA_ID
import uk.co.symec.domain.repository.ContentRepository

class UpdateContentStatusUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val updateContentStatusUseCase by lazy { UpdateContentStatusUseCaseImpl(contentRepository) }

    @Test
    fun `test UpdateContentStatusUseCase calls ContentRepository`() {
        runBlocking {
            updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 0)
            verify(contentRepository).updateStatusContent(DEVICE_ID, CONTENT_ID, 0)
        }
    }
}