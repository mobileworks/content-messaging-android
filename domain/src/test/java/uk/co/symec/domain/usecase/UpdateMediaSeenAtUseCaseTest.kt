package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.MEDIA_ID
import uk.co.symec.domain.repository.MediaRepository

class UpdateMediaSeenAtUseCaseTest {

    private val mediaRepository: MediaRepository = mock(MediaRepository::class.java)
    private val updateMediaSeenAtUseCase by lazy { UpdateMediaSeenAtUseCaseImpl(mediaRepository) }

    @Test
    fun `test UpdateMediaSeenAtUseCase calls ContentRepository`() {
        runBlocking {
            updateMediaSeenAtUseCase(MEDIA_ID)
            verify(mediaRepository).updateSeenAt(MEDIA_ID)
        }
    }
}