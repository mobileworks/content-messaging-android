package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.FCM
import uk.co.symec.domain.repository.UpdateDeviceInfoRepository

class UpdateDeviceInfoUseCaseTest {

    private val updateDeviceInfoRepository: UpdateDeviceInfoRepository = mock(UpdateDeviceInfoRepository::class.java)
    private val updateDeviceInfoUseCase by lazy { UpdateDeviceInfoUseCaseImpl(updateDeviceInfoRepository) }

    @Test
    fun `test UpdateContentStatusUseCase calls ContentRepository`() {
        runBlocking {
            updateDeviceInfoUseCase(DEVICE_ID, FCM)
            verify(updateDeviceInfoRepository).updateDevice(DEVICE_ID, FCM)
        }
    }
}