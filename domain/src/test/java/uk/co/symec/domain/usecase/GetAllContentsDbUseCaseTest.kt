package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository

class GetAllContentsDbUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getAllContentsDb by lazy { GetAllContentsDbUseCaseImpl(contentRepository) }

    @Test
    fun `test GetAllContentsDbUseCase calls ContentRepository`() {
        runBlocking {
            getAllContentsDb(true)
            verify(contentRepository).getAllContentsFromDb()
        }
    }
}