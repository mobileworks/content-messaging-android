package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository

class UpdateContentSignatureUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val updateContentSignatureUseCase by lazy { UpdateContentSignatureUseCaseImpl(contentRepository) }

    @Test
    fun `test GetUpdateSignatureContentUseCase calls ContentRepository`() {
        runBlocking {
            updateContentSignatureUseCase(CONTENT_ID, "")
            verify(contentRepository).updateSignatureContent(CONTENT_ID, "")
        }
    }
}