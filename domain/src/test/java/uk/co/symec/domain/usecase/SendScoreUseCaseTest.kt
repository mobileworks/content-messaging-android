package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.CONTENT_ID
import uk.co.symec.domain.DEVICE_ID
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository
import uk.co.symec.domain.repository.ScoreRepository

class SendScoreUseCaseTest {

    private val scoreRepository: ScoreRepository = mock(ScoreRepository::class.java)
    private val sendScoreUseCase by lazy { SendScoreUseCaseImpl(scoreRepository) }

    @Test
    fun `test SendScoreUseCase calls ScoreRepository`() {
        runBlocking {
            sendScoreUseCase(4, DEVICE_ID, CONTENT_ID)
            verify(scoreRepository).sendScore(4, DEVICE_ID, CONTENT_ID)
        }
    }
}