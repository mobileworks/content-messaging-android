package uk.co.symec.domain.usecase

import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import uk.co.symec.domain.IS_INTERNAL
import uk.co.symec.domain.repository.ContentRepository

class GetContentsDbUseCaseTest {

    private val contentRepository: ContentRepository = mock(ContentRepository::class.java)
    private val getContentsDb by lazy { GetContentsDbUseCaseImpl(contentRepository) }

    @Test
    fun `test GetContentsDbUseCase calls ContentRepository`() {
        runBlocking {
            getContentsDb(IS_INTERNAL)
            verify(contentRepository).getContentsFromDb(IS_INTERNAL)
        }
    }
}