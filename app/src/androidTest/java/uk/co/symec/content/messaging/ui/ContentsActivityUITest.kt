package uk.co.symec.content.messaging.ui

import android.content.Context
import android.view.View
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.filters.LargeTest
import kotlinx.coroutines.*
import org.hamcrest.BaseMatcher
import org.hamcrest.CoreMatchers.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.ui.activity.ContentsActivity
import uk.co.symec.content.messaging.ui.activity.RegisterToGroupActivity
import uk.co.symec.data.db.AppDatabase
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.entity.ContentEntity
import java.io.IOException
import java.util.*

@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class ContentsActivityUITest {

    private lateinit var contentDao: ContentDAO
    private lateinit var db: AppDatabase

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        contentDao = db.contentDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @get:Rule
    var activityScenarioRule = activityScenarioRule<ContentsActivity>()

    @Test
    fun checkIfTabsAreVisible() {

        onView(isRoot()).perform(waitFor(5000))
        onView(withId(R.id.cv_content_tab)).check(matches(isDisplayed()))
        onView(
            allOf(
                withId(R.id.text_selected),
                isDisplayed()
            )
        ).check(matches(withText(R.string.external_tab)))
        onView(
            allOf(
                withId(R.id.text_unselected),
                isDisplayed()
            )
        ).check(matches(withText(R.string.internal_tab)))
    }

    @Test
    fun checkIfSearchAndRngAreVisible() {
        onView(isRoot()).perform(waitFor(2000))
        onView(allOf(withId(R.id.menu_search), isDisplayed()))
        onView(allOf(withId(R.id.action_ring), isDisplayed()))

        onView(isRoot()).perform(waitFor(2000))
    }

    @Test
    fun saveContentAndReadInList() {
        onView(isRoot()).perform(waitFor(2000))
        GlobalScope.launch {

            val content1 = ContentEntity(
                "1",
                "TITLE_1",
                "TEXT_1",
                true,
                true,
                Date(),
                Date(),
                1,
                "",
                true,
                mutableListOf()
            )
            contentDao.insertContent(content1)
            val content2 = ContentEntity(
                "2",
                "TITLE_2",
                "TEXT_2",
                true,
                true,
                Date(),
                Date(),
                1,
                "",
                true,
                mutableListOf()
            )
            contentDao.insertContent(content2)
            val content3 = ContentEntity(
                "3",
                "TITLE_3",
                "TEXT_3",
                true,
                true,
                Date(),
                Date(),
                1,
                "",
                true,
                mutableListOf()
            )
            contentDao.insertContent(content3)
            val content4 = ContentEntity(
                "4",
                "TITLE_4",
                "TEXT_4",
                true,
                true,
                Date(),
                Date(),
                1,
                "",
                true,
                mutableListOf()
            )
            contentDao.insertContent(content4)
            val content5 = ContentEntity(
                "5",
                "TITLE_5",
                "TEXT_5",
                true,
                true,
                Date(),
                Date(),
                1,
                "",
                true,
                mutableListOf()
            )
            contentDao.insertContent(content5)

            val todoItem = content5.title?.let { contentDao.getContentByTitle(it) }
            assertThat(todoItem, equalTo(content5))
        }
    }

    @Test
    fun openDetailsViewWithShare() {
        onView(isRoot()).perform(waitFor(2000))
        onView(
            allOf(
                isDisplayed(),
                first(withParent(withId(R.id.rv_content_list)))
            )
        ).perform(click())
        onView(isRoot()).perform(waitFor(2000))

        val appCompatImageButton = onView(
            allOf(
                withId(R.id.cl_share),
                withEffectiveVisibility(Visibility.VISIBLE)
            )
        )
        appCompatImageButton.perform(click())
    }

//    @Test
//    fun clickRWItemWithShare() {
//        onView(isRoot()).perform(waitFor(2000))
//        onView(
//            allOf(
//                isDisplayed(),
//                first(withParent(withId(R.id.rv_content_list)))
//            )
//        ).perform(RecyclerViewActions.actionOnItemAtPosition<ContentsViewHolder>(0, clickItemWithId(R.id.iv_send)))
//        onView(isRoot()).perform(waitFor(5000))
//    }

    private fun <T> first(matcher: Matcher<T>): Matcher<T>? {
        return object : BaseMatcher<T>() {
            var isFirst = true
            override fun matches(item: Any): Boolean {
                if (isFirst && matcher.matches(item)) {
                    isFirst = false
                    return true
                }
                return false
            }

            override fun describeTo(description: Description) {
                description.appendText("should return first matching item")
            }
        }
    }

    fun clickItemWithId(id: Int): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View>? {
                return null
            }

            override fun getDescription(): String {
                return "Click on a child view with specified id."
            }

            override fun perform(uiController: UiController, view: View) {
                val v = view.findViewById<View>(id) as View
                v.performClick()
            }
        }
    }

    private fun waitFor(delay: Long): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isRoot()
            }

            override fun getDescription(): String {
                return "wait for " + delay + "milliseconds"
            }

            override fun perform(uiController: UiController, view: View?) {
                uiController.loopMainThreadForAtLeast(delay)
            }
        }
    }
}
