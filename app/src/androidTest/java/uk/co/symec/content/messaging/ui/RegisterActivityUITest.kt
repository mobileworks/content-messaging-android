package uk.co.symec.content.messaging.ui

import android.view.View
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.filters.LargeTest
import org.hamcrest.Matcher
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.ui.activity.RegisterToGroupActivity

@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class RegisterActivityUITest {

    @get:Rule
    var activityScenarioRule = activityScenarioRule<RegisterToGroupActivity>()

    @Test
    fun a_registerToGroupTest() {

        //verify if text exists
        onView(withId(R.id.tv_title)).check(matches(withText(R.string.txt_header)))
        onView(withId(R.id.tv_save)).check(matches(withText(R.string.txt_save_group_id)))

        // Type text and then press the button.
        onView(withId(R.id.et_group_id))
            .perform(typeText(GROUP_ID), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.et_group_id)).check(matches(withText(GROUP_ID)))
        //click button
        onView(withId(R.id.btn_log_in)).perform(click())

        //wait and check if tabs visible
        onView(isRoot()).perform(waitFor(1000))

        // Type text and then press the button.
        onView(withId(R.id.et_asset_id))
            .perform(typeText(ASSET_ID), closeSoftKeyboard())

        // Check that the text was changed.
        onView(withId(R.id.et_asset_id)).check(matches(withText(ASSET_ID)))
        //click button
        onView(withId(R.id.btn_log_in)).perform(click())

        //wait and check if tabs visible
        onView(isRoot()).perform(waitFor(5000))

        onView(withId(R.id.cv_content_tab)).check(matches(isDisplayed()))
    }

    private fun waitFor(delay: Long): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isRoot()
            }

            override fun getDescription(): String {
                return "wait for " + delay + "milliseconds"
            }

            override fun perform(uiController: UiController, view: View?) {
                uiController.loopMainThreadForAtLeast(delay)
            }
        }
    }

    companion object {
        const val ASSET_ID = "6666"
        const val GROUP_ID = "7e22a6eb-a5fe-472e-a9d8-8164f487e469"
    }
}
