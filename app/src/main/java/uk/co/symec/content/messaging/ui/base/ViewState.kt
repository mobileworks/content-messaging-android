package uk.co.symec.content.messaging.ui.base

import uk.co.symec.domain.model.HttpErrorPackage
import uk.co.symec.domain.model.UpdateListPackage


sealed class ViewState<out T : Any>
class Success<out T : Any>(val data: T) : ViewState<T>()
class SuccessWithDownload<out T : Any>(val data: T) : ViewState<T>()
class SuccessHideLoading<out T : Any>(val data: UpdateListPackage<T>) : ViewState<T>()
class SuccessWithShowDetails<out T : Any>(val data: T, val guid: String) : ViewState<T>()
class Update<out T : Any>(val data: T) : ViewState<T>()
class SuccessButNoInternet<out T : Any>(val data: T) : ViewState<T>()
class SuccessButConnectionError<out T : Any>(val data: HttpErrorPackage<T>) : ViewState<T>()
class Error<out T : Any>(val error: Throwable) : ViewState<T>()
class Logout<out T : Any>(val data: Boolean) : ViewState<T>()
class Archived<out T : Any>(val data: Boolean) : ViewState<T>()
class Loading<out T : Any> : ViewState<T>()
class NoInternetState<T : Any> : ViewState<T>()

fun <T : Any> getStatus(viewState: ViewState<T>): String {
    return when (viewState) {
        is Loading -> "Loading"
        is Success -> "Success"
        is SuccessWithShowDetails -> "SuccessWithShowDetails"
        is SuccessButNoInternet -> "SuccessButNoInternet"
        is SuccessButConnectionError -> "SuccessButConnectionError"
        is Error -> "Error"
        is Logout -> "Logout"
        is Archived -> "Archived"
        is NoInternetState -> "NoInternetState"
        is Update -> "Update"
        is SuccessHideLoading -> "SuccessHideLoading"
        is SuccessWithDownload -> "SuccessWithDownload"
    }
}


