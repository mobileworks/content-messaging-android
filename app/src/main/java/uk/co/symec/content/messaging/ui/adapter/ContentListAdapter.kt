package uk.co.symec.content.messaging.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.BuildConfig
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.ContentsListItemBinding
import uk.co.symec.domain.model.ContentModel

class ContentListAdapter(val clickListener: ContentsListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var contentsList = mutableListOf<ContentModel>()

    fun setContentsList(list: List<ContentModel>) {
//        val diffResult = DiffUtil.calculateDiff(ContentsDiffCallback(contentsList, list))
//        contentsList.clear()
//        contentsList.addAll(list)
//        diffResult.dispatchUpdatesTo(this)
        contentsList.clear()
        contentsList.addAll(list)
        notifyDataSetChanged()
    }

    fun setQueriedList(list: List<ContentModel>) {
        contentsList.clear()
        contentsList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ContentsViewHolder.from(parent)
    }

    override fun getItemId(position: Int): Long {
        return contentsList[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return contentsList.size
    }

    private fun getItem(position: Int): ContentModel {
        return contentsList[position]
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ContentsViewHolder -> {
                val header = getItem(position)
                holder.bind(header, clickListener)
                holder.setIsRecyclable(false)
            }
        }
    }
}

class ContentsViewHolder private constructor(val binding: ContentsListItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ContentModel, clickListener: ContentsListener) {
        binding.content = item
        binding.clickListener = clickListener

        val firstMediaFile = item.media.firstOrNull()
        if (firstMediaFile == null) {
            binding.tvEmptyPreview.visibility = View.VISIBLE
        } else {
            if (firstMediaFile.thumbnail_link == null) {
                binding.ivPreview.setImageResource(R.drawable.ic_paperclip)
            } else {
                try {
                    val picasso = Picasso.get()
                    if (BuildConfig.DEBUG) {
                        picasso.isLoggingEnabled = true
                    }
                    picasso
                        .load(firstMediaFile.thumbnail_link)
                        .fit()
                        .centerInside()
                        .into(binding.ivPreview, object :
                            Callback {
                            override fun onSuccess() {
                            }

                            override fun onError(e: Exception?) {
                            }
                        })
                } catch (error: Exception) {
                    println(error.message)
                }
            }
        }

        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): ContentsViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ContentsListItemBinding.inflate(layoutInflater, parent, false)
            return ContentsViewHolder(binding)
        }
    }
}

class ContentsDiffCallback(
    private val oldList: List<ContentModel>,
    private val newList: List<ContentModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id === newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val oldItem = oldList[oldPosition]
        val newItem = newList[newPosition]

        println("Old item ${oldItem.id} = ${oldItem.status}")
        println("New item ${newItem.id} = ${newItem.status}")

        return oldItem == newItem
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}

class ContentsListener(val clickListener: (id: String) -> Unit, val shareListener: (content: String) -> Unit) {
    fun onClick(content: ContentModel) = clickListener(content.id)

    fun onShare(content: ContentModel) = shareListener(content.text ?: "")
}
