package uk.co.symec.content.messaging.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.google.zxing.integration.android.IntentIntegrator
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.RegisterAssetIdFragmentBinding
import uk.co.symec.content.messaging.vm.RegisterToGroupViewModel

class RegisterAssetIdFragment : Fragment() {

    val viewModel: RegisterToGroupViewModel by activityViewModels()
    private var _binding: RegisterAssetIdFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = RegisterAssetIdFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.validationState.observe(requireActivity(), { validation ->
            if (validation == 3) {
                binding.etAssetId.error = resources.getString(R.string.missing_asset_id)
            } else {
                binding.etAssetId.error = null
            }
        })

        binding.btnQr.setOnClickListener {
            IntentIntegrator.forSupportFragment(this).initiateScan()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(requireContext(), R.string.cancel, Toast.LENGTH_LONG).show()
            } else {
                binding.etAssetId.setText(result.contents)
                viewModel.assetId = result.contents
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    companion object {
        fun newInstance() = RegisterAssetIdFragment()
    }
}