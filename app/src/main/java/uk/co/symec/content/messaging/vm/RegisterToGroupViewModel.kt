package uk.co.symec.content.messaging.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.content.messaging.ui.base.Archived
import uk.co.symec.content.messaging.ui.base.BaseViewModel
import uk.co.symec.content.messaging.ui.base.Error
import uk.co.symec.content.messaging.ui.base.Success
import uk.co.symec.domain.model.RegisterToGroupModel
import uk.co.symec.domain.model.onArchived
import uk.co.symec.domain.model.onFailure
import uk.co.symec.domain.model.onSuccess
import uk.co.symec.domain.usecase.RegisterIntoGroupUseCase
import javax.inject.Inject

@HiltViewModel
public class RegisterToGroupViewModel @Inject constructor(private val register: RegisterIntoGroupUseCase) :
    BaseViewModel<RegisterToGroupModel>() {
    var assetId = String()
    var groupId = String()
    var fcm = String()
    var androidId = String()
    var model = String()

    val _validationState = MutableLiveData<Int>()
    val validationState: LiveData<Int>
        get() = _validationState

    fun register() {
        executeUseCase {
            register(assetId, fcm, groupId, model, androidId)
                .onSuccess {
                    if (it.token == null) {
                        _viewState.value = Error(Throwable(it.message))
                    } else {
                        _viewState.value = Success(it)
                    }
                }
                .onArchived { _viewState.value = Archived(it) }
                .onFailure { _viewState.value = Error(it.throwable) }
            println(_viewState.value)
        }
    }

    fun validationAssetId() {
        if (assetId.isBlank()) {
            _validationState.value = 3
        } else {
            register()
            _validationState.value = 4
        }
    }

    fun validationGroupId() {
        if (groupId.isBlank()) {
            _validationState.value = 1
        } else {
            _validationState.value = 2
        }
    }
}