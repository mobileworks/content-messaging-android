package uk.co.symec.content.messaging.di

import android.content.Context
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uk.co.symec.data.utils.CoroutineContextProvider
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    enum class State { BACKGROUND, FOREGROUND }

    private var state = State.BACKGROUND

    private var wasInBackground = false

    private var updateExternalList = false
    private var updateInternalList = false

    @Singleton
    @Provides
    fun provideCoroutineContext(): CoroutineContextProvider {
        return CoroutineContextProvider()
    }

    @Provides
    @Singleton
    fun provideAnalytics(@ApplicationContext context: Context): FirebaseAnalytics {
        return FirebaseAnalytics.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideCrashlytics(): FirebaseCrashlytics {
        return FirebaseCrashlytics.getInstance()
    }

    @Provides
    @Singleton
    fun getUpdateExternalList() = updateExternalList

    @Provides
    @Singleton
    fun setUpdateExternalList(value: Boolean): Boolean {
        updateExternalList = value
        return updateExternalList
    }

    @Provides
    @Singleton
    fun getUpdateInternalList() = updateInternalList

    @Provides
    @Singleton
    fun setUpdateInternalList(value: Boolean): Boolean {
        updateInternalList = value
        return updateInternalList
    }

    @Provides
    @Singleton
    fun getWasInBackground() = wasInBackground

    @Provides
    @Singleton
    fun setState(value: State): State {
        if (!wasInBackground) {
            wasInBackground = value == State.FOREGROUND && state == State.BACKGROUND
        }
        state = value
        return state
    }

    @Provides
    @Singleton
    fun resetState(value: State): State {
        wasInBackground = false
        state = value
        return state
    }
}