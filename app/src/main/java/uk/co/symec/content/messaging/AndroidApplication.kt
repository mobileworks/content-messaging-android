package uk.co.symec.content.messaging

import android.app.Application
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import com.google.firebase.FirebaseApp
import dagger.hilt.android.HiltAndroidApp
import uk.co.symec.content.messaging.di.AppModule


@HiltAndroidApp
class AndroidApplication : Application(), LifecycleObserver {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    // Application level lifecycle events
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onEnteredForeground() {
        Log.d("App", "Application did enter foreground")
        AppModule.setState(AppModule.State.FOREGROUND)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onEnteredBackground() {
        Log.d("App", "Application did enter background")
        AppModule.setState(AppModule.State.BACKGROUND)
    }
}