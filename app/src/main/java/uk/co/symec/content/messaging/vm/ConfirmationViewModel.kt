package uk.co.symec.content.messaging.vm

import android.graphics.Bitmap
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.content.messaging.ui.base.BaseViewModel
import uk.co.symec.content.messaging.ui.base.Error
import uk.co.symec.content.messaging.ui.base.Success
import uk.co.symec.content.messaging.ui.base.ViewState
import uk.co.symec.domain.model.MessageContentModel
import uk.co.symec.domain.model.onFailure
import uk.co.symec.domain.model.onSuccess
import uk.co.symec.domain.usecase.SendSignatureUseCase
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*
import javax.inject.Inject

@HiltViewModel
public class ConfirmationViewModel @Inject constructor(
    private val sendSignatureUseCase: SendSignatureUseCase
) : BaseViewModel<MessageContentModel>() {

    protected val _isButtonEnabled = MutableLiveData<Boolean>()
    val isButtonEnabled: LiveData<Boolean>
        get() = _isButtonEnabled

    protected val _signState = MutableLiveData<ViewState<Boolean>>()
    val signState: LiveData<ViewState<Boolean>>
        get() = _signState

    var userName = String()
        set(value) {
            _isButtonEnabled.value = fileResult != null && value.isNotBlank()
            field = value
        }

    var fileResult: File? = null
        set(value) {
            _isButtonEnabled.value = value != null
            && userName.isNotBlank()
            field = value
        }

    fun getFile(bitmap: Bitmap, path: String) {
        val fos: OutputStream

        val dir = File(path)
        if (dir.exists()) {
            for (aChildren in dir.list()) {
                File(dir, aChildren).delete()
            }
        }
        dir.mkdirs()

        val image = File(dir, "signature.png")
        fos = FileOutputStream(image)
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos)
        Objects.requireNonNull(fos).close()
        fileResult = image
    }

    fun resetSignature() {
        fileResult = null
    }

    fun sendSignature(file: File, signName: String, deviceId: String, contentId: String) = executeUseCase {
        sendSignatureUseCase(file, signName, deviceId, contentId)
            .onSuccess { _signState.value = Success(it) }
            .onFailure { _signState.value = Error(it.throwable) }
    }
}