package uk.co.symec.content.messaging.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.ui.base.Success
import uk.co.symec.content.messaging.ui.base.SuccessButNoInternet
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.*
import javax.inject.Inject

@HiltViewModel
public class DetailsViewModel @Inject constructor(
    private val getContentUseCase: GetContentUseCase,
    private val updateContentsUseCase: UpdateContentStatusUseCase,
    private val getMediaUseCase: GetMediaUseCase,
    private val getUpdatedStatusDbUseCase: GetUpdatedStatusDbUseCase,
    private val deleteUpdatedStatusFromDbUseCase: DeleteUpdatedStatusFromDbUseCase,
    private val sendScoreUseCase: SendScoreUseCase,
    private val updateMediaSeenAtUseCase: UpdateMediaSeenAtUseCase
) : BaseViewModel<ContentModel>() {

    protected val _mediaState = MutableLiveData<ViewState<MediaModel>>()
    val mediaState: LiveData<ViewState<MediaModel>>
        get() = _mediaState

    protected val _scoreState = MutableLiveData<ViewState<Boolean>>()
    val scoreState: LiveData<ViewState<Boolean>>
        get() = _scoreState

    protected val _dateUpdateState = MutableLiveData<ViewState<Boolean>>()
    val dateUpdateState: LiveData<ViewState<Boolean>>
        get() = _dateUpdateState

    fun getContent(contentId: String) = executeUseCase {
        getContentUseCase(contentId)
            .onSuccess {
                _viewState.value = Success(it)
                runSynchro()
            }
            .onSuccessButNoInternet {
                _viewState.value = SuccessButNoInternet(it)
                isConnectedNow = false
            }
            .onFailure {
                _viewState.value = Error(it.throwable)
                isConnectedNow = false
            }
    }

    fun sendScore(score: Int, deviceId: String, mediaId: String) = executeUseCase {
        sendScoreUseCase(score, deviceId, mediaId)
            .onSuccess { _scoreState.value = Success(it) }
            .onFailure { _scoreState.value = Error(it.throwable) }
    }

    fun seenAtUpdated(mediaId: String) = executeUseCase {
        updateMediaSeenAtUseCase(mediaId)
            .onSuccess { _dateUpdateState.value = Success(it) }
            .onFailure { _dateUpdateState.value = Error(it.throwable) }
    }

    fun statusSeen(deviceId: String, contentId: String) = executeUseCase {
        updateContentsUseCase(deviceId, contentId, 2)
            .onSuccess {
                _viewState.value = Update(it)
                runSynchro()
            }
            .onSuccessButNoInternet {
                _viewState.value = SuccessButNoInternet(it)
                isConnectedNow = false
            }
            .onFailure {
                _viewState.value = Error(it.throwable)
                isConnectedNow = false
            }
    }

    fun statusViewed(deviceId: String, contentId: String) = executeUseCase {
        updateContentsUseCase(deviceId, contentId, 1)
            .onSuccess {
                _viewState.value = Update(it)
                runSynchro()
            }
            .onSuccessButNoInternet {
                _viewState.value = SuccessButNoInternet(it)
                isConnectedNow = false
            }
            .onFailure {
                _viewState.value = Error(it.throwable)
                isConnectedNow = false
            }
    }

    fun getFile(id: String) = executeUseCase {
        getMediaUseCase(id)
            .onSuccess { _mediaState.value = Success(it) }
            .onFailure { _mediaState.value = Error(it.throwable) }
    }

    suspend fun runSynchro() {
        if (isConnected() && !isConnectedNow) {
            isConnectedNow = true
            //send all unsynchronized statuses
            getUpdatedStatusDbUseCase().onSuccess {
                it.forEach {
                    updateContentsUseCase(it.deviceId, it.contentId, it.status)
                        .onSuccess {
                            deleteUpdatedStatusFromDbUseCase(it.id)
                            isConnectedNow = true
                        }
                        .onSuccessButNoInternet {
                            deleteUpdatedStatusFromDbUseCase(it.id)
                            isConnectedNow = false
                        }
                        .onFailure {
                            _viewState.value = Error(it.throwable)
                            isConnectedNow = false
                        }
                }
            }
        }
    }
}