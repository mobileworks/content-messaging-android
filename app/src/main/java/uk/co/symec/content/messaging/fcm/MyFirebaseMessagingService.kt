package uk.co.symec.content.messaging.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.*
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.di.AppModule.setUpdateExternalList
import uk.co.symec.content.messaging.di.AppModule.setUpdateInternalList
import uk.co.symec.content.messaging.ui.activity.RegisterToGroupActivity
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.usecase.UpdateDeviceInfoUseCase
import javax.inject.Inject

class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var updateDeviceInfoUseCase: UpdateDeviceInfoUseCase

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        var title = ""
        var body = ""
        var contentId: String? = null
        var action: String? = null
        var isInternal = "false"

        Log.d(TAG, "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: ${remoteMessage.data}")
            contentId = remoteMessage.data[CONTENT_GUID]
            action = remoteMessage.data[ACTION]
            isInternal = remoteMessage.data[IS_INTERNAL] ?: "false"
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            title = it.title ?: ""
            body = it.body ?: ""
            Log.d(TAG, "Message Notification Body: ${it.body}")
        }

        if (!contentId.isNullOrBlank()) {
            val intent = Intent(CONTENT_UPDATE)
            intent.putExtra(CONTENT_GUID, contentId)
            intent.putExtra(ACTION, action)
            intent.putExtra(IS_INTERNAL, isInternal)
            applicationContext.sendBroadcast(intent)

            val booleanInternal = isInternal.toBoolean()
            if (booleanInternal) {
                setUpdateInternalList(true)
            } else {
                setUpdateExternalList(true)
            }

            if (title.isNotBlank() && body.isNotBlank()) {
                action?.let {
                    if (!action.contentEquals(DELETE)) {
                        sendNotification(title, body, contentId, action, isInternal)
                    }
                }
            }
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // [START on_new_token]
    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private fun handleNow() {
        Log.d(TAG, "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        Log.d(TAG, "sendRegistrationTokenToServer($token)")

        GlobalScope.launch {
            withContext(Dispatchers.IO) {
                with(NetworkModule.provideSharedPreferences(this@MyFirebaseMessagingService)) {
                    getString(Constants.ASSET_ID, null)?.let {
                        updateDeviceInfoUseCase.invoke(it, token ?: "")
                    }
                }
            }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(
        messageTitle: String,
        messageBody: String,
        contentId: String?,
        action: String?,
        isInternal: String
    ) {

        val intent = Intent(this, RegisterToGroupActivity::class.java)
        intent.putExtra(CONTENT_GUID, contentId)
        intent.putExtra(ACTION, action)
        intent.putExtra(IS_INTERNAL, isInternal)
        val pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Notification channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMsgService"
        const val CONTENT_UPDATE = "contentUpdate"
        const val CONTENT_GUID = "content_guid"
        const val IS_INTERNAL = "is_internal"
        const val ACTION = "action"
        const val UPDATE = "update"
        const val CREATE = "create"
        const val DELETE = "delete"
    }
}