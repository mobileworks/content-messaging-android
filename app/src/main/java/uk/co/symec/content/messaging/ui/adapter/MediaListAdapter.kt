package uk.co.symec.content.messaging.ui.adapter

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.BuildConfig
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.MediasListItemBinding
import uk.co.symec.domain.model.MediaModel


class MediaListAdapter(val context: Context, val clickListener: MediasListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mediasList = mutableListOf<MediaModel>()

    fun setMediaList(list: List<MediaModel>) {
//        val diffCallback = MediasDiffCallback(mediasList, list)
//        val diffResult = DiffUtil.calculateDiff(diffCallback)
//        mediasList.clear()
//        mediasList.addAll(list)
//        diffResult.dispatchUpdatesTo(this)
        mediasList.clear()
        mediasList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MediasViewHolder.from(parent)
    }

    override fun getItemId(position: Int): Long {
        return mediasList[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return mediasList.size
    }

    private fun getItem(position: Int): MediaModel {
        return mediasList[position]
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MediasViewHolder -> {
                val header = getItem(position)
                holder.bind(header, clickListener)
                holder.setIsRecyclable(false)
            }
        }
    }
}

class MediasViewHolder private constructor(
    val binding: MediasListItemBinding,
    val context: Context
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: MediaModel, clickListener: MediasListener) {
        binding.media = item
        binding.clickListener = clickListener

        var ratingBarReady = false

        var scoreBarSize = 0

        binding.rlScoreResult.setOnClickListener {
            binding.rlScoreResult.apply {
                alpha = 1f
                visibility = View.VISIBLE
                isClickable = false
                animate()
                    .alpha(0f)
                    .setInterpolator(LinearInterpolator())
                    .setDuration(500L)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            binding.rlScoreResult.visibility = View.GONE
                        }
                    })
            }

            if (scoreBarSize == 0) {
                scoreBarSize = binding.llScore.width
            }

//            val a = ResizeAnimation(binding.llScore)
//            a.duration = 1000L
//            a.setParams(0, scoreBarSize)

            binding.llScore.apply {
                alpha = 0f
                visibility = View.VISIBLE
                animate()
                    .alpha(1f)
                    .setInterpolator(LinearInterpolator())
                    .setDuration(500L)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            super.onAnimationEnd(animation)
                            isClickable = true
                            ratingBarReady = true
                        }
                    })
                //startAnimation(a)
            }
        }

        binding.rbScore.setOnRatingBarChangeListener { ratingBar, v, b ->
            if (ratingBarReady) {
                Log.d("RatingLog", "Rating clicked")
                clickListener.setScore(v.toInt(), item.id)

//                val a = ResizeAnimation(binding.llScore)
//                a.duration = 500L
//                a.startOffset = 1200L
//                a.setParams(scoreBarSize, 0)

                binding.llScore.apply {
                    animate()
                        .alpha(0f)
                        .setStartDelay(1500L)
                        .setInterpolator(LinearInterpolator())
                        .setDuration(500L)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                //super.onAnimationEnd(animation)
                                visibility = View.GONE
                                Log.d("RatingLog", "llscore ended")
                            }

                            override fun onAnimationStart(animation: Animator?) {
                                super.onAnimationStart(animation)
                                alpha = 1f
                                visibility = View.VISIBLE
                                ratingBarReady = false
                                Log.d("RatingLog", "llscore started")
                            }

                            override fun onAnimationCancel(animation: Animator?) {
                                super.onAnimationCancel(animation)
                                alpha = 1f
                                visibility = View.VISIBLE
                                Log.d("RatingLog", "llscore cancelled")
                            }
                        })
                    //startAnimation(a)
                }

                val scoreInt = v.toInt()
                binding.tvScore.text = "${scoreInt}/5"
                binding.ivScore.setImageResource(R.drawable.ic_star_with_score)
                binding.rlScoreResult.apply {
                    animate()
                        .alpha(1f)
                        .setStartDelay(1500L)
                        .setInterpolator(LinearInterpolator())
                        .setDuration(500L)
                        .setListener(object : AnimatorListenerAdapter() {
                            override fun onAnimationEnd(animation: Animator) {
                                //super.onAnimationEnd(animation)
                                isClickable = true
                                Log.d("RatingLog", "llscoreresult ended")
                            }

                            override fun onAnimationStart(animation: Animator?) {
                                super.onAnimationStart(animation)
                                alpha = 0f
                                visibility = View.VISIBLE
                                ratingBarReady = false
                                Log.d("RatingLog", "llscoreresult started")
                            }

                            override fun onAnimationCancel(animation: Animator?) {
                                super.onAnimationCancel(animation)
                                alpha = 0f
                                visibility = View.GONE
                                Log.d("RatingLog", "llscoreresult canceled")
                            }
                        })
                }
            } else {
                Log.d("RatingLog", "Rating not clicked")
            }
        }

        val thumbnailLink = item.thumbnail_link
        if (thumbnailLink == null) {
            binding.ivPreview.setImageResource(R.drawable.ic_paperclip)
        } else {
            try {
                val picasso = Picasso.get()
                if (BuildConfig.DEBUG) {
                    picasso.isLoggingEnabled = true
                }
                picasso
                    .load(item.thumbnail_link)
                    .fit()
                    .centerInside()
                    .into(binding.ivPreview, object :
                        Callback {
                        override fun onSuccess() {
                        }

                        override fun onError(e: Exception?) {
                            binding.ivPreview.setImageResource(R.drawable.ic_paperclip)
                        }
                    })
            } catch (error: Exception) {
                println(error.message)
            }
        }
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): MediasViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = MediasListItemBinding.inflate(layoutInflater, parent, false)
            return MediasViewHolder(binding, parent.context)
        }
    }
}

class MediasDiffCallback(
    private val oldList: List<MediaModel>,
    private val newList: List<MediaModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id === newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val oldItem = oldList[oldPosition]
        val newItem = newList[newPosition]

        return oldItem === newItem
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}

class MediasListener(val clickListener: (id: MediaModel) -> Unit, val scoreListener: (score: Int, mediaId: String) -> Unit) {
    fun onClick(media: MediaModel) = clickListener(media)

    fun setScore(score: Int, mediaId: String) = scoreListener(score, mediaId)
}
