package uk.co.symec.content.messaging.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uk.co.symec.content.messaging.databinding.NotificationItemBinding
import uk.co.symec.domain.model.ContentModel

class AlertDialogAdapter(list: List<ContentModel>, val clickListener: AdapterListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var contentList: List<ContentModel> = list

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ContentsNotificationViewHolder.from(parent)
    }

    override fun getItemId(position: Int): Long {
        return contentList[position].hashCode().toLong()
    }

    override fun getItemCount(): Int {
        return contentList.size
    }

    private fun getItem(position: Int): ContentModel {
        return contentList[position]
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ContentsNotificationViewHolder -> {
                val header = getItem(position)
                holder.bind(header, clickListener)
                holder.setIsRecyclable(false)
            }
        }
    }
}

class ContentsNotificationViewHolder private constructor(val binding: NotificationItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ContentModel, clickListener: AdapterListener) {
        binding.content = item
        binding.clickListener = clickListener

        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): ContentsNotificationViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = NotificationItemBinding.inflate(layoutInflater, parent, false)
            return ContentsNotificationViewHolder(binding)
        }
    }
}

class AdapterListener(val clickListener1: (id: String) -> Unit) {
    fun onClick1(content: ContentModel) = clickListener1(content.id)
}