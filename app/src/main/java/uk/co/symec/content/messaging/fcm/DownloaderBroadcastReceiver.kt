package uk.co.symec.content.messaging.fcm

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.annotation.CallSuper
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import uk.co.symec.content.messaging.R
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.domain.model.MediaModel
import uk.co.symec.domain.model.onFailure
import uk.co.symec.domain.model.onSuccess
import uk.co.symec.domain.usecase.SaveMediaUseCase
import javax.inject.Inject

abstract class HiltBroadcastReceiver : BroadcastReceiver() {
    @CallSuper
    override fun onReceive(context: Context, intent: Intent) {
    }
}

@DelicateCoroutinesApi
@AndroidEntryPoint
class DownloaderBroadcastReceiver : HiltBroadcastReceiver() {

    @Inject
    lateinit var saveMediaUseCase: SaveMediaUseCase

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)

        NetworkModule.getFileSetContainerList().firstOrNull { it.downloadId == id }
            ?.let { item ->
                val mediaModel = item.mediaModel
                mediaModel.contentId = item.contentGuid
                mediaModel.savedPath = item.devicePath
                saveMedia(mediaModel, context)
                NetworkModule.getFileSetContainerList().remove(item)
            }
    }

    private fun saveMedia(media: MediaModel, context: Context) {
        GlobalScope.launch {
            saveMediaUseCase(media)
                .onSuccess {
                    showToast(context, context.getString(R.string.file_downloaded, it))
                }
                .onFailure {
                    showToast(context, it.throwable.message ?: "Error")
                }
        }
    }

    private suspend fun showToast(context: Context, value: String) {
        withContext(Dispatchers.Main) {
            Toast.makeText(
                context,
                value,
                Toast.LENGTH_LONG
            ).show()
        }
    }
}