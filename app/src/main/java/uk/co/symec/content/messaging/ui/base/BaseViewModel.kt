package uk.co.symec.content.messaging.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uk.co.symec.content.messaging.utils.extensions.launch
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.data.utils.CoroutineContextProvider
import javax.inject.Inject

abstract class BaseViewModel<T : Any> : ViewModel() {

    @Inject
    lateinit var coroutineContext: CoroutineContextProvider

    @Inject
    lateinit var connectivity: Connectivity

    var isConnectedNow = true

    protected val _viewState = MutableLiveData<ViewState<T>>()
    val viewState: LiveData<ViewState<T>>
        get() = _viewState

    //only for old data
    val _viewStatePrevious = MutableLiveData<ViewState<T>>()
    val viewStatePrevious: LiveData<ViewState<T>>
        get() = _viewStatePrevious

    protected fun executeUseCase(action: suspend () -> Unit, noInternetAction: () -> Unit) {
        _viewState.value = Loading()
        if (connectivity.hasNetworkAccess()) {
            launch { action() }
        } else {
            noInternetAction()
        }
    }

    protected fun executeUseCase(action: suspend () -> Unit) {
        _viewState.value = Loading()
        launch { action() }
    }

    inline fun onceOnly(crossinline block: (ViewState<T>) -> Unit) {
        viewState.value?.let { viewState ->
            val status = getStatus(viewState)
            if (viewStatePrevious.value == null) {
                block(viewState)
            } else {
                viewStatePrevious.value?.let {
                    if (status != getStatus(it)) {
                        block(viewState)
                    }
                }
            }
        }
    }

    fun isConnected(): Boolean {
        return connectivity.hasNetworkAccess()
    }
}