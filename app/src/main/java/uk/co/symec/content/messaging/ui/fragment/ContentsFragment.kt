package uk.co.symec.content.messaging.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.ContentsListFragmentBinding
import uk.co.symec.content.messaging.di.AppModule
import uk.co.symec.content.messaging.ui.activity.ContentsActivity
import uk.co.symec.content.messaging.ui.adapter.ContentListAdapter
import uk.co.symec.content.messaging.ui.adapter.ContentsListener
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.utils.extensions.snackbar
import uk.co.symec.content.messaging.vm.ContentsViewModel
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.model.ContentModel

@AndroidEntryPoint
class ContentsFragment : BaseFragment<ContentsListFragmentBinding>() {

    val viewModel: ContentsViewModel by viewModels()
    private var _binding: ContentsListFragmentBinding? = null
    private val binding get() = _binding!!

    var contentList: List<ContentModel> = mutableListOf()
    private var isInternal = false
    private var showDetailsForGuid: String? = null

    lateinit var adapter: ContentListAdapter

    override fun viewReady() {
        subscribeToData()
    }

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isInternal = arguments?.getBoolean(Constants.IS_INTERNAL) ?: false
        showDetailsForGuid = arguments?.getString(Constants.CONTENT_ID, null)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = ContentsListFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvContentList.layoutManager = LinearLayoutManager(activity)
        binding.rvContentList.setHasFixedSize(true)
        adapter = ContentListAdapter(ContentsListener(clickListener = {
            val fragment = DetailsBottomFragment()
            val bundle = Bundle()
            bundle.putString(Constants.CONTENT_ID, it)
            fragment.arguments = bundle
            requireActivity().supportFragmentManager.beginTransaction().add(fragment, "details")
                .commitNowAllowingStateLoss()

        }, shareListener = {
            shareContent(it)
        }))

        GlobalScope.launch(context = Dispatchers.Main) {
            binding.rvContentList.adapter = adapter
            subscribeToData()
            getContents()
        }
    }

    override fun onResume() {
        super.onResume()
        if (AppModule.getWasInBackground()) {
            AppModule.setUpdateInternalList(true)
            AppModule.setUpdateExternalList(true)
            AppModule.resetState(AppModule.State.FOREGROUND)
        }

        if (isInternal) {
            if (AppModule.getUpdateInternalList()) {
                getContents()
                AppModule.setUpdateInternalList(false)
            }
        } else {
            if (AppModule.getUpdateExternalList()) {
                getContents()
                AppModule.setUpdateExternalList(false)
            }
        }
    }

    fun getContents(showDetailsForGuid: String? = null) {
        with(getSharedPreferences()) {
            val deviceId = getString(Constants.ASSET_ID, "") ?: ""
            if (showDetailsForGuid != null) {
                this@ContentsFragment.showDetailsForGuid = showDetailsForGuid
            }
            viewModel.getContents(
                deviceId,
                isInternal,
                this@ContentsFragment.showDetailsForGuid,
                1
            )
            viewModel.getContentsDb(isInternal)
        }
    }

    fun updateContent(contentId: String, isInternal: Boolean, updateList: Boolean) {
        with(getSharedPreferences()) {
            val deviceId = getString(Constants.ASSET_ID, "") ?: ""
            viewModel.updateContent(deviceId, contentId, isInternal, updateList)
        }
    }

    fun deleteContent(contentId: String, isInternal: Boolean, updateList: Boolean) {
        viewModel.deleteContent(contentId, isInternal, updateList)
    }

    private fun subscribeToData() {
        viewModel.viewState.observe(viewLifecycleOwner, { vs ->
            handleViewState(vs)
        })

        viewModel.mediaState.observe(viewLifecycleOwner, { vs ->
            handleMediaState(vs)
        })
    }

    private fun handleViewState(viewState: ViewState<List<ContentModel>>) {
        when (viewState) {
            is Loading -> showLoading(binding.loading)
            is Success -> listLoaded(viewState.data)
            is SuccessWithDownload -> {
                viewModel.onceOnly {
                    downloadFilesList(viewState.data)
                }
            }
            is SuccessHideLoading -> {
                hideLoading(binding.loading)
                if (viewState.data.isInternal) {
                    AppModule.setUpdateInternalList(true)
                } else {
                    AppModule.setUpdateExternalList(true)
                }
            }
            is SuccessWithShowDetails -> {
                if (showDetailsForGuid != null) {
                    listLoaded(viewState.data)
                    val fragment = DetailsBottomFragment()
                    val bundle = Bundle()
                    bundle.putString(Constants.CONTENT_ID, viewState.guid)
                    fragment.arguments = bundle
                    requireActivity().supportFragmentManager.beginTransaction()
                        .add(fragment, "details")
                        .commitNowAllowingStateLoss()
                    showDetailsForGuid = null
                }
            }
            is SuccessButNoInternet -> {
                showNoInternetError()
                listLoaded(viewState.data)
            }
            is SuccessButConnectionError -> {
                showOtherError(
                    viewState.data.httpError.throwable.localizedMessage ?: "Default error message"
                )
                listLoaded(viewState.data.data)
            }
            is Logout -> logoutDialog(
                resources.getString(R.string.dialog_user_was_logged_out),
                true
            )
            is Archived -> logoutDialog(
                resources.getString(R.string.dialog_device_has_been_archived),
                false
            )
            is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
            is NoInternetState -> showNoInternetError()
            else -> {
            }
        }
        viewModel._viewStatePrevious.value = viewState
    }

    private fun handleMediaState(viewState: ViewState<Boolean>) {
        when (viewState) {
            is Success -> {
                hideLoading(binding.loading)
                //snackbar("Files downloaded", binding.root)
            }
            else -> {
            }
            //is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
        }
    }

    private fun listLoaded(list: List<ContentModel>) {
        hideLoading(binding.loading)
        adapter.setContentsList(list)
        contentList = list

        if (list.isEmpty()) binding.emptyView.visibility =
            View.VISIBLE else binding.emptyView.visibility = View.GONE

        //update isNew indicator
        var show = false
        list.forEach {
            if (it.status == 0) {
                show = true
                return@forEach
            }
        }
        (requireActivity() as? ContentsActivity)?.let {
            it.updateTab(it, show, isInternal)
        }
    }

    private fun downloadFilesList(list: List<ContentModel>) {
        hideLoading(binding.loading)
        list.forEach {
            viewModel.getMedias(it)
        }
    }

    private fun showNoInternetError() {
        try {
            hideLoading(binding.loading)
            snackbar(getString(R.string.no_internet_error_message), binding.root)
        } catch (e: IllegalArgumentException) {
            println(e.localizedMessage)
        }
    }

    private fun showOtherError(errorMsg: String) {
        try {
            hideLoading(binding.loading)
            snackbar(errorMsg, binding.root)
        } catch (e: IllegalArgumentException) {
            println(e.localizedMessage)
        }
    }

    private fun handleError(error: String) {
        try {
            hideLoading(binding.loading)
            showError(error, binding.root)
        } catch (e: IllegalArgumentException) {
            println(e.localizedMessage)
        }
    }

    private fun logoutDialog(title: String, deleteData: Boolean) {
        if (deleteData) {
            with(getSharedPreferences().edit()) {
                putString(Constants.TOKEN, null)
                putString(Constants.ANDROID_ID, null)
                putString(Constants.ASSET_ID, null)
                putString(Constants.GROUP_ID, null)
                apply()
            }
        }

        val alertDialog: AlertDialog = let {
            val builder = AlertDialog.Builder(requireContext())
            builder.apply {
                setTitle(title)
                setCancelable(false)
                setPositiveButton(
                    R.string.ok
                ) { dialog, _ ->
                    dialog.dismiss()
                    requireActivity().finishAndRemoveTask()
                    Intent(Intent.ACTION_MAIN).apply {
                        addCategory(Intent.CATEGORY_HOME)
                        flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(this)
                    }
                }
            }
            builder.create()
        }
        alertDialog.show()
    }

    companion object {
        fun newInstance(isInternal: Boolean, isShowDetailsFor: String?): ContentsFragment {
            val fragmentFirst = ContentsFragment()
            val bundle = Bundle()
            bundle.putBoolean(Constants.IS_INTERNAL, isInternal)
            bundle.putString(Constants.CONTENT_ID, isShowDetailsFor)
            fragmentFirst.arguments = bundle
            return fragmentFirst
        }
    }
}