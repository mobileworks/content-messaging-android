package uk.co.symec.content.messaging.ui.activity

import android.annotation.SuppressLint
import android.content.*
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.content.messaging.BuildConfig
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.RegisterToGroupActivityBinding
import uk.co.symec.content.messaging.fcm.MyFirebaseMessagingService
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.ui.fragment.RegisterAssetIdFragment
import uk.co.symec.content.messaging.ui.fragment.RegisterGroupFragment
import uk.co.symec.content.messaging.utils.extensions.getSharedPreferences
import uk.co.symec.content.messaging.utils.extensions.snackbar
import uk.co.symec.content.messaging.utils.extensions.subscribe
import uk.co.symec.content.messaging.vm.RegisterToGroupViewModel
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.model.RegisterToGroupModel
import javax.inject.Inject

private const val ASSET_ID = "assetID"
private const val GROUP_ID = "GroupID"

@AndroidEntryPoint
class RegisterToGroupActivity : BaseActivity() {

    private val viewModel: RegisterToGroupViewModel by viewModels()
    lateinit var binding: RegisterToGroupActivityBinding

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var crashlytics: FirebaseCrashlytics

    var assetId: String? = null
    var groupId: String? = null
    var token: String? = null

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        with(getSharedPreferences()) {
            assetId = getString(Constants.ASSET_ID, null)
            groupId = getString(Constants.GROUP_ID, null)
            token = getString(Constants.TOKEN, null)
        }

        if (assetId != null && groupId != null && token != null) {
            Intent(this, ContentsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent?.extras?.let { extras ->
                    //when click from push notification app in background
                    val action = extras.getString(MyFirebaseMessagingService.ACTION, null)
                    val contentGuid =
                        extras.getString(MyFirebaseMessagingService.CONTENT_GUID, null)
                    val isInternal = extras.getString(
                        MyFirebaseMessagingService.IS_INTERNAL,
                        "false"
                    )
                    val isInternalBoolean = isInternal.toBoolean()
                    if (contentGuid != null && action != null) {
                        putExtra(MyFirebaseMessagingService.ACTION, action)
                        putExtra(MyFirebaseMessagingService.IS_INTERNAL, isInternalBoolean)
                        putExtra(MyFirebaseMessagingService.CONTENT_GUID, contentGuid)
                    }
                }
                startActivity(this)
                return
            }
        }

        binding = RegisterToGroupActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        binding.vpRegister.adapter = RegisterAdapter(this)
        binding.vpRegister.isUserInputEnabled = false

        viewModel.model = "${Build.MANUFACTURER} ${Build.MODEL}"
        viewModel.androidId = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )

//        //for tests
//        viewModel.groupId = "7e22a6eb-a5fe-472e-a9d8-8164f487e469"
//        with(getSharedPreferences().edit()) {
//            putString(Constants.GROUP_ID, viewModel.groupId)
//            apply()
//        }
//        binding.vpRegister.setCurrentItem(1, true)
//        binding.tvSave.text = resources.getString(R.string.txt_save_settings)
//
//        //for tests

        val myRestrictionsMgr =
            getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager
        val appRestrictions: Bundle = myRestrictionsMgr.applicationRestrictions
        if (appRestrictions.containsKey(GROUP_ID)) {
            val groupId = appRestrictions.getString(GROUP_ID, null)
            if (groupId != null) {
                viewModel.groupId = groupId
                with(getSharedPreferences().edit()) {
                    putString(Constants.GROUP_ID, groupId)
                    apply()
                }
                binding.vpRegister.setCurrentItem(1, true)
                binding.tvSave.text = resources.getString(R.string.txt_save_settings)
            }
        }

        binding.btnLogIn.setOnClickListener {
            if (binding.vpRegister.currentItem == 0) {
                //first group page
                if (viewModel.groupId.isNotBlank()) {
                    binding.vpRegister.setCurrentItem(1, true)
                    binding.tvSave.text = resources.getString(R.string.txt_save_settings)
                } else {
                    viewModel.validationGroupId()
                    Toast.makeText(
                        this,
                        resources.getString(R.string.missing_group_id),
                        Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                //second assetId page
                if (viewModel.fcm.isBlank()) {
                    FirebaseMessaging.getInstance().token.addOnCompleteListener OnCompleteListener@{ task ->
                        if (!task.isSuccessful) {
                            println("Fetching FCM registration token failed ${task.exception}")
                            Toast.makeText(
                                this,
                                R.string.no_internet_error_message,
                                Toast.LENGTH_LONG
                            ).show()
                            return@OnCompleteListener
                        }

                        // Get new FCM registration token
                        println("Token ${task.result}")
                        viewModel.fcm = task.result
                        with(getSharedPreferences().edit()) {
                            putString(Constants.FCM, task.result)
                            apply()
                        }
                        viewModel.validationAssetId()
                    }
                } else {
                    viewModel.validationAssetId()
                }
            }
        }

        intent?.extras?.let { extras ->
            //soti
            val assetID = extras.getString(ASSET_ID, null)
            if (assetID != null) {
                viewModel.assetId = assetID
                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        println("Fetching FCM registration token failed ${task.exception}")
                        return@OnCompleteListener
                    }

                    // Get new FCM registration token
                    println("Token ${task.result}")
                    viewModel.fcm = task.result
                    with(getSharedPreferences().edit()) {
                        putString(Constants.FCM, task.result)
                        apply()
                    }
                    viewModel.validationAssetId()
                })
            }
        }

        if (BuildConfig.DEBUG) {
            viewModel.groupId = "004a7ced-f2ec-42ae-a0ff-a94714d9f508"
           // viewModel.groupId = "7e22a6eb-a5fe-472e-a9d8-8164f487e469"
            binding.vpRegister.setCurrentItem(1, true)
            binding.tvSave.text = resources.getString(R.string.txt_save_settings)
        }

        subscribeToData()
    }

    override fun onResume() {
        super.onResume()
        if (!(assetId != null && groupId != null && token != null)) {
            val myRestrictionsMgr =
                getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager
            val appRestrictions: Bundle = myRestrictionsMgr.applicationRestrictions
            if (appRestrictions.containsKey(GROUP_ID)) {
                val groupId = appRestrictions.getString(GROUP_ID, null)
                if (groupId != null) {
                    viewModel.groupId = groupId
                    with(getSharedPreferences().edit()) {
                        putString(Constants.GROUP_ID, groupId)
                        apply()
                    }
                    binding.vpRegister.setCurrentItem(1, true)
                    binding.tvSave.text = resources.getString(R.string.txt_save_settings)
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        intent?.extras?.let { extras ->
            //when app in background and clicked on push notification
            val action = extras.getString(MyFirebaseMessagingService.ACTION, null)
            val contentGuid = extras.getString(MyFirebaseMessagingService.CONTENT_GUID, null)
            val isInternal = extras.getString(MyFirebaseMessagingService.IS_INTERNAL, "false")
            val isInternalBoolean = isInternal.toBoolean()
            if (contentGuid != null && action != null) {
                val intentMenu = Intent(this, ContentsActivity::class.java).apply {
                    putExtra(MyFirebaseMessagingService.ACTION, action)
                    putExtra(MyFirebaseMessagingService.IS_INTERNAL, isInternalBoolean)
                    putExtra(MyFirebaseMessagingService.CONTENT_GUID, contentGuid)
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK
                }
                startActivity(intentMenu)
                return
            }
        }

        val assetId: String?
        val groupId: String?
        val token: String?
        with(getSharedPreferences()) {
            assetId = getString(Constants.ASSET_ID, null)
            groupId = if (viewModel.groupId.isBlank()) {
                getString(Constants.GROUP_ID, null)
            } else {
                viewModel.groupId
            }
            token = getString(Constants.TOKEN, null)
        }

        if (assetId != null && groupId != null && token != null) {
            Intent(this, ContentsActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK
                intent?.extras?.let { extras ->
                    //when click from push notification app in background
                    val action = extras.getString(MyFirebaseMessagingService.ACTION, null)
                    val contentGuid =
                        extras.getString(MyFirebaseMessagingService.CONTENT_GUID, null)
                    val isInternal =
                        extras.getBoolean(MyFirebaseMessagingService.IS_INTERNAL, false)
                    if (contentGuid != null && action != null) {
                        putExtra(MyFirebaseMessagingService.ACTION, action)
                        putExtra(MyFirebaseMessagingService.IS_INTERNAL, isInternal)
                        putExtra(MyFirebaseMessagingService.CONTENT_GUID, contentGuid)
                    }
                }
                startActivity(this)
                return
            }
        } else if (groupId != null) {
            //first SOTI login
            intent?.extras?.let { extras ->
                //soti
                val assetID = extras.getString(ASSET_ID, null)
                if (assetID != null) {
                    viewModel.assetId = assetID
                    FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            println("Fetching FCM registration token failed ${task.exception}")
                            return@OnCompleteListener
                        }

                        // Get new FCM registration token
                        println("Token ${task.result}")
                        viewModel.fcm = task.result
                        with(getSharedPreferences().edit()) {
                            putString(Constants.FCM, task.result)
                            apply()
                        }
                        viewModel.validationAssetId()
                    })
                }
            }
        } else {
            finish()
            startActivity(getIntent())
        }
    }

    private fun subscribeToData() {
        viewModel.viewState.subscribe(this, ::handleViewState)
    }

    private fun handleViewState(viewState: ViewState<RegisterToGroupModel>) {
        when (viewState) {
            is Loading -> showLoading(binding.loading)
            is Success -> registerSuccessful(viewState.data)
            is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
            is NoInternetState -> showNoInternetError()
            is Logout -> logoutDialog(resources.getString(R.string.dialog_user_was_logged_out), true)
            is Archived -> logoutDialog(resources.getString(R.string.dialog_device_has_been_archived), false)
            else -> {
            }
        }
    }

    private fun registerSuccessful(model: RegisterToGroupModel) {
        hideLoading(binding.loading)
        with(getSharedPreferences().edit()) {
            putString(Constants.TOKEN, model.token)
            putString(Constants.GROUP_NAME, model.groupName)
            putString(Constants.ANDROID_ID, viewModel.androidId)
            putString(Constants.ASSET_ID, viewModel.assetId)
            putString(Constants.GROUP_ID, viewModel.groupId)
            apply()
        }

        addAnalytics(firebaseAnalytics, crashlytics, viewModel)

        Intent(this, ContentsActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK and Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(this)
            return
        }
    }

    private fun addAnalytics(
        firebaseAnalytics: FirebaseAnalytics,
        crashlytics: FirebaseCrashlytics,
        viewModel: RegisterToGroupViewModel
    ) {
        crashlytics.apply {
            setCustomKey(Constants.IS_DEVELOPER, BuildConfig.DEBUG)
            setCustomKey(Constants.ASSET_ID, viewModel.assetId)
            setCustomKey(Constants.GROUP_ID, viewModel.groupId)
            setCustomKey(Constants.IS_OFFLINE, "${viewModel.isConnected()}")
            setUserId(viewModel.androidId)
        }
        firebaseAnalytics.apply {
            setUserProperty(Constants.IS_DEVELOPER, java.lang.String.valueOf(BuildConfig.DEBUG))
            setUserProperty(Constants.ANDROID_ID, viewModel.androidId)
            setUserProperty(Constants.ASSET_ID, viewModel.assetId)
            setUserProperty(Constants.GROUP_ID, viewModel.groupId)
            setUserProperty(Constants.IS_OFFLINE, "${viewModel.isConnected()}")
            setUserId(viewModel.androidId)
        }
    }

    private fun handleError(error: String) {
        hideLoading(binding.loading)
        showError(error, binding.root)
    }

    private fun showNoInternetError() {
        hideLoading(binding.loading)
        snackbar(getString(R.string.no_internet_error_message), binding.root)
    }

    override fun onBackPressed() {
        //do nothing
    }

    private fun logoutDialog(title: String, deleteData : Boolean) {
        if (deleteData) {
            with(getSharedPreferences().edit()) {
                putString(Constants.TOKEN, null)
                putString(Constants.ANDROID_ID, null)
                putString(Constants.ASSET_ID, null)
                putString(Constants.GROUP_ID, null)
                apply()
            }
        }

        val alertDialog: AlertDialog = let {
            val builder = AlertDialog.Builder(this)
            builder.apply {
                setTitle(title)
                setCancelable(false)
                setPositiveButton(
                    R.string.ok
                ) { dialog, _ ->
                    dialog.dismiss()
                    finishAndRemoveTask()
                    Intent(Intent.ACTION_MAIN).apply {
                        addCategory(Intent.CATEGORY_HOME)
                        flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(this)
                    }
                }
            }
            builder.create()
        }
        alertDialog.show()
    }

    private inner class RegisterAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

        override fun getItemCount(): Int = 2

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> RegisterGroupFragment.newInstance()
                else -> RegisterAssetIdFragment.newInstance()
            }
        }
    }
}