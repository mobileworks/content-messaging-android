package uk.co.symec.content.messaging.ui.fragment

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.ConfirmationFragmentBinding
import uk.co.symec.content.messaging.ui.base.Error
import uk.co.symec.content.messaging.ui.base.Success
import uk.co.symec.content.messaging.ui.base.ViewState
import uk.co.symec.content.messaging.vm.ConfirmationViewModel
import uk.co.symec.content.messaging.vm.ContentActivityViewModel
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.model.StatementModel


@AndroidEntryPoint
class ConfirmationFragment : BottomSheetDialogFragment() {

    private val viewModel: ConfirmationViewModel by viewModels()
    private val parentViewModel: ContentActivityViewModel by activityViewModels()
    private var _binding: ConfirmationFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var contentId: String

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contentId = arguments?.getString(Constants.CONTENT_ID) ?: ""
        if (parentViewModel.confirmationText.isBlank()) {
            NetworkModule.getAssetId(requireContext())?.let {
                parentViewModel.getStatement(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment
        _binding = ConfirmationFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setText(parentViewModel.confirmationText)

        binding.ivClose.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
        }

        binding.signature.isLatestSignatureReady.observe(viewLifecycleOwner, {
            if (it) {
                val path = NetworkModule.getSignatureFolder(requireContext())
                viewModel.getFile(binding.signature.bitmap!!, path)
            }
        })

        binding.ivDelete.setOnClickListener {
            val alertDialog: AlertDialog = let {
                AlertDialog.Builder(requireContext()).apply {
                    setTitle(R.string.dialog_delete_signature)
                    setCancelable(false)
                    setNegativeButton(R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                    }
                    setPositiveButton(
                        R.string.ok
                    ) { dialog, _ ->
                        dialog.dismiss()
                        binding.signature.initializeSignature()
                        viewModel.resetSignature()
                    }
                }.create()
            }
            alertDialog.show()
        }

        binding.clSave.setOnClickListener {
            if (viewModel.fileResult != null) {
                NetworkModule.getAssetId(requireContext())?.let {
                    viewModel.sendSignature(viewModel.fileResult!!, viewModel.userName, it, contentId)
                }
            } else {
                Toast.makeText(requireContext(), R.string.signature_error, Toast.LENGTH_LONG).show()
            }
        }

        viewModel.isButtonEnabled.observe(viewLifecycleOwner, {
            if (it) {
                binding.clSave.apply {
                    background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.button_shape_share)
                }
            } else {
                binding.clSave.apply {
                    background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.button_shape_save)
                }
            }
        })

        viewModel.signState.observe(viewLifecycleOwner, {
            when (it) {
                is Success -> {
                    requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
                    parentViewModel.updateSignature(contentId)
                }
                else -> {
                    Toast.makeText(requireContext(), R.string.signature_error_toast, Toast.LENGTH_LONG).show()
                }
            }
        })

        parentViewModel.statementState.observe(this, { vs ->
            handleStatementState(vs)
        })

        dialog?.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
        }
    }

    private fun setText(text: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.tvContent.text =
                Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            binding.tvContent.text = Html.fromHtml(text)
        }
    }

    private fun handleStatementState(viewState: ViewState<StatementModel>) {
        when (viewState) {
            is Success -> {
                parentViewModel.confirmationText = viewState.data.statement ?: getString(R.string.confirmation_text)
                setText(parentViewModel.confirmationText)
            }
            is Error -> {
                parentViewModel.confirmationText = getString(R.string.confirmation_text)
                setText(parentViewModel.confirmationText)
            }
            else -> {
            }
        }
    }
}