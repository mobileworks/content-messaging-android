package uk.co.symec.content.messaging.ui.activity

import android.app.SearchManager
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.os.StatFs
import android.os.storage.StorageManager
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import uk.co.symec.content.messaging.BuildConfig
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.ContentsListActivityBinding
import uk.co.symec.content.messaging.fcm.MyFirebaseMessagingService
import uk.co.symec.content.messaging.ui.adapter.AdapterListener
import uk.co.symec.content.messaging.ui.adapter.AlertDialogAdapter
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.ui.fragment.ContentsFragment
import uk.co.symec.content.messaging.ui.fragment.DetailsBottomFragment
import uk.co.symec.content.messaging.utils.TabUtils
import uk.co.symec.content.messaging.utils.extensions.getSharedPreferences
import uk.co.symec.content.messaging.vm.ContentActivityViewModel
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.StatementModel
import java.util.*
import javax.inject.Inject
import kotlin.math.min

private const val NUM_PAGES = 2

@AndroidEntryPoint
class ContentsActivity : BaseActivity() {

    val viewModel: ContentActivityViewModel by viewModels()
    lateinit var binding: ContentsListActivityBinding

    private var textCartItemCount: TextView? = null

    private var latestContentList: List<ContentModel> = mutableListOf()
    private var quarriedList: MutableList<ContentModel> = ArrayList()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    @Inject
    lateinit var crashlytics: FirebaseCrashlytics

    var showDetailsForGuid: String? = null
    var isInternal = false

    var tabList = mutableListOf<TabUtils>()

    @DelicateCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ContentsListActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.lifecycleOwner = this
        binding.vpContentList.adapter = PagerAdapter(this)

        GlobalScope.launch(context = Dispatchers.Main) {
            tabList.add(TabUtils(resources.getString(R.string.external_tab), false))
            tabList.add(TabUtils(resources.getString(R.string.internal_tab), false))

            TabLayoutMediator(
                binding.tlContentTab,
                binding.vpContentList
            ) { tab, _ -> // Styling each tab here
                tab.setCustomView(R.layout.view_toggle_button)
            }.attach()

            tabOperations(binding.tlContentTab)

            subscribeToData()
            intent?.let { intent ->
                val action = intent.getStringExtra(MyFirebaseMessagingService.ACTION)
                val contentGuid = intent.getStringExtra(MyFirebaseMessagingService.CONTENT_GUID)
                isInternal = intent.getBooleanExtra(MyFirebaseMessagingService.IS_INTERNAL, false)
                if (contentGuid != null && action != null) {
                    if (action.contentEquals(MyFirebaseMessagingService.UPDATE) || action.contentEquals(
                            MyFirebaseMessagingService.CREATE
                        )
                    ) {
                        showDetailsForGuid = contentGuid
                        if (isInternal) {
                            //internal
                            if (binding.tlContentTab.selectedTabPosition == 0 || binding.tlContentTab.selectedTabPosition == -1) {
                                //change tab
                                binding.tlContentTab.selectTab(binding.tlContentTab.getTabAt(1))
                            }
                            getCurrentFragment()?.getContents(contentGuid)
                        } else {
                            //external
                            if (binding.tlContentTab.selectedTabPosition == 1 || binding.tlContentTab.selectedTabPosition == -1) {
                                //change tab
                                binding.tlContentTab.selectTab(binding.tlContentTab.getTabAt(0))
                            }
                            getCurrentFragment()?.getContents(contentGuid)
                        }
                    } else if (action.contentEquals(MyFirebaseMessagingService.DELETE)) {
                        getCurrentFragment()?.getContents()
                    }
                }
            }
        }

        GlobalScope.launch(context = Dispatchers.Main) {
            with(getSharedPreferences()) {
                getString(Constants.ASSET_ID, null)?.let {
                    crashlytics.setCustomKey(Constants.ASSET_ID, it)
                    firebaseAnalytics.setUserProperty(Constants.ASSET_ID, it)

                    viewModel.getStatement(it)
                }
                getString(Constants.GROUP_ID, null)?.let {
                    crashlytics.setCustomKey(Constants.GROUP_ID, it)
                    firebaseAnalytics.setUserProperty(Constants.GROUP_ID, it)
                }
                getString(Constants.ANDROID_ID, null)?.let {
                    crashlytics.setUserId(it)
                    firebaseAnalytics.setUserProperty(Constants.ANDROID_ID, it)
                    firebaseAnalytics.setUserId(it)
                }

                crashlytics.setCustomKey(Constants.IS_DEVELOPER, BuildConfig.DEBUG)
                crashlytics.setCustomKey(Constants.IS_OFFLINE, "${viewModel.isConnected()}")

                firebaseAnalytics.setUserProperty(
                    Constants.IS_DEVELOPER, BuildConfig.DEBUG.toString()
                )

                firebaseAnalytics.setUserProperty(
                    Constants.IS_OFFLINE,
                    "${viewModel.isConnected()}"
                )
            }
        }
    }

    private fun subscribeToData() {
        viewModel.viewState.observe(this, { vs ->
            handleViewState(vs)
        })

        viewModel.statementState.observe(this, { vs ->
            handleStatementState(vs)
        })
    }

    private var updateContentReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //when app on this activity and notification arrives
            val action = intent.extras?.getString(MyFirebaseMessagingService.ACTION, "")
            val contentGuid = intent.extras?.getString(MyFirebaseMessagingService.CONTENT_GUID, "")
            val isInternal =
                intent.extras?.getString(MyFirebaseMessagingService.IS_INTERNAL, "false")
            val isInternalBoolean = isInternal.toBoolean()
            if (contentGuid != null && action != null) {
                val updateList = when (binding.tlContentTab.selectedTabPosition) {
                    0 -> !isInternalBoolean
                    else -> isInternalBoolean
                }
                if (action.contentEquals(MyFirebaseMessagingService.UPDATE) || action.contentEquals(
                        MyFirebaseMessagingService.CREATE
                    )
                ) {
                    getCurrentFragment()?.updateContent(contentGuid, isInternalBoolean, updateList)
                } else if (action.contentEquals(MyFirebaseMessagingService.DELETE)) {
                    getCurrentFragment()?.deleteContent(contentGuid, isInternalBoolean, updateList)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(
            updateContentReceiver,
            IntentFilter(MyFirebaseMessagingService.CONTENT_UPDATE)
        )
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(updateContentReceiver)
    }

    private fun handleViewState(viewState: ViewState<List<ContentModel>>) {
        when (viewState) {
            is Success -> listLoaded(viewState.data)
            is Error -> handleError(viewState.error.localizedMessage ?: "Default error message")
            else -> {
            }
        }
    }

    private fun handleStatementState(viewState: ViewState<StatementModel>) {
        when (viewState) {
            is Success -> {
                viewModel.confirmationText =
                    viewState.data.statement ?: getString(R.string.confirmation_text)
            }
            is Error -> {
                viewModel.confirmationText = getString(R.string.confirmation_text)
                handleError(viewState.error.localizedMessage ?: "Default error message")
            }
            else -> {
            }
        }
    }

    fun updateTab(
        activity: ContentsActivity,
        show: Boolean, isInternal: Boolean
    ) {
        val index = when (isInternal) {
            false -> 0
            true -> 1
        }
        try {
            activity.tabList[index].isNew = show
            viewModel.getLatestContents()
            activity.binding.tlContentTab.getTabAt(index)?.let { tab ->
                tabHandling(tab, show)
            }
        } catch (e: Exception) {
            //
        }
    }

    private fun tabHandling(tab: TabLayout.Tab, show: Boolean) {
        tab.customView?.apply {
            if (tab.isSelected) {
                findViewById<ImageView>(R.id.iv_selected)?.apply {
                    visibility = if (show) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
            } else {
                findViewById<ImageView>(R.id.iv_unselected)?.apply {
                    visibility = if (show) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
            }
        }
    }

    private fun listLoaded(list: List<ContentModel>) {
        latestContentList = list
        println("Latest list size = ${latestContentList.size}")
        var counter = 0
        list.forEach {
            if (it.status == 0) {
                counter++
            }
        }
        setupBadge(counter)
    }

    private fun handleError(error: String) {
        showError(error, binding.root)
    }

    private fun tabOperations(tabs: TabLayout) {
        if (isInternal) {
            initialSettings(1, 0, tabs)
        } else {
            initialSettings(0, 1, tabs)
        }

        tabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                TabUtils.selectTab(tab, tabList[tab.position])
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                TabUtils.unSelectTab(tab, tabList[tab.position])
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
            }
        })
    }

    private fun initialSettings(select: Int, unselect: Int, tabs: TabLayout) {
        tabs.getTabAt(unselect)?.let {
            TabUtils.unSelectTab(it, tabList[unselect])
        }
        tabs.getTabAt(select)?.let {
            tabs.selectTab(it)
            TabUtils.selectTab(it, tabList[select])
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val menuItem = menu.findItem(R.id.action_ring)
        val actionView: View = menuItem.actionView
        textCartItemCount = actionView.findViewById(R.id.cart_badge) as TextView
        setupBadge(0)

        val manager = getSystemService(SEARCH_SERVICE) as SearchManager
        val search: SearchView = menu.findItem(R.id.menu_search).actionView as SearchView
        search.setSearchableInfo(manager.getSearchableInfo(componentName))

        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                TODO("Not yet implemented")
            }

            override fun onQueryTextChange(query: String): Boolean {
                if (query.length > 1) {
                    quarriedList.clear()
                    getCurrentFragment()?.let {
                        for (item: ContentModel in it.contentList) {
                            item.title?.let { title ->
                                if (title.toLowerCase(Locale.getDefault()).contains(query)) {
                                    quarriedList.add(item)
                                }
                            }
                        }
                        it.adapter.setQueriedList(quarriedList)
                    }
                } else {
                    getCurrentFragment()?.let {
                        it.adapter.setContentsList(it.contentList)
                    }
                }
                return true
            }
        })

        actionView.setOnClickListener { onOptionsItemSelected(menuItem) }
        return true
    }

    fun getCurrentFragment(): ContentsFragment? {
        supportFragmentManager.findFragmentByTag("f" + binding.vpContentList.currentItem)
            ?.let { selectedFragment ->
                (selectedFragment as? ContentsFragment)?.let {
                    println("Current fragment ${binding.vpContentList.currentItem}")
                    return it
                }
            }
        println("Current fragment null")
        return null
    }

    private fun setupBadge(counter: Int) {
        textCartItemCount?.apply {
            if (counter == 0) {
                if (visibility != View.GONE) {
                    visibility = View.GONE
                }
            } else {
                text = "${min(counter, 99)}"
                if (visibility != View.VISIBLE) {
                    visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_ring -> {
                customAlertDialog()
                return true
            }
            R.id.action_info -> {
                infoAlertDialog()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun customAlertDialog() {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.custom_alert_dialog, null)
        dialogBuilder.setView(dialogView)
        val alertDialog: AlertDialog = dialogBuilder.create().apply {
            window?.apply {
                setGravity(Gravity.TOP)
                setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
        }
        val list: RecyclerView = dialogView.findViewById(R.id.rv_dialog_list)
        list.layoutManager = LinearLayoutManager(this)
        list.setHasFixedSize(true)

        val adapter = AlertDialogAdapter(
            latestContentList,
            AdapterListener(clickListener1 = {
                val fragment = DetailsBottomFragment()
                val bundle = Bundle()
                bundle.putString(Constants.CONTENT_ID, it)
                fragment.arguments = bundle
                supportFragmentManager.beginTransaction().add(fragment, it)
                    .commitNowAllowingStateLoss()
            })
        )

        list.adapter = adapter

        alertDialog.show()
    }

    private fun infoAlertDialog() {
        val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.info_alert_dialog, null)
        dialogBuilder.setView(dialogView)
        val alertDialog: AlertDialog = dialogBuilder.create().apply {
            window?.apply {
                setGravity(Gravity.TOP)
                setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
        }

        dialogView.findViewById<TextView>(R.id.tv_version).apply {
            try {
                val pInfo: PackageInfo = this@ContentsActivity.packageManager.getPackageInfo(
                    applicationContext.packageName,
                    0
                )
                text = resources.getString(R.string.app_version, pInfo.versionName)
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        }

        dialogView.findViewById<TextView>(R.id.tv_group_id).apply {
            NetworkModule.getGroupId(context)?.let { groupId ->
                NetworkModule.getGroupName(context)?.let {
                    text = resources.getString(R.string.group_id_label, it, groupId)
                }
            }
        }

        dialogView.findViewById<TextView>(R.id.tv_asset_id).apply {
            NetworkModule.getAssetId(context)?.let {
                text = resources.getString(R.string.asset_id_label, it)
            }
        }

        alertDialog.show()
    }

    override fun onBackPressed() {
        //do nothing
    }

    private inner class PagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment {
            var showGuid: String? = null
            if (isInternal && position == 1) {
                showGuid = showDetailsForGuid
            } else if (!isInternal && position == 0) {
                showGuid = showDetailsForGuid
            }
            showDetailsForGuid = null
            return when (position) {
                0 -> ContentsFragment.newInstance(false, showGuid)
                else -> ContentsFragment.newInstance(true, showGuid)
            }
        }
    }
}