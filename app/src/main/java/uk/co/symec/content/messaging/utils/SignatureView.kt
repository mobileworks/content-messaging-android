package uk.co.symec.content.messaging.utils

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlin.math.abs


class SignatureView : View {

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : this(context, attrs) {}

    val _isLatestSignatureReady = MutableLiveData<Boolean>()
    val isLatestSignatureReady: LiveData<Boolean>
        get() = _isLatestSignatureReady

    private val pencilPaint = Paint()
    private val fillPaint = Paint()
    private val dotPaint = Paint()
    private val textPaint = Paint()
    var bitmap: Bitmap? = null
    private var canvas: Canvas? = null
    private val mPath: Path = Path()
    private val mBitmapPaint: Paint = Paint(Paint.DITHER_FLAG)
    private var mX = 0f
    private var mY = 0f
    private var lineOffsetLeft = 0f
    private var lineOffsetRight = 0f
    var filename: String? = null
    var width1 = 0
    var height1 = 0
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        width1 = w
        height1 = h
        initializeSignature()
    }

    fun initializeSignature() {
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        canvas = Canvas(bitmap!!)
        val rect = RectF(0f, 0f, width.toFloat(), height.toFloat())
        canvas!!.drawRoundRect(rect, 20f, 20f, fillPaint)
        lineOffsetLeft = 1.0f * width / 10
        lineOffsetRight = lineOffsetLeft + 0.8f * width
        canvas!!.drawText("Signature", 30f, 70f, textPaint)
        this.invalidate()
    }

    public override fun onDraw(canvas: Canvas) {
        canvas.drawBitmap(bitmap!!, 0f, 0f, mBitmapPaint)
        canvas.drawPath(mPath, pencilPaint)
        enableDrawing = true
    }

    private fun touchStarted(x: Float, y: Float) {
        mPath.reset()
        mPath.moveTo(x, y)
        mX = x
        mY = y
    }

    private fun touchMoved(x: Float, y: Float) {
        val dx = abs(x - mX)
        val dy = abs(y - mY)
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2)
            mX = x
            mY = y
        }
    }

    private fun touchEnded() {
        if (mPath.isEmpty) {
            mPath.addCircle(mX, mY, 1f, Path.Direction.CW)
            canvas!!.drawPath(mPath, dotPaint)
        } else {
            mPath.lineTo(mX, mY)
            canvas!!.drawPath(mPath, pencilPaint)
        }
        mPath.reset()
        _isLatestSignatureReady.value = true
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val x = event.x
        val y = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchStarted(x, y)
            MotionEvent.ACTION_MOVE -> touchMoved(x, y)
            MotionEvent.ACTION_UP -> touchEnded()
        }
        this.invalidate()
        return true
    }

    companion object {
        private const val TOUCH_TOLERANCE = 4f
        var enableDrawing = false
    }

    init {
        pencilPaint.isAntiAlias = true
        pencilPaint.isDither = true
        pencilPaint.color = Color.BLACK
        pencilPaint.style = Paint.Style.STROKE
        pencilPaint.strokeJoin = Paint.Join.ROUND
        pencilPaint.strokeCap = Paint.Cap.ROUND
        pencilPaint.strokeWidth = 8f

        dotPaint.isAntiAlias = true
        dotPaint.isDither = true
        dotPaint.color = Color.BLACK
        dotPaint.style = Paint.Style.FILL_AND_STROKE
        dotPaint.strokeJoin = Paint.Join.ROUND
        dotPaint.strokeCap = Paint.Cap.ROUND
        dotPaint.strokeWidth = 8f

        fillPaint.isAntiAlias = true
        fillPaint.isDither = true
        fillPaint.color = Color.argb(12, 20, 25, 53)
        fillPaint.style = Paint.Style.FILL
        fillPaint.strokeJoin = Paint.Join.ROUND
        fillPaint.strokeCap = Paint.Cap.ROUND
        fillPaint.strokeWidth = 8f

        textPaint.isAntiAlias = true
        textPaint.isDither = true
        textPaint.color = Color.rgb(158,159,163)
        textPaint.textSize = resources.displayMetrics.scaledDensity * 17
    }
}