package uk.co.symec.content.messaging.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.StatementModel
import uk.co.symec.domain.model.onFailure
import uk.co.symec.domain.model.onSuccess
import uk.co.symec.domain.usecase.GetLatestContentsUseCase
import uk.co.symec.domain.usecase.GetStatementUseCase
import uk.co.symec.domain.usecase.UpdateContentSignatureUseCase
import javax.inject.Inject

@HiltViewModel
public class ContentActivityViewModel @Inject constructor(
    private val getLatestContentsUseCase: GetLatestContentsUseCase,
    private val updateContentSignatureUseCase: UpdateContentSignatureUseCase,
    private val getStatementUseCase: GetStatementUseCase
): BaseViewModel<List<ContentModel>>() {
    protected val _contentViewState = MutableLiveData<ViewState<ContentModel>>()
    val contentViewState: LiveData<ViewState<ContentModel>>
        get() = _contentViewState

    protected val _statementState = MutableLiveData<ViewState<StatementModel>>()
    val statementState: LiveData<ViewState<StatementModel>>
        get() = _statementState

    var confirmationText = String()

    fun getLatestContents() = executeUseCase {
        getLatestContentsUseCase()
            .onSuccess {
                _viewState.value = Success(it)
            }
            .onFailure { _viewState.value = Error(it.throwable) }
    }

    fun updateSignature(contentId: String) = executeUseCase {
        updateContentSignatureUseCase(contentId, "")
            .onSuccess {
                _contentViewState.value = Update(it)
            }
            .onFailure { _contentViewState.value = Error(it.throwable) }
    }

    fun getStatement(deviceId: String) = executeUseCase {
        getStatementUseCase(deviceId)
            .onSuccess {
                _statementState.value = Success(it)
            }
            .onFailure { _statementState.value = Error(it.throwable) }
    }
}