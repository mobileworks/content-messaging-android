package uk.co.symec.content.messaging.utils

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.tabs.TabLayout
import uk.co.symec.content.messaging.R

class TabUtils constructor(var label: String, var isNew: Boolean): TabLayout.Tab() {

    companion object {
        fun unSelectTab(tab: TabLayout.Tab, tabUtils: TabUtils) {
            tab.customView?.apply {
                findViewById<CardView>(R.id.cl_selected)?.apply {
                    visibility = View.GONE
                }
                findViewById<ConstraintLayout>(R.id.cl_unselected)?.apply {
                    visibility = View.VISIBLE
                }
                findViewById<ImageView>(R.id.iv_unselected)?.apply {
                    visibility = if (tabUtils.isNew) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
                findViewById<ImageView>(R.id.iv_selected)?.apply {
                    visibility = if (tabUtils.isNew) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
                findViewById<TextView>(R.id.text_unselected)?.apply {
                    text = tabUtils.label
                }
                findViewById<TextView>(R.id.text_selected)?.apply {
                    text = tabUtils.label
                }
            }
        }
        fun selectTab(tab: TabLayout.Tab, tabUtils: TabUtils) {
            tab.customView?.apply {
                findViewById<CardView>(R.id.cl_selected)?.apply {
                    visibility = View.VISIBLE
                }
                findViewById<ConstraintLayout>(R.id.cl_unselected)?.apply {
                    visibility = View.GONE
                }
                findViewById<ImageView>(R.id.iv_unselected)?.apply {
                    visibility = if (tabUtils.isNew) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
                findViewById<ImageView>(R.id.iv_selected)?.apply {
                    visibility = if (tabUtils.isNew) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
                findViewById<TextView>(R.id.text_unselected)?.apply {
                    text = tabUtils.label
                }
                findViewById<TextView>(R.id.text_selected)?.apply {
                    text = tabUtils.label
                }
            }
        }
    }
}