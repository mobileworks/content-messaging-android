package uk.co.symec.content.messaging.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.ui.base.Archived
import uk.co.symec.content.messaging.ui.base.Logout
import uk.co.symec.content.messaging.ui.base.Success
import uk.co.symec.content.messaging.ui.base.SuccessButConnectionError
import uk.co.symec.content.messaging.ui.base.SuccessButNoInternet
import uk.co.symec.content.messaging.utils.extensions.launch
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.*
import javax.inject.Inject

@HiltViewModel
public class ContentsViewModel @Inject constructor(
    private val getContentsUseCase: GetContentsUseCase,
    private val downloadFilesUseCase: DownloadFilesUseCase,
    private val getContentsDbUseCase: GetContentsDbUseCase,
    private val getAllContentsDbUseCase: GetAllContentsDbUseCase,
    private val deleteContentUseCase: DeleteContentUseCase,
    private val updateContentUseCase: UpdateContentUseCase,
    private val deleteContentFromDbUseCase: DeleteContentFromDbUseCase,
    private val getUpdatedStatusDbUseCase: GetUpdatedStatusDbUseCase,
    private val updateContentsUseCase: UpdateContentStatusUseCase,
    private val deleteUpdatedStatusFromDbUseCase: DeleteUpdatedStatusFromDbUseCase
) :
    BaseViewModel<List<ContentModel>>() {

    val _mediaState = MutableLiveData<ViewState<Boolean>>()
    val mediaState: LiveData<ViewState<Boolean>>
        get() = _mediaState

    fun getContents(
        deviceId: String,
        isInternal: Boolean,
        showDetailsForGuid: String?,
        page: Int = 1
    ) =
        executeUseCase {
            var page1 = page
            val list = mutableListOf<String>()
            var result = getContentsUseCase(deviceId, isInternal, page1)
            while (result.second) {
                result.first
                    .onSuccessDouble {
                        if (showDetailsForGuid != null) {
                            if (page1 == 1) {
                                _viewState.value =
                                    SuccessWithShowDetails(it.data, showDetailsForGuid)
                            }
                        } else {
                            if (it.fromServer.size < 6) {
                                _viewState.value = Success(it.data)
                            }
                        }
                        list.addAll(it.fromServer.map { x -> x.id })
                    }
                    .onLogout { _viewState.value = Logout(it) }
                    .onArchived { _viewState.value = Archived(it) }
                page1++
                result = getContentsUseCase(deviceId, isInternal, page1)
            }

            if (!result.second) {
                result.first
                    .onSuccessDouble {
                        if (showDetailsForGuid != null) {
                            if (page1 == 1) {
                                _viewState.value =
                                    SuccessWithShowDetails(it.data, showDetailsForGuid)
                            }
                        } else {
                            _viewState.value = Success(it.data)
                        }
                        list.addAll(it.fromServer.map { x -> x.id })
                    }
                    .onSuccessButNoInternet {
                        _viewState.value = SuccessButNoInternet(it)
                        isConnectedNow = false
                    }
                    .onSuccessButConnectionError {
                        _viewState.value = SuccessButConnectionError(it)
                        isConnectedNow = false
                    }
                    .onLogout { _viewState.value = Logout(it) }
                    .onArchived { _viewState.value = Archived(it) }
                    .onFailure {
                        _viewState.value = Error(it.throwable)
                        isConnectedNow = false
                    }
            }

            println("List with ${list.size} elements")
            deleteContentFromDbUseCase(list).onSuccess {
                if (it) {
                    getContentsDb(isInternal)
                    getAllContentsDb(true)
                }
            }
            runSynchro()
        }

    suspend fun runSynchro() {
        if (isConnected() && !isConnectedNow) {
            isConnectedNow = true
            //send all unsynchronized statuses
            getUpdatedStatusDbUseCase().onSuccess {
                it.forEach {
                    updateContentsUseCase(it.deviceId, it.contentId, it.status)
                        .onSuccess {
                            deleteUpdatedStatusFromDbUseCase(it.id)
                        }
                        .onSuccessButNoInternet {
                            deleteUpdatedStatusFromDbUseCase(it.id)
                            isConnectedNow = false
                        }
                        .onFailure {
                            _viewState.value = Error(it.throwable)
                            isConnectedNow = false
                        }
                }
            }
        }
    }

    fun getContentsDb(isInternal: Boolean) =
        launch {
            getContentsDbUseCase(isInternal)
                .onSuccess {
                    println("${it.size} values loaded from DB")
                    _viewState.value = Success(it)
                }
                .onSuccessButNoInternet { _viewState.value = SuccessButNoInternet(it) }
                .onSuccessButConnectionError {
                    _viewState.value = SuccessButConnectionError(it)
                }
                .onFailure { _viewState.value = Error(it.throwable) }
        }

    fun getAllContentsDb(downloadFiles: Boolean = false) =
        launch {
            getAllContentsDbUseCase(downloadFiles)
                .onSuccess {
                    println("${it.size} values loaded from DB")
                    if (downloadFiles) {
                        _viewState.value = SuccessWithDownload(it)
                    }
                }
                .onFailure { _viewState.value = Error(it.throwable) }
        }

    fun updateContent(
        deviceId: String,
        contentId: String,
        isInternal: Boolean,
        updateList: Boolean
    ) = executeUseCase {
        updateContentUseCase(deviceId, contentId, isInternal)
            .onSuccess {
                if (updateList) {
                    _viewState.value = SuccessWithDownload(it)
                } else {
                    _viewState.value = SuccessHideLoading(UpdateListPackage(it, isInternal))
                }
                runSynchro()
            }
            .onSuccessButNoInternet {
                if (updateList) {
                    _viewState.value = Success(it)
                } else {
                    _viewState.value = SuccessHideLoading(UpdateListPackage(it, isInternal))
                }
                isConnectedNow = false
            }
            .onArchived { _viewState.value = Archived(it) }
    }

    fun deleteContent(contentId: String, isInternal: Boolean, updateList: Boolean) =
        executeUseCase {
            deleteContentUseCase(contentId, isInternal)
                .onSuccess {
                    if (updateList) {
                        _viewState.value = Success(it)
                    } else {
                        _viewState.value = SuccessHideLoading(UpdateListPackage(it, isInternal))
                    }
                }
                .onArchived { _viewState.value = Archived(it) }
        }

    fun getMedias(content: ContentModel) {
        launch {
            downloadFilesUseCase(content)
                .onSuccess { _mediaState.value = Success(it) }
                .onFailure { _mediaState.value = Error(it.throwable) }
        }
    }
}