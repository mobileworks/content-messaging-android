package uk.co.symec.content.messaging.ui.base

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.provider.Telephony
import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.utils.extensions.getSharedPreferences


abstract class BaseFragment<T : ViewDataBinding> : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewReady()
    }

    protected fun onBackPressed() = (activity as BaseActivity).onBackPressed()

    abstract fun viewReady()

    open fun showError(@StringRes errorMessage: Int, rootView: View) {
        (activity as BaseActivity).showError(errorMessage, rootView)
    }

    open fun showError(errorMessage: String?, rootView: View) {
        (activity as BaseActivity).showError(errorMessage, rootView)
    }

    open fun shareContent(content: String) {
        val defaultSmsPackageName =
            Telephony.Sms.getDefaultSmsPackage(activity)

        Intent().apply {
            action = Intent.ACTION_SEND
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, content)

            if (defaultSmsPackageName != null) {
                setPackage(defaultSmsPackageName)
            }

            if (resolveActivity(requireActivity().packageManager) != null) {
                startActivity(this)
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.no_app_to_handle_share,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    open fun showLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).showLoading(loading)
    }

    open fun hideLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).hideLoading(loading)
    }

    open fun getSharedPreferences(): SharedPreferences {
        return (activity as BaseActivity).getSharedPreferences()
    }
}