package uk.co.symec.content.messaging.ui.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.*
import android.net.Uri
import android.os.Bundle
import android.provider.Telephony
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.FileProvider
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import uk.co.symec.content.messaging.BuildConfig
import uk.co.symec.content.messaging.R
import uk.co.symec.content.messaging.databinding.DetailsFragmentBinding
import uk.co.symec.content.messaging.ui.activity.ContentsActivity
import uk.co.symec.content.messaging.ui.adapter.MediaListAdapter
import uk.co.symec.content.messaging.ui.adapter.MediasListener
import uk.co.symec.content.messaging.ui.base.*
import uk.co.symec.content.messaging.vm.ContentActivityViewModel
import uk.co.symec.content.messaging.vm.DetailsViewModel
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.data.utils.Constants
import uk.co.symec.domain.model.ContentModel
import uk.co.symec.domain.model.MediaModel
import java.io.File
import java.lang.StringBuilder


@AndroidEntryPoint
class DetailsBottomFragment : BottomSheetDialogFragment() {

    private val viewModel: DetailsViewModel by viewModels()
    private val parentViewModel: ContentActivityViewModel by activityViewModels()
    private var _binding: DetailsFragmentBinding? = null
    private val binding get() = _binding!!

    lateinit var contentId: String

    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        contentId = arguments?.getString(Constants.CONTENT_ID) ?: ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View { // Inflate the layout for this fragment

        _binding = DetailsFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this

        viewModel.getContent(contentId)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewState.observe(viewLifecycleOwner, { vs ->
            handleViewState(vs)
        })

        parentViewModel.contentViewState.observe(viewLifecycleOwner, { vs ->
            handleViewState(vs)
        })

        viewModel.mediaState.observe(viewLifecycleOwner, { vs ->
            handleMediaState(vs)
        })

        viewModel.scoreState.observe(viewLifecycleOwner, { vs ->
            handleScoreState(vs)
        })

        binding.clShare.setOnClickListener {
            share()
        }

        binding.clConfirm.setOnClickListener {
            if (binding.content?.showConfirmation == true) {
                val fragment = ConfirmationFragment()
                val bundle = Bundle()
                bundle.putString(Constants.CONTENT_ID, contentId)
                fragment.arguments = bundle
                fragment.isCancelable = false
                requireActivity().supportFragmentManager.beginTransaction()
                    .add(fragment, "confirmation")
                    .commitNowAllowingStateLoss()
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.already_confirmed_toast,
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        binding.ivClose.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
        }
    }

    private fun handleViewState(viewState: ViewState<ContentModel>) {
        when (viewState) {
            is Success -> {
                binding.content = viewState.data

                if (viewState.data.status != 1) {
                    NetworkModule.getAssetId(requireContext())?.let {
                        viewModel.statusViewed(it, contentId)
                    }
                }

                binding.rvMediaList.layoutManager = LinearLayoutManager(activity)
                binding.rvMediaList.setHasFixedSize(true)

                val adapter =
                    MediaListAdapter(requireContext(), MediasListener(clickListener = { media ->
                        if (viewState.data.status != 2) {
                            NetworkModule.getAssetId(requireContext())?.let {
                                viewModel.statusSeen(it, contentId)
                            }
                        }
                        viewModel.seenAtUpdated(media.id)
                        if (viewModel.isConnected()) {
                            if (media.isFileSaved) {
                                viewModel.getFile(media.id)
                            } else {
                                when (media.extension) {
                                    "pdf", "pptx", "ppt", "xls", "xlsx" -> {
                                        val link = "http://docs.google.com/viewer?url=${media.link}"
                                        val browserIntent = Intent(ACTION_VIEW, Uri.parse(link))
                                        startActivity(browserIntent)
                                    }
                                    else -> {
                                        val browserIntent =
                                            Intent(ACTION_VIEW, Uri.parse(media.link))
                                        startActivity(browserIntent)
                                    }
                                }
                            }
                        } else {
                            viewModel.getFile(media.id)
                        }
                    }, scoreListener = { score, mediaId ->
                        NetworkModule.getAssetId(requireContext())?.let {
                            viewModel.sendScore(score, it, mediaId)
                        }
                    }
                    ))

                adapter.setMediaList(viewState.data.media)
                binding.rvMediaList.adapter = adapter
            }
            is Update -> {
                binding.content = viewState.data
                (requireActivity() as? ContentsActivity)?.let {
                    it.getCurrentFragment()?.getContents()
                    it.viewModel.getLatestContents()
                }
            }
            is SuccessButNoInternet -> {
                binding.content = viewState.data
                (requireActivity() as? ContentsActivity)?.let {
                    it.getCurrentFragment()?.getContents()
                    it.viewModel.getLatestContents()
                }
            }
            else -> { //
            }
        }
    }

    private fun handleMediaState(viewState: ViewState<MediaModel>) {
        when (viewState) {
            is Success -> {
                if (viewState.data.savedPath.isNotBlank()) {
                    openFile(viewState.data.savedPath)
                } else {
                    Toast.makeText(
                        requireContext(),
                        R.string.file_not_downloaded,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            is Error -> {
                Toast.makeText(requireContext(), viewState.error.message, Toast.LENGTH_LONG).show()
            }
            else -> { //
            }
        }
    }

    private fun handleScoreState(viewState: ViewState<Boolean>) {
        when (viewState) {
            is Success -> {
                Toast.makeText(requireContext(), "Score updated", Toast.LENGTH_SHORT).show()
            }
            is Error -> {
                Toast.makeText(requireContext(), "Score not updated", Toast.LENGTH_SHORT).show()
            }
            else -> { //
            }
        }
    }

    private fun openFile(path: String) {
        val file = File(path)

        //test
//        val split = path.split("/")
//        val count = split.size
//        val newCount = count - 3
//
//        val newPath = StringBuilder()
//        for (i in 0..newCount) {
//            newPath.append(split[i]).append("/")
//        }
//
//        println(newPath.toString())
//
//        val dir = File(newPath.toString())
//
//        val list = dir.list()
//        dir.exists()
//        list.forEach {
//            println(it)
//
//            val newDir = File("$newPath/$it")
//            println(newDir.exists())
//            val list2 = newDir.list()
//            list2.forEach {
//                println(it)
//                val newFile = File("$newDir/$it")
//                println(newFile.exists())
//            }
//        }
        //

        // Get URI and MIME type of file
        val uri: Uri = FileProvider.getUriForFile(
            requireContext(),
            BuildConfig.APPLICATION_ID + ".provider",
            file
        )
        requireContext().contentResolver.getType(uri)?.let { mime ->
            try {
                Intent().apply {
                    action = ACTION_VIEW
                    setDataAndType(uri, mime)
                    flags = FLAG_GRANT_READ_URI_PERMISSION
                    if (resolveActivity(requireActivity().packageManager) != null) {
                        startActivity(this)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            R.string.no_app_to_handle,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(requireContext(), R.string.no_app_to_handle, Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    fun showError(errorMessage: String?, rootView: View) {
        (activity as BaseActivity).showError(errorMessage, rootView)
    }

    fun showLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).showLoading(loading)
    }

    fun hideLoading(loading: ConstraintLayout) {
        (activity as BaseActivity).hideLoading(loading)
    }

    private fun share() {
        val defaultSmsPackageName =
            Telephony.Sms.getDefaultSmsPackage(activity)

        Intent().apply {
            action = ACTION_SEND
            type = "text/plain"

            putExtra(EXTRA_TEXT, binding.content?.text ?: "")

            if (defaultSmsPackageName != null) {
                setPackage(defaultSmsPackageName)
            }

            if (resolveActivity(requireActivity().packageManager) != null) {
                startActivity(this)
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.no_app_to_handle_share,
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}