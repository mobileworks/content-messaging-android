package uk.co.symec.content.messaging.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import uk.co.symec.content.messaging.*
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.GetLatestContentsUseCase
import uk.co.symec.domain.usecase.GetStatementUseCase
import uk.co.symec.domain.usecase.UpdateContentSignatureUseCase

@ExperimentalCoroutinesApi
class ContentActivityViewModelTest {

    private val getLatestContentsUseCase: GetLatestContentsUseCase = mock(GetLatestContentsUseCase::class.java)
    private val updateContentSignatureUseCase: UpdateContentSignatureUseCase = mock(UpdateContentSignatureUseCase::class.java)
    private val getStatementUseCase: GetStatementUseCase = mock(GetStatementUseCase::class.java)
    private val contentActivityViewModel by lazy { ContentActivityViewModel(getLatestContentsUseCase, updateContentSignatureUseCase, getStatementUseCase) }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test getLatestContents sets liveData value when success`() = runBlocking {
        `when`(getLatestContentsUseCase()).thenReturn(
            Failure(
                HttpError(Throwable(
                    DB_ENTRY_ERROR
                ))
            )
        )
        contentActivityViewModel.getLatestContents()
        assert(contentActivityViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test getLatestContents sets liveData value when failure`() = runBlocking {
        `when`(getLatestContentsUseCase()).thenReturn(Success(mutableListOf()))
        contentActivityViewModel.getLatestContents()
        assert(contentActivityViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test updateSignature sets liveData value when success update list true`() = runBlocking {
       `when`(updateContentSignatureUseCase(CONTENT_ID, "")).thenReturn(
            Success(ContentModel.getFilledModel())
        )
        contentActivityViewModel.updateSignature(CONTENT_ID)
        assert(contentActivityViewModel.contentViewState.value is uk.co.symec.content.messaging.ui.base.Update)
    }

    @Test
    fun `test updateSignature sets liveData value when success update list false`() = runBlocking {
        `when`(updateContentSignatureUseCase(CONTENT_ID, "")).thenReturn(
            Failure(
                HttpError(Throwable(
                    DB_ENTRY_ERROR
                ))
            )
        )
        contentActivityViewModel.updateSignature(CONTENT_ID)
        assert(contentActivityViewModel.contentViewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test getStatement sets liveData value when success`() = runBlocking {
        `when`(getStatementUseCase(ASSET_ID)).thenReturn(
            Failure(
                HttpError(Throwable(
                    DB_ENTRY_ERROR
                ))
            )
        )
        contentActivityViewModel.getStatement(ASSET_ID)
        assert(contentActivityViewModel.statementState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test getStatement sets liveData value when failure`() = runBlocking {
        `when`(getStatementUseCase(ASSET_ID)).thenReturn(Success(StatementModel("Test", null)))
        contentActivityViewModel.getStatement(ASSET_ID)
        assert(contentActivityViewModel.statementState.value is uk.co.symec.content.messaging.ui.base.Success)
    }
}