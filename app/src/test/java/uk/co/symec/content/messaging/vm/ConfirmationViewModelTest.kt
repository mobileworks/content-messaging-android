package uk.co.symec.content.messaging.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.kotlin.whenever
import uk.co.symec.content.messaging.CONTENT_ID
import uk.co.symec.content.messaging.DEVICE_ID
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.content.messaging.MEDIA_ID
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.SendScoreUseCase
import uk.co.symec.domain.usecase.*
import java.io.File

@ExperimentalCoroutinesApi
class ConfirmationViewModelTest {

    private val sendSignatureUseCase: SendSignatureUseCase = mock(SendSignatureUseCase::class.java)

    private val confirmationViewModel by lazy { ConfirmationViewModel(sendSignatureUseCase) }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        confirmationViewModel.connectivity = mock(Connectivity::class.java)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test sendScore sets liveData value when success`() = runBlocking {
        val file = File.createTempFile("file",".jpg")
        `when`(sendSignatureUseCase(file, "test", DEVICE_ID, CONTENT_ID)).thenReturn(Success(true))
        confirmationViewModel.sendSignature(file, "test", DEVICE_ID, CONTENT_ID)
        assert(confirmationViewModel.signState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test sendScore sets liveData value when failure`() = runBlocking {
        val file = File.createTempFile("file",".jpg")
        `when`(sendSignatureUseCase(file, "test", DEVICE_ID, CONTENT_ID)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        confirmationViewModel.sendSignature(file, "test", DEVICE_ID, CONTENT_ID)
        assert(confirmationViewModel.signState.value is uk.co.symec.content.messaging.ui.base.Error)
    }
}