package uk.co.symec.content.messaging.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.kotlin.whenever
import uk.co.symec.content.messaging.CONTENT_ID
import uk.co.symec.content.messaging.DEVICE_ID
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.content.messaging.MEDIA_ID
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.SendScoreUseCase
import uk.co.symec.domain.usecase.*

@ExperimentalCoroutinesApi
class DetailsViewModelTest {

    private val getContentUseCase: GetContentUseCase = mock(GetContentUseCase::class.java)
    private val updateContentStatusUseCase: UpdateContentStatusUseCase = mock(UpdateContentStatusUseCase::class.java)
    private val getMediaUseCase: GetMediaUseCase = mock(GetMediaUseCase::class.java)
    private val getUpdatedStatusDbUseCase: GetUpdatedStatusDbUseCase = mock(GetUpdatedStatusDbUseCase::class.java)
    private val deleteUpdatedStatusFromDbUseCase: DeleteUpdatedStatusFromDbUseCase = mock(DeleteUpdatedStatusFromDbUseCase::class.java)
    private val sendScoreUseCase: SendScoreUseCase = mock(SendScoreUseCase::class.java)
    private val updateMediaSeenAtUseCase: UpdateMediaSeenAtUseCase = mock(UpdateMediaSeenAtUseCase::class.java)

    private val detailsViewModel by lazy { DetailsViewModel(getContentUseCase, updateContentStatusUseCase,
        getMediaUseCase, getUpdatedStatusDbUseCase, deleteUpdatedStatusFromDbUseCase, sendScoreUseCase, updateMediaSeenAtUseCase) }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        detailsViewModel.connectivity = mock(Connectivity::class.java)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test getContent sets liveData value when success`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(getContentUseCase(CONTENT_ID)).thenReturn(Success(contentModel))
        detailsViewModel.getContent(CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test getContent sets liveData value when no internet`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(getContentUseCase(CONTENT_ID)).thenReturn(SuccessButNoInternet(contentModel))
        detailsViewModel.getContent(CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessButNoInternet)
    }

    @Test
    fun `test getContent sets liveData value when failure`() = runBlocking {
        `when`(getContentUseCase(CONTENT_ID)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.getContent(CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test sendScore sets liveData value when success`() = runBlocking {
        `when`(sendScoreUseCase(2, DEVICE_ID, CONTENT_ID)).thenReturn(Success(true))
        detailsViewModel.sendScore(2, DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.scoreState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test sendScore sets liveData value when failure`() = runBlocking {
        `when`(sendScoreUseCase(2, DEVICE_ID, CONTENT_ID)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.sendScore(2, DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.scoreState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test updateSeenAt sets liveData value when success`() = runBlocking {
        `when`(updateMediaSeenAtUseCase(MEDIA_ID)).thenReturn(Success(true))
        detailsViewModel.seenAtUpdated(MEDIA_ID)
        assert(detailsViewModel.dateUpdateState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test updateSeenAt sets liveData value when failure`() = runBlocking {
        `when`(updateMediaSeenAtUseCase(MEDIA_ID)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.seenAtUpdated(MEDIA_ID)
        assert(detailsViewModel.dateUpdateState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test statusSeen sets liveData value when success`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 2)).thenReturn(Success(contentModel))
        detailsViewModel.statusSeen(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Update)
    }

    @Test
    fun `test statusSeen sets liveData value when no internet`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 2)).thenReturn(SuccessButNoInternet(contentModel))
        detailsViewModel.statusSeen(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessButNoInternet)
    }

    @Test
    fun `test statusSeen sets liveData value when failure`() = runBlocking {
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 2)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.statusSeen(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test statusViewed sets liveData value when success`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 1)).thenReturn(Success(contentModel))
        detailsViewModel.statusViewed(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Update)
    }

    @Test
    fun `test statusViewed sets liveData value when no internet`() = runBlocking {
        val contentModel = ContentModel.getEmptyModel()
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 1)).thenReturn(SuccessButNoInternet(contentModel))
        detailsViewModel.statusViewed(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessButNoInternet)
    }

    @Test
    fun `test statusViewed sets liveData value when failure`() = runBlocking {
        `when`(updateContentStatusUseCase(DEVICE_ID, CONTENT_ID, 1)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.statusViewed(DEVICE_ID, CONTENT_ID)
        assert(detailsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test getFile sets liveData value when success`() = runBlocking {
        val mediaModel = MediaModel.getEmptyModel()
        `when`(getMediaUseCase(CONTENT_ID)).thenReturn(Success(mediaModel))
        detailsViewModel.getFile(CONTENT_ID)
        assert(detailsViewModel.mediaState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test getFile sets liveData value when failure`() = runBlocking {
        `when`(getMediaUseCase(CONTENT_ID)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        detailsViewModel.getFile(CONTENT_ID)
        assert(detailsViewModel.mediaState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test runSynchro sets liveData value when success`() = runBlocking {
        val statusModel = UpdatedStatusModel.getEmptyModel()
        val list = mutableListOf(statusModel)
        detailsViewModel.isConnectedNow = false
        whenever(detailsViewModel.connectivity.hasNetworkAccess()).thenReturn(true)
        `when`(getUpdatedStatusDbUseCase()).thenReturn(Success(list))
        detailsViewModel.runSynchro()
        assert(detailsViewModel.isConnectedNow)
    }
}