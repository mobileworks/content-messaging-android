package uk.co.symec.content.messaging.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import uk.co.symec.content.messaging.ASSET_ID
import uk.co.symec.content.messaging.DEVICE_ID
import uk.co.symec.content.messaging.FCM
import uk.co.symec.content.messaging.GROUP_ID
import uk.co.symec.data.apiservice.GENERAL_NETWORK_ERROR
import uk.co.symec.domain.model.Failure
import uk.co.symec.domain.model.HttpError
import uk.co.symec.domain.model.RegisterToGroupModel
import uk.co.symec.domain.model.Success
import uk.co.symec.domain.usecase.RegisterIntoGroupUseCase

@ExperimentalCoroutinesApi
class RegisterToGroupViewModelTest {

    private val registerIntoGroupUseCase: RegisterIntoGroupUseCase =
        mock(RegisterIntoGroupUseCase::class.java)

    private val registerToGroupViewModel by lazy { RegisterToGroupViewModel(registerIntoGroupUseCase) }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `test registerIntoGroup sets liveData value when success`() = runBlocking {
        val contentModel = RegisterToGroupModel("token", "", "")
        `when`(registerIntoGroupUseCase(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)).thenReturn(
            Success(contentModel)
        )
        registerToGroupViewModel.androidId = DEVICE_ID
        registerToGroupViewModel.groupId = GROUP_ID
        registerToGroupViewModel.assetId = ASSET_ID
        registerToGroupViewModel.fcm = FCM
        registerToGroupViewModel.model = "Samsung"
        registerToGroupViewModel.register()
        assert(registerToGroupViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test registerIntoGroup sets liveData value when failure`() = runBlocking {
        `when`(registerIntoGroupUseCase(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)).thenReturn(
            Failure(
                HttpError(Throwable(GENERAL_NETWORK_ERROR))
            )
        )
        registerToGroupViewModel.androidId = DEVICE_ID
        registerToGroupViewModel.groupId = GROUP_ID
        registerToGroupViewModel.assetId = ASSET_ID
        registerToGroupViewModel.fcm = FCM
        registerToGroupViewModel.model = "Samsung"
        registerToGroupViewModel.register()
        assert(registerToGroupViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test validation results 1`() = runBlocking {
        registerToGroupViewModel.validationGroupId()

        if (registerToGroupViewModel.validationState.value == 1) {
            assert(true)
        } else {
            assert(false)
        }
    }

    @Test
    fun `test validation results 2`() = runBlocking {
        registerToGroupViewModel.groupId = GROUP_ID
        registerToGroupViewModel.validationGroupId()

        if (registerToGroupViewModel.validationState.value == 2) {
            assert(true)
        } else {
            assert(false)
        }
    }

    @Test
    fun `test validation results 3`() = runBlocking {
        registerToGroupViewModel.validationAssetId()

        if (registerToGroupViewModel.validationState.value == 3) {
            assert(true)
        } else {
            assert(false)
        }
    }

    @Test
    fun `test validation results 4`() = runBlocking {
        registerToGroupViewModel.assetId = ASSET_ID
        registerToGroupViewModel.validationAssetId()

        if (registerToGroupViewModel.validationState.value == 4) {
            assert(true)
        } else {
            assert(false)
        }
    }
}