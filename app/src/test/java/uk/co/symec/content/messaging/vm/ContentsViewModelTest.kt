package uk.co.symec.content.messaging.vm

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.mockito.kotlin.whenever
import uk.co.symec.content.messaging.*
import uk.co.symec.content.messaging.ui.base.ViewState
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.domain.model.*
import uk.co.symec.domain.usecase.*

@ExperimentalCoroutinesApi
class ContentsViewModelTest {

    private val getContentsUseCase: GetContentsUseCase = mock(GetContentsUseCase::class.java)
    private val downloadFilesUseCase: DownloadFilesUseCase = mock(DownloadFilesUseCase::class.java)
    private val getContentsDbUseCase: GetContentsDbUseCase = mock(GetContentsDbUseCase::class.java)
    private val updateContentUseCase: UpdateContentUseCase = mock(UpdateContentUseCase::class.java)
    private val getAllContentsDbUseCase: GetAllContentsDbUseCase = mock(GetAllContentsDbUseCase::class.java)
    private val deleteContentUseCase: DeleteContentUseCase = mock(DeleteContentUseCase::class.java)
    private val getUpdatedStatusDbUseCase: GetUpdatedStatusDbUseCase =
        mock(GetUpdatedStatusDbUseCase::class.java)
    private val updateContentStatusUseCase: UpdateContentStatusUseCase =
        mock(UpdateContentStatusUseCase::class.java)
    private val deleteUpdatedStatusFromDbUseCase: DeleteUpdatedStatusFromDbUseCase =
        mock(DeleteUpdatedStatusFromDbUseCase::class.java)

    private val deleteContentFromDbUseCase: DeleteContentFromDbUseCase =
        mock(DeleteContentFromDbUseCase::class.java)
    private val contentsViewModel by lazy {
        ContentsViewModel(
            getContentsUseCase,
            downloadFilesUseCase,
            getContentsDbUseCase,
            getAllContentsDbUseCase,
            deleteContentUseCase,
            updateContentUseCase,
            deleteContentFromDbUseCase,
            getUpdatedStatusDbUseCase,
            updateContentStatusUseCase,
            deleteUpdatedStatusFromDbUseCase
        )
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    private val dispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        contentsViewModel.connectivity = mock(Connectivity::class.java)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test getContents sets liveData value when success with details`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        ).thenReturn(Pair(SuccessDouble(DoubleDataPackage(fakeContentsList, fakeContentsList)), false))

        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, CONTENT_ID, 1)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessWithShowDetails)
    }

    @Test
    fun `test getContents sets liveData value when success with details logout`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        ).thenReturn(Pair(Logout(true), false))

        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, CONTENT_ID, 1)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Logout)
    }

    @Test
    fun `test getContents sets liveData value when success with no details`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        ).thenReturn(Pair(SuccessDouble(DoubleDataPackage(fakeContentsList, fakeContentsList)), false))

        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, null, 1)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test getContents sets liveData value when success more pages`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        ).thenReturn(Pair(Success(fakeContentsList), true))
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                2
            )
        ).thenReturn(Pair(Success(fakeContentsList), false))
        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, CONTENT_ID, 1)
        assert(contentsViewModel.viewState.value is ViewState<List<ContentModel>>)
    }

    @Test
    fun `test getContents sets liveData value when success no internet`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        ).thenReturn(Pair(SuccessButNoInternet(fakeContentsList), false))
        whenever(contentsViewModel.connectivity.hasNetworkAccess()).thenReturn(false)

        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, CONTENT_ID, 1)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessButNoInternet)
    }

    @Test
    fun `test getContents sets liveData value when success other error`() = runBlocking {
        `when`(
            getContentsUseCase(
                DEVICE_ID,
                IS_INTERNAL,
                1
            )
        )
            .thenReturn(
                Pair(
                    SuccessButConnectionError(
                        HttpErrorPackage(
                            fakeContentsList,
                            HttpError(Throwable(""))
                        )
                    ), false
                )
            )
        contentsViewModel.getContents(DEVICE_ID, IS_INTERNAL, CONTENT_ID, 1)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessButConnectionError)
    }

    @Test
    fun `test getContentsDb sets liveData value when success`() = runBlocking {
        `when`(getContentsDbUseCase(IS_INTERNAL)).thenReturn(Success(fakeContentsList))
        contentsViewModel.getContentsDb(IS_INTERNAL)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test getAllContentsDb sets liveData value when success`() = runBlocking {
        `when`(getAllContentsDbUseCase(true)).thenReturn(Success(fakeContentsList))
        contentsViewModel.getAllContentsDb(true)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessWithDownload)
    }

    @Test
    fun `test getAllContentsDb sets liveData value when failure`() = runBlocking {
        `when`(getAllContentsDbUseCase(true)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        contentsViewModel.getAllContentsDb(true)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test updateContent sets liveData value when success update list true`() = runBlocking {
        `when`(updateContentUseCase(DEVICE_ID, CONTENT_ID, IS_INTERNAL)).thenReturn(
            Success(fakeContentsList)
        )
        contentsViewModel.updateContent(DEVICE_ID, CONTENT_ID, IS_INTERNAL, UPDATE_LIST_TRUE)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessWithDownload)
    }

    @Test
    fun `test updateContent sets liveData value when success update list false`() = runBlocking {
        `when`(updateContentUseCase(DEVICE_ID, CONTENT_ID, IS_INTERNAL)).thenReturn(
            Success(fakeContentsList)
        )
        contentsViewModel.updateContent(DEVICE_ID, CONTENT_ID, IS_INTERNAL, UPDATE_LIST_FALSE)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessHideLoading)
    }

    @Test
    fun `test deleteContent sets liveData value when success update list true`() = runBlocking {
        `when`(deleteContentUseCase(CONTENT_ID, IS_INTERNAL)).thenReturn(Success(fakeContentsList))
        contentsViewModel.deleteContent(CONTENT_ID, IS_INTERNAL, UPDATE_LIST_TRUE)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test deleteContent sets liveData value when success update list false`() = runBlocking {
        `when`(deleteContentUseCase(CONTENT_ID, IS_INTERNAL)).thenReturn(Success(fakeContentsList))
        contentsViewModel.deleteContent(CONTENT_ID, IS_INTERNAL, UPDATE_LIST_FALSE)
        assert(contentsViewModel.viewState.value is uk.co.symec.content.messaging.ui.base.SuccessHideLoading)
    }

    @Test
    fun `test getMedias sets liveData value when success`() = runBlocking {
        val cm = ContentModel.getFilledModel()
        `when`(downloadFilesUseCase(cm)).thenReturn(Success(true))
        contentsViewModel.getMedias(cm)
        assert(contentsViewModel.mediaState.value is uk.co.symec.content.messaging.ui.base.Success)
    }

    @Test
    fun `test getMedias sets liveData value when failure`() = runBlocking {
        val cm = ContentModel.getFilledModel()
        `when`(downloadFilesUseCase(cm)).thenReturn(Failure(HttpError(Throwable(DB_ENTRY_ERROR))))
        contentsViewModel.getMedias(cm)
        assert(contentsViewModel.mediaState.value is uk.co.symec.content.messaging.ui.base.Error)
    }

    @Test
    fun `test runSynchro sets liveData value when success`() = runBlocking {
        val statusModel = UpdatedStatusModel.getEmptyModel()
        val list = mutableListOf(statusModel)
        contentsViewModel.isConnectedNow = false
        whenever(contentsViewModel.connectivity.hasNetworkAccess()).thenReturn(true)
        `when`(getUpdatedStatusDbUseCase()).thenReturn(Success(list))
        contentsViewModel.runSynchro()
        assert(contentsViewModel.isConnectedNow)
    }
}