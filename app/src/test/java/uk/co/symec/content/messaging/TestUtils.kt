package uk.co.symec.content.messaging

import uk.co.symec.domain.model.ContentModel

const val IS_INTERNAL = false
const val DEVICE_ID = "1234"
const val CONTENT_ID = "fdsfd-fdsfdsf"
const val MEDIA_ID = "abcd"
const val GROUP_ID = "fdsfd-fdsfdsf"
const val ASSET_ID = "fdsfd-fdsfdsf"
const val FCM = "fdsfd-fdsfdsf"
const val UPDATE_LIST_TRUE = true
const val UPDATE_LIST_FALSE = false

val fakeContentsList =  mutableListOf(ContentModel.getFilledModel(), ContentModel.getFilledModel())