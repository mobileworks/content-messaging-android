package uk.co.symec.data.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat

import org.junit.Before
import org.junit.Test
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.db.entity.MediaEntity
import java.io.IOException
import java.util.*


class ContentTest {
    lateinit var context: Context

    private lateinit var contentDAO: ContentDAO
    private lateinit var db: AppDatabase

    private lateinit var content2: ContentEntity
    private lateinit var content3: ContentEntity

    @Before
    fun createDb() {
        context = ApplicationProvider.getApplicationContext()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        contentDAO = db.contentDao()

        runBlocking {
            try {
                val list = mutableListOf<MediaEntity>()

                    val media = MediaEntity(
                        "11",
                        "1",
                        "fdsf",
                        "sdf",
                        "sfsdf",
                        "sdfsf",
                        "sdfds",
                        Date(),
                        Date(),
                        Date(),
                        "http://path",
                        0
                    )
                media.message = "fdsfsdfsdf"
                list.add(media)

                val xx = list.toList()

                val content1 = ContentEntity(
                    "1",
                    "",
                    "",
                    false,
                    true,
                    Date(),
                    Date(),
                    0,
                    0,
                    "100",
                    false,
                    xx
                )

                contentDAO.insertContent(content1)
                content2 = ContentEntity(
                    "2",
                    "",
                    "",
                    true,
                    true,
                    Date(1),
                    Date(2),
                    100,
                    0,
                    "100",
                    false,
                    mutableListOf(
                        MediaEntity(
                            "21",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            Date(),
                            Date(),
                            Date(),
                            "",
                            0
                        )
                    )
                )
                contentDAO.insertContent(content2)
                content3 = ContentEntity(
                    "3",
                    "",
                    "",
                    true,
                    true,
                    Date(1),
                    Date(2),
                    100,
                    0,
                    "100",
                    false,
                    mutableListOf(
                        MediaEntity(
                            "31",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            Date(),
                            Date(),
                            Date(),
                            "",
                            0
                        )
                    )
                )
                contentDAO.insertContent(content3)
            } catch (e: Exception) {
                print(e.message)
            }
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun readSelectedContent() {
        runBlocking {
            val result = contentDAO.getContent("2")
            assertThat(result, equalTo(content2))
        }
    }

    @Test
    @Throws(Exception::class)
    fun readAllData() {
        runBlocking {
            val list = contentDAO.getAllContents()
            assert(list.size == 3)
        }
    }

    @Test
    @Throws(Exception::class)
    fun readInternal() {
        runBlocking {
            val list = contentDAO.getContents(true)
            assert(list.size == 2)
        }
    }

    @Test
    @Throws(Exception::class)
    fun readInternal2() {
        runBlocking {
            val list = contentDAO.checkIfNewStatusOnList(true)
            assert(list == 1)
        }
    }

    @Test
    @Throws(Exception::class)
    fun deleteContent() {
        runBlocking {
            val result = contentDAO.deleteContent(content3)
            assert(result == 1)
        }
    }
}