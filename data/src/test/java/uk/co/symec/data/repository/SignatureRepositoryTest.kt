package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.utils.*
import java.io.File

class SignatureRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(TestCoroutineContextProvider::class.java)
    private val signatureRepository = SignatureRepositoryImpl(connectionServiceTestApi)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        signatureRepository.connectivity = connectivity
        signatureRepository.contextProvider = coroutineContextProvider
    }

//    @Test
//    fun `test sendSignature calls api upon success`() {
//        runBlocking {
//            val file = File.createTempFile("file",".jpg")
//            val filePart = MultipartBody.Part.createFormData(
//                "file",
//                file.name,
//                RequestBody.create(MediaType.parse("image/*"), file)
//            )
//            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
//            whenever(connectionServiceTestApi.sendSignature(filePart, NAME, ASSET_ID, CONTENT_ID)).thenReturn(
//                Response.success(updateDeviceInfoResponse)
//            )
//            signatureRepository.sendSignature(file, NAME, ASSET_ID, CONTENT_ID)
//
//            verify(connectionServiceTestApi, times(0)).sendSignature(filePart, NAME, ASSET_ID, CONTENT_ID)
//        }
//    }

    @Test
    fun `test sendSignature calls api upon failure`() {
        runBlocking {
            val file = File.createTempFile("file",".jpg")
            val filePart = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(MediaType.parse("image/*"), file)
            )
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.sendSignature(filePart, NAME, ASSET_ID, CONTENT_ID)).thenReturn(
                Response.success(updateDeviceInfoResponse)
            )
            signatureRepository.sendSignature(file, NAME, ASSET_ID, CONTENT_ID)

            verify(connectionServiceTestApi, times(0)).sendSignature(filePart, NAME, ASSET_ID, CONTENT_ID)
        }
    }
}