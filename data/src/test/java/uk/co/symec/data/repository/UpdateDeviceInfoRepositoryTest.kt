package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.utils.*

class UpdateDeviceInfoRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(TestCoroutineContextProvider::class.java)
    private val updateDeviceInfoRepository = UpdateDeviceInfoRepositoryImpl(connectionServiceTestApi)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        updateDeviceInfoRepository.connectivity = connectivity
        updateDeviceInfoRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test updateDeviceInfo calls api upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.updateDeviceInfo(DEVICE_ID, FCM)).thenReturn(
                Response.success(updateDeviceInfoResponse)
            )
            updateDeviceInfoRepository.updateDevice(DEVICE_ID, FCM)

            verify(connectionServiceTestApi, times(1)).updateDeviceInfo(DEVICE_ID, FCM)
        }
    }

    @Test
    fun `test updateDeviceInfo calls api upon failure`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.updateDeviceInfo(DEVICE_ID, FCM)).thenReturn(
                Response.success(updateDeviceInfoResponse)
            )
            updateDeviceInfoRepository.updateDevice(DEVICE_ID, FCM)

            verify(connectionServiceTestApi, times(0)).updateDeviceInfo(DEVICE_ID, FCM)
        }
    }
}