package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.utils.*

class ScoreRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val mediaDao: MediaDAO = mock(MediaDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(TestCoroutineContextProvider::class.java)
    private val scoreRepository = ScoreRepositoryImpl(connectionServiceTestApi, mediaDao)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        scoreRepository.connectivity = connectivity
        scoreRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test setScore calls api upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.sendScore(ASSET_ID, CONTENT_ID, 3)).thenReturn(
                Response.success(successUpdateContentResponse)
            )
            scoreRepository.sendScore(3, ASSET_ID, CONTENT_ID)

            verify(connectionServiceTestApi, times(1)).sendScore(ASSET_ID, CONTENT_ID, 3)
        }
    }

    @Test
    fun `test setScore calls api upon failure`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.sendScore(ASSET_ID, CONTENT_ID, 3)).thenReturn(
                Response.success(updateDeviceInfoResponse)
            )
            scoreRepository.sendScore(3, ASSET_ID, CONTENT_ID)

            verify(connectionServiceTestApi, times(0)).sendScore(ASSET_ID, CONTENT_ID, 3)
        }
    }
}