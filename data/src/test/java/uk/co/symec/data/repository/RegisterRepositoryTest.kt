package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.utils.*

class RegisterRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(TestCoroutineContextProvider::class.java)
    private val registerRepository = RegisterRepositoryImpl(connectionServiceTestApi)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        registerRepository.connectivity = connectivity
        registerRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test registerIntoGroup calls api upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.registerInTheGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)).thenReturn(
                Response.success(registerToGroupResponse)
            )
            registerRepository.registerIntoGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)

            verify(connectionServiceTestApi, times(1)).registerInTheGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)
        }
    }

    @Test
    fun `test registerIntoGroup calls api upon failure`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.registerInTheGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)).thenReturn(
                Response.success(registerToGroupResponse)
            )
            registerRepository.registerIntoGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)

            verify(connectionServiceTestApi, times(0)).registerInTheGroup(ASSET_ID, FCM, GROUP_ID, "Samsung", DEVICE_ID)
        }
    }
}