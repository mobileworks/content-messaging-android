package uk.co.symec.data.utils

import okhttp3.MediaType
import okhttp3.ResponseBody
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.db.entity.ContentWithMediaEntity
import uk.co.symec.data.db.entity.MediaEntity
import uk.co.symec.data.db.entity.UpdatedStatusEntity
import uk.co.symec.data.models.*
import java.util.*

const val IS_INTERNAL = false
const val DEVICE_ID = "1234"
const val PAGE = 1
const val CONTENT_ID = "fdsfd-fdsfdsf"
const val MEDIA_ID = "abcd"
const val STATUS = 1
const val GROUP_ID = "fdsfd-fdsfdsf"
const val ASSET_ID = "fdsfd-fdsfdsf"
const val FCM = "fdsfd-fdsfdsf"
const val NAME = "name"

const val FAKE_FAILURE_ERROR_CODE = 400

val successContentsResponse = GetContentsResponse(1, mutableListOf(), "", 1, "","",10,"", 10, "")
val successContentResponse = GetContentResponse(CONTENT_ID,"","",0,0, Date(), Date(), 0, "", 0, mutableListOf(), "")
val successUpdateContentResponse = MessageResponse("Success")
val errorUpdateContentResponse = MessageResponse("")
val registerToGroupResponse = RegisterToGroupResponse("xccvxcv", "test name", "xcvxcv")
val updateDeviceInfoResponse = MessageResponse("xccvxcv")
val failureResponseBody: ResponseBody = ResponseBody.create(MediaType.parse("text"), "network error")
val fakeContentsEntity =  mutableListOf<ContentEntity>()
val fakeContentsMediaEntity =  mutableListOf<ContentWithMediaEntity>()
val fakeContentEntity =  ContentEntity(CONTENT_ID,"","",false, false, Date(), Date(), 0, "", false, mutableListOf())
val fakeContentMediaEntity =  ContentWithMediaEntity(fakeContentEntity, mutableListOf())
val fakeUpdatedStatusesEntity = mutableListOf<UpdatedStatusEntity>()
val fakeUpdatedStatusEntity = UpdatedStatusEntity("1", 1, "xxxx")
val fakeMediaEntity =  MediaEntity("","","","", "", "","", Date(), Date(), null,"")

val fakeContentsEntity2 =  mutableListOf(ContentEntity(CONTENT_ID + "x","","",false, false, Date(), Date(), 0, null, false, mutableListOf()))
val fakeContentsEntity3 =  mutableListOf(ContentEntity(CONTENT_ID + "y","","",false, false, Date(), Date(), 0, null, false, mutableListOf()))