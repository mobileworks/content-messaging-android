package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.dao.UpdatedStatusDAO
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.utils.*
import java.util.*

class ContentRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val contentDao: ContentDAO = mock(ContentDAO::class.java)
    private val mediaDao: MediaDAO = mock(MediaDAO::class.java)
    private val updatedStatusDao: UpdatedStatusDAO = mock(UpdatedStatusDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider =
        spy(TestCoroutineContextProvider::class.java)
    private val contentRepository = ContentRepositoryImpl(
        connectionServiceTestApi,
        contentDao,
        mediaDao,
        updatedStatusDao
    )

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        contentRepository.connectivity = connectivity
        contentRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test getContents calls api and saves data to db upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.getContents(DEVICE_ID)).thenReturn(
                Response.success(successContentsResponse)
            )
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getContents(DEVICE_ID, IS_INTERNAL, PAGE)

            verify(connectionServiceTestApi, times(1)).getContents(DEVICE_ID)
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test getContents calls api and saves data to db upon failure`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.getContents(DEVICE_ID)).thenReturn(
                Response.error(FAKE_FAILURE_ERROR_CODE, failureResponseBody)
            )
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getContents(DEVICE_ID, IS_INTERNAL, PAGE)

            verify(connectionServiceTestApi, times(1)).getContents(DEVICE_ID)
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test getContents calls api and saves data to db upon success no internet`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.getContents(DEVICE_ID)).thenReturn(
                Response.success(successContentsResponse)
            )
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getContents(DEVICE_ID, IS_INTERNAL, PAGE)

            verify(connectionServiceTestApi, times(0)).getContents(DEVICE_ID)
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test getContent reads data from db upon success`() {
        runBlocking {
            whenever(contentDao.getContentWithMedia(CONTENT_ID)).thenReturn(
                fakeContentMediaEntity
            )
            contentRepository.getContent(CONTENT_ID)

            verify(contentDao, times(1)).getContentWithMedia(CONTENT_ID)
        }
    }

    @Test
    fun `test getContentFromDb reads data from db upon success`() {
        runBlocking {
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getContentsFromDb(IS_INTERNAL)

            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test getAllContentFromDb reads data from db upon success`() {
        runBlocking {
            whenever(contentDao.getAllContents()).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getAllContentsFromDb()

            verify(contentDao, times(1)).getAllContents()
        }
    }

    @Test
    fun `test getUpdatedStatuses reads data from db upon success`() {
        runBlocking {
            whenever(updatedStatusDao.getAllUpdatedStatus()).thenReturn(
                fakeUpdatedStatusesEntity
            )
            contentRepository.getUpdatedStatuses()

            verify(updatedStatusDao, times(1)).getAllUpdatedStatus()
        }
    }

    @Test
    fun `test deleteUpdatedStatuses reads data from db`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(updatedStatusDao.deleteUpdateStatus(fakeUpdatedStatusEntity)).thenReturn(
                1
            )
            whenever(updatedStatusDao.getUpdatedStatus("1")).thenReturn(
                fakeUpdatedStatusEntity
            )
            contentRepository.deleteUpdatedStatusFromDb("1")

            verify(updatedStatusDao, times(1)).getUpdatedStatus("1")
            verify(updatedStatusDao, times(1)).deleteUpdateStatus(fakeUpdatedStatusEntity)
        }
    }

    @Test
    fun `test updateContent calls api and saves data to db upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.getContent(DEVICE_ID, CONTENT_ID)).thenReturn(
                Response.success(successContentResponse)
            )
            whenever(contentDao.insertContent(successContentResponse.mapToRoomEntity())).thenReturn(
                1234
            )
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.updateContentAndReturnList(DEVICE_ID, CONTENT_ID, IS_INTERNAL)

            verify(connectionServiceTestApi, times(1)).getContent(
                DEVICE_ID, CONTENT_ID
            )
            verify(contentDao, times(1)).insertContent(successContentResponse.mapToRoomEntity())
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test updateContent calls api and saves data to db upon no internet`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.getContent(DEVICE_ID, CONTENT_ID)).thenReturn(
                Response.success(successContentResponse)
            )
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            whenever(contentDao.getContent(CONTENT_ID)).thenReturn(
                fakeContentEntity
            )
            contentRepository.updateContentAndReturnList(DEVICE_ID, CONTENT_ID, IS_INTERNAL)

            verify(contentDao, times(1)).getContent(CONTENT_ID)
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test deleteContent calls api and saves data to db upon success`() {
        runBlocking {
            val fakeContentEntity = fakeContentEntity
            whenever(contentDao.getContent(CONTENT_ID)).thenReturn(
                fakeContentEntity
            )
            whenever(contentDao.deleteContent(fakeContentEntity)).thenReturn(1)
            whenever(contentDao.getContents(IS_INTERNAL)).thenReturn(
                fakeContentsEntity
            )
            contentRepository.deleteContentAndReturnList(CONTENT_ID, IS_INTERNAL)

            verify(contentDao, times(1)).getContent(CONTENT_ID)
            verify(contentDao, times(1)).deleteContent(fakeContentEntity)
            verify(contentDao, times(1)).getContents(IS_INTERNAL)
        }
    }

    @Test
    fun `test deleteContentFromDb calls and deletes data from db`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            val fakeContentEntity = ContentEntity(CONTENT_ID + "x","","",false, false, Date(), Date(), 0, 0,"", false, mutableListOf())
            val fakeContentsEntity = fakeContentsEntity2
            whenever(contentDao.deleteContent(fakeContentEntity)).thenReturn(1)
            whenever(contentDao.getAllContents()).thenReturn(
                fakeContentsEntity
            )
            contentRepository.deleteContentFromDb(fakeContentsEntity3.map { x -> x.id })

            verify(contentDao, times(1)).getAllContents()
            verify(contentDao, times(1)).deleteContent(fakeContentsEntity.first())
        }
    }

    @Test
    fun `test getLatestContents reads data from db upon success`() {
        runBlocking {
            whenever(contentDao.getLatestContents()).thenReturn(
                fakeContentsEntity
            )
            contentRepository.getLatestContents()

            verify(contentDao, times(1)).getLatestContents()
        }
    }

    @Test
    fun `test updateStatusContent calls api and saves data to db upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(
                connectionServiceTestApi.contentStatusUpdate(
                    DEVICE_ID,
                    CONTENT_ID,
                    STATUS
                )
            ).thenReturn(
                Response.success(successUpdateContentResponse)
            )
            whenever(contentDao.updateContent(CONTENT_ID, STATUS)).thenReturn(
                Pair(fakeContentEntity, true)
            )
            contentRepository.updateStatusContent(DEVICE_ID, CONTENT_ID, STATUS)

            verify(connectionServiceTestApi, times(1)).contentStatusUpdate(
                DEVICE_ID,
                CONTENT_ID,
                STATUS
            )
            verify(contentDao, times(1)).updateContent(CONTENT_ID, STATUS)
        }
    }

    @Test
    fun `test updateStatusContent calls api and saves data to db upon no success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(
                connectionServiceTestApi.contentStatusUpdate(
                    DEVICE_ID,
                    CONTENT_ID,
                    STATUS
                )
            ).thenReturn(
                Response.success(errorUpdateContentResponse)
            )
            whenever(contentDao.getContent(CONTENT_ID)).thenReturn(fakeContentEntity)

            contentRepository.updateStatusContent(DEVICE_ID, CONTENT_ID, STATUS)

            verify(connectionServiceTestApi, times(1)).contentStatusUpdate(
                DEVICE_ID,
                CONTENT_ID,
                STATUS
            )
            verify(contentDao, times(1)).getContent(CONTENT_ID)
        }
    }

    @Test
    fun `test updateStatusContent calls api and saves data to db upon success no internet`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(contentDao.updateContent(CONTENT_ID, STATUS)).thenReturn(
                Pair(fakeContentEntity, true)
            )
            contentRepository.updateStatusContent(DEVICE_ID, CONTENT_ID, STATUS)

            verify(contentDao, times(1)).updateContent(CONTENT_ID, STATUS)
        }
    }

    @Test
    fun `test updateSignatureContent calls api and saves data to db upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(contentDao.updateSignature(CONTENT_ID, "")).thenReturn(
                Pair(fakeContentEntity, true)
            )
            contentRepository.updateSignatureContent(CONTENT_ID, "")

            verify(contentDao, times(1)).updateSignature(CONTENT_ID, "")
        }
    }
}