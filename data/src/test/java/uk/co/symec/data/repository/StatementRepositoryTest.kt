package uk.co.symec.data.repository

import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.models.GetStatementResponse
import uk.co.symec.data.utils.*

class StatementRepositoryTest {

    private val connectionServiceTestApi: ConnectionService = mock(ConnectionService::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(TestCoroutineContextProvider::class.java)
    private val statementRepository = StatementRepositoryImpl(connectionServiceTestApi)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        statementRepository.connectivity = connectivity
        statementRepository.contextProvider = coroutineContextProvider
    }

    @Test
    fun `test getStatement calls api upon success`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(connectionServiceTestApi.getStatement(ASSET_ID)).thenReturn(
                Response.success(GetStatementResponse("test", null))
            )
            statementRepository.getStatement(ASSET_ID)

            verify(connectionServiceTestApi, times(1)).getStatement(ASSET_ID)
        }
    }

    @Test
    fun `test getStatement calls api upon failure`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(false)
            whenever(connectionServiceTestApi.getStatement(ASSET_ID)).thenReturn(
                Response.success(GetStatementResponse(null, "fd"))
            )
            statementRepository.getStatement(ASSET_ID)

            verify(connectionServiceTestApi, times(0)).getStatement(ASSET_ID)
        }
    }
}