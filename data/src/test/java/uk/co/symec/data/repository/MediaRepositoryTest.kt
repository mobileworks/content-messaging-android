package uk.co.symec.data.repository

import android.app.DownloadManager
import android.content.Context
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.db.entity.MediaEntity
import uk.co.symec.data.utils.*
import uk.co.symec.domain.model.ContentModel
import java.util.*


class MediaRepositoryTest {
    @Rule @JvmField
    var tempFolder = TemporaryFolder()

    private val context: Context = spy(Context::class.java)
    private val mediaDao: MediaDAO = mock(MediaDAO::class.java)
    private val connectivity: Connectivity = spy(Connectivity::class.java)
    private val coroutineContextProvider: TestCoroutineContextProvider = spy(
        TestCoroutineContextProvider::class.java
    )
    private val mediaRepository = MediaRepositoryImpl(
        mediaDao,
        context
    )

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        mediaRepository.connectivity = connectivity
        mediaRepository.contextProvider = coroutineContextProvider
//        `when`(DownloadManager.Request.VISIBILITY_VISIBLE).thenReturn(0)
        `when`(context.filesDir).thenReturn(tempFolder.newFolder())
    }

    @Test
    fun `test getMedia calls api and saves data to db upon success`() {
        runBlocking {
            whenever(mediaDao.getMedia(CONTENT_ID)).thenReturn(
                fakeMediaEntity
            )
            mediaRepository.getMedia(CONTENT_ID)

            verify(mediaDao, times(1)).getMedia(CONTENT_ID)
        }
    }


    @Test
    fun `test update seenAt calls and updates data from db`() {
        runBlocking {
            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
            whenever(mediaDao.updateSeenAt(MEDIA_ID)).thenReturn(Pair(Date(), true))
            mediaRepository.updateSeenAt(MEDIA_ID)

            verify(mediaDao, times(1)).updateSeenAt(MEDIA_ID)
        }
    }

//    @Test
//    fun `test getMedias calls api and download files media file already existing`() {
//        runBlocking {
//            //`when`(context.getString(R.string)).thenReturn("cancel")
//            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
//            whenever(mediaDao.getMediaWithContentId("", "xxxxx")).thenReturn(
//                MediaEntity("","xxxxx","","","","", null, Date(), Date(), "")
//            )
//
//            mediaRepository.getMedias(ContentModel.getFilledModel())
//
//            verify(mediaDao, times(1)).getMediaWithContentId("", "xxxxx")
//        }
//    }

//    @Test
//    fun `test getMedias calls api and download files media file update`() {
//        runBlocking {
//            //`when`(context.getString(R.string)).thenReturn("cancel")
//            whenever(connectivity.hasNetworkAccess()).thenReturn(true)
//            whenever(mediaDao.getMediaWithContentId("", "xxxxx")).thenReturn(
//                MediaEntity("","xxxxx","","","","", null, Date(), Date(), "")
//            )
//
//            mediaRepository.getMedias(ContentModel.getFilledModel())
//
//            verify(mediaDao, times(1)).getMediaWithContentId("", "xxxxx")
//        }
//    }
}