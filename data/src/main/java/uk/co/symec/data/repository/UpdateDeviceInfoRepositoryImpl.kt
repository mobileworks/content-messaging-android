package uk.co.symec.data.repository

import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.getDataApi
import uk.co.symec.domain.model.MessageContentModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.UpdateDeviceInfoRepository
import javax.inject.Inject

class UpdateDeviceInfoRepositoryImpl @Inject constructor(
    private val api: ConnectionService
) : BaseRepository(),
    UpdateDeviceInfoRepository {

    override suspend fun updateDevice(deviceId: String, fcm: String): Result<MessageContentModel> {
        return fetchData(
            dataProvider = {
                api.updateDeviceInfo(deviceId, fcm).getDataApi()
            }
        )
    }
}

