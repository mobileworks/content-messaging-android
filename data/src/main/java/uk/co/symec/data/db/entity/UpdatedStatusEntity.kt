package uk.co.symec.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.UpdatedStatusModel

@Entity(
    tableName = "updated_status"
)
data class UpdatedStatusEntity(
    @PrimaryKey @ColumnInfo(name = "content_id") val id: String,
    @ColumnInfo(name = "new_status") var status: Int,
    @ColumnInfo(name = "device_id") var deviceId: String

): DomainMapper<UpdatedStatusModel> {
    override fun mapToDomainModel() = UpdatedStatusModel(id, status, deviceId)

    @Ignore
    override var message: String? = null
}