package uk.co.symec.data.repository

import kotlinx.coroutines.withContext
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.data.apiservice.NO_INTERNET_CONNECTION
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.data.utils.CoroutineContextProvider
import uk.co.symec.domain.model.*
import javax.inject.Inject

open class BaseRepository {

    @Inject
    lateinit var connectivity: Connectivity

    @Inject
    lateinit var contextProvider: CoroutineContextProvider

    /**
     * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
     */
    protected suspend fun <T : Any> fetchDataFromDb(
        dbDataProvider: suspend () -> DomainMapper<T>?
    ): Result<T> {
        return withContext(contextProvider.io) {
            val dbResult = dbDataProvider()
            if (dbResult != null) {
                Success(dbResult.mapToDomainModel())
            } else {
                Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
            }
        }
    }


    protected suspend fun <T : Any> fetchDataListFromDb(
        dbDataProvider: suspend () -> List<DomainMapper<T>>?
    ): Result<List<T>> {
        return withContext(contextProvider.io) {
            val dbResult = dbDataProvider()
            if (dbResult != null) {
                Success(dbResult.map { x -> x.mapToDomainModel() })
            } else {
                Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
            }
        }
    }

    /**
     * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
     */
    protected suspend fun <T : Any> fetchData(
        apiDataProvider: suspend () -> Result<T>,
        dbDataProvider: suspend () -> DomainMapper<T>?
    ): Result<T> {
        return if (connectivity.hasNetworkAccess()) {
            withContext(contextProvider.io) {
                apiDataProvider()
            }
        } else {
            withContext(contextProvider.io) {
                val dbResult = dbDataProvider()
                if (dbResult != null) {
                    SuccessButNoInternet(dbResult.mapToDomainModel())
                } else {
                    Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                }
            }
        }
    }

    /**
     * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
     * List version
     */
    protected suspend fun <T : Any> fetchDataList(
        apiDataProvider: suspend () -> Result<List<T>>,
        dbDataProvider: suspend () -> List<DomainMapper<T>>
    ): Result<List<T>> {
        return if (connectivity.hasNetworkAccess()) {
            withContext(contextProvider.io) {
                apiDataProvider()
            }
        } else {
            withContext(contextProvider.io) {
                val dbResult = dbDataProvider()
                SuccessButNoInternet(dbResult.map { x -> x.mapToDomainModel() })
            }
        }
    }

    /**
     * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
     * List version
     */
    protected suspend fun <T : Any> fetchContentList(
        apiDataProvider: suspend () -> Pair<Result<List<T>>, Boolean>,
        dbDataProvider: suspend () -> List<DomainMapper<T>>
    ): Pair<Result<List<T>>, Boolean> {
        return if (connectivity.hasNetworkAccess()) {
            withContext(contextProvider.io) {
                apiDataProvider()
            }
        } else {
            withContext(contextProvider.io) {
                val dbResult = dbDataProvider()
                Pair(SuccessButNoInternet(dbResult.map { x -> x.mapToDomainModel() }), false)
            }
        }
    }

    /**
     * Use this when communicating only with the api service
     */
    protected suspend fun <T : Any> fetchData(dataProvider: suspend () -> Result<T>): Result<T> {
        return if (connectivity.hasNetworkAccess()) {
            withContext(contextProvider.io) {
                dataProvider()
            }
        } else {
            Failure(HttpError(Throwable(NO_INTERNET_CONNECTION)))
        }
    }
}