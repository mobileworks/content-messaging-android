package uk.co.symec.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.squareup.moshi.Moshi
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.dao.UpdatedStatusDAO
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.db.entity.MediaEntity
import uk.co.symec.data.db.entity.UpdatedStatusEntity

/**
 * The Room database for this app
 */

const val DB_ENTRY_ERROR = "No entry found in database"

@Database(entities = [ContentEntity::class, MediaEntity::class, UpdatedStatusEntity::class], version = 4, exportSchema = true)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun contentDao(): ContentDAO
    abstract fun mediaDao(): MediaDAO
    abstract fun updatedStatusDao(): UpdatedStatusDAO

    companion object {
        @Volatile var moshi: Moshi? = null
    }
}