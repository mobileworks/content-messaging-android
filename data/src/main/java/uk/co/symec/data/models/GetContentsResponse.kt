package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.RoomMapper
import uk.co.symec.data.db.entity.ContentEntity
import java.util.*

@JsonClass(generateAdapter = true)
data class GetContentsResponse(
    val current_page: Int?,
    val data: List<GetContentResponse>?,
    val first_page_url: String?,
    val from: Int?,
    val next_page_url: String?,
    val path: String?,
    val per_page: Int?,
    val prev_page_url: String?,
    val to: Int?,
    override var message: String?
) : RoomMapper<List<ContentEntity>> {

    override fun mapToRoomEntity(): List<ContentEntity> {
        val myList = mutableListOf<ContentEntity>()
        data?.let {
            for (content in it) {
                myList.add(
                    ContentEntity(
                        content.id ?: "",
                        content.title,
                        content.text,
                        (content.is_internal ?: 0) > 0,
                        (content.send_notification ?: 0) > 0,
                        content.created_at ?: Date(),
                        content.updated_at ?: Date(),
                        content.status ?: 0,
                        content.bookmark ?: 0,
                        content.signature,
                        (content.confirmation_required ?: 0) > 0,
                        content.medias?.map { x -> x.mapToRoomEntity() } ?: mutableListOf())
                )
            }
        }
        return myList
    }
}