package uk.co.symec.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.ContentModel
import java.util.*

@Entity(
    tableName = "content"
)
data class ContentEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "text") val text: String?,
    @ColumnInfo(name = "is_internal") val isInternal: Boolean,
    @ColumnInfo(name = "send_notification") val sendNotification: Boolean,
    @ColumnInfo(name = "created_at") val createdAt: Date,
    @ColumnInfo(name = "updated_at") val updatedAt: Date,
    @ColumnInfo(name = "status") var status: Int,
    @ColumnInfo(name = "bookmark") var bookmark: Int = 0,
    @ColumnInfo(name = "signature") var signature: String?,
    @ColumnInfo(name = "confirmation_required") var confirmationRequired: Boolean,
    @ColumnInfo(name = "media") val mediaList: List<MediaEntity>
) : DomainMapper<ContentModel> {

    override fun mapToDomainModel() = ContentModel(
        id,
        title ?: "",
        text ?: "",
        isInternal,
        sendNotification,
        createdAt,
        updatedAt,
        status,
        signature,
        bookmark,
        confirmationRequired,
        mediaList.map { x -> x.mapToDomainModel() })

    @Ignore
    override var message: String? = null
}