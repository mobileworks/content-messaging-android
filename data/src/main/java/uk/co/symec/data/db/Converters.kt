package uk.co.symec.data.db

import androidx.room.TypeConverter
import com.squareup.moshi.Types
import uk.co.symec.data.db.entity.MediaEntity
import java.util.*

/**
 * Type converters to allow Room to reference complex data types.
 */
class Converters {

    @TypeConverter
    fun dateToDatestamp(date: Date?): Long = date?.time ?: 0L

    @TypeConverter
    fun datestampToDate(value: Long): Date =
        Date().apply { time = value }

    @TypeConverter
    fun fromMediaToJson(medias: List<MediaEntity>?): String? {
        val type = Types.newParameterizedType(List::class.java, MediaEntity::class.java)
        val adapter = AppDatabase.moshi?.adapter<List<MediaEntity>>(type)
        return adapter?.toJson(medias)
    }

    @TypeConverter
    fun fromJsonToMedia(value: String): List<MediaEntity>? {
        val type = Types.newParameterizedType(List::class.java, MediaEntity::class.java)
        val adapter = AppDatabase.moshi?.adapter<List<MediaEntity>>(type)
        return adapter?.fromJson(value)
    }
}
