package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.MessageContentModel

@JsonClass(generateAdapter = true)
data class MessageResponse (
    override var message: String?
) : DomainMapper<MessageContentModel> {
    override fun mapToDomainModel() = MessageContentModel(message)
}