package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.StatementModel

@JsonClass(generateAdapter = true)
data class GetStatementResponse(
    val statement: String?,
    override var message: String?
) : DomainMapper<StatementModel> {
    override fun mapToDomainModel() = StatementModel(statement, message)
}