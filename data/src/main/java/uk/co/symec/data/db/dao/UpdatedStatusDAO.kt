package uk.co.symec.data.db.dao

import androidx.room.*
import uk.co.symec.data.db.entity.UpdatedStatusEntity

@Dao
interface UpdatedStatusDAO {
    @Query("SELECT * FROM updated_status")
    fun getAllUpdatedStatus(): List<UpdatedStatusEntity>

    @Query("SELECT * FROM updated_status WHERE content_id = :guid LIMIT 1")
    fun getUpdatedStatus(guid: String): UpdatedStatusEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUpdateStatus(updateStatusEntity: UpdatedStatusEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateUpdateStatus(updateStatusEntity: UpdatedStatusEntity): Int

    @Delete
    suspend fun deleteUpdateStatus(updateStatusEntity: UpdatedStatusEntity): Int

    @Transaction
    suspend fun updateStatus(deviceId: String, id: String, status: Int): Pair<UpdatedStatusEntity?, Boolean> {
        var updated = false
        var updateStatus = getUpdatedStatus(id)
        if (updateStatus == null) {
            updateStatus = UpdatedStatusEntity(id, status, deviceId)
            insertUpdateStatus(updateStatus)
            updated = true
        } else {
            if (updateStatus.status != status) {
                updateStatus.status = status
                if (updateUpdateStatus(updateStatus) > 0) {
                    updated = true
                }
            }
        }
        return Pair(updateStatus, updated)
    }
}