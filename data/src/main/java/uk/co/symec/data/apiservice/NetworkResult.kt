package uk.co.symec.data.apiservice

import retrofit2.Response
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.models.GetContentsResponse
import uk.co.symec.domain.model.*
import java.io.IOException

interface DomainMapper<T : Any> {
    var message: String?
    fun mapToDomainModel(): T
}

interface RoomMapper<out T : Any> {
    var message: String?
    fun mapToRoomEntity(): T
}

inline fun <T : Any> Response<T>.onSuccess(action: (T) -> Unit): Response<T> {
    if (isSuccessful) body()?.run(action)
    return this
}

inline fun <T : Any> Response<T>.onFailure(action: (HttpError) -> Unit) {
    if (!isSuccessful) errorBody()?.run { action(HttpError(Throwable(message()), code())) }
}

/**
 * Use this if you need to cache data after fetching it from the api, or retrieve something from cache
 */

inline fun <T : RoomMapper<R>, R : DomainMapper<U>, U : Any> Response<T>.getData(
    cacheAction: (R) -> Unit,
    fetchFromCacheAction: () -> R?
): Result<U> {
    try {
        onSuccess {
            return if (it.message == "Archived") {
                Archived(true)
            } else {
                val databaseEntity = it.mapToRoomEntity()
                cacheAction(databaseEntity)
                Success(databaseEntity.mapToDomainModel())
            }
        }
        onFailure {
            return if (it.errorCode == 410) {
                Logout(true)
            } else {
                val cachedModel = fetchFromCacheAction()
                return if (cachedModel != null) {
                    SuccessButConnectionError(HttpErrorPackage(cachedModel.mapToDomainModel(), it))
                } else {
                    Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                }
            }
        }
        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
    } catch (e: IOException) {
        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
    }
}

//inline fun <T : RoomMapper<R>, R : DomainMapper<U>, U : Any> Response<List<T>>.getDataList(
//    cacheAction: (List<R>) -> Unit,
//    fetchFromCacheAction: () -> List<R>
//): Result<List<U>> {
//    try {
//        onSuccess {
//            val xy = mutableListOf<R>()
//            it.forEach { x ->
//                xy.add(x.mapToRoomEntity())
//            }
//            cacheAction(xy)
//            val cachedModel = fetchFromCacheAction()
//            val resultList = mutableListOf<U>()
//            cachedModel.forEach {
//                resultList.add(it.mapToDomainModel())
//            }
//            return Success(resultList)
//        }
//        onFailure {
//            return if (it.errorCode == 410) {
//                Logout(true)
//            } else {
//                val cachedModel = fetchFromCacheAction()
//                if (cachedModel != null) {
//                    val list = mutableListOf<U>()
//                    cachedModel.forEach { item ->
//                        list.add(item.mapToDomainModel())
//                    }
//                    SuccessButConnectionError(HttpErrorPackage(list, it))
//                } else {
//                    Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
//                }
//            }
//        }
//        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
//    } catch (e: IOException) {
//        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
//    }
//}

inline fun <T : RoomMapper<R>, R : DomainMapper<U>, U : Any> Response<GetContentsResponse>.getContentList(
    cacheAction: (List<ContentEntity>) -> Unit,
    fetchFromCacheAction: () -> List<R>
): Pair<Result<List<U>>, Boolean> {
    try {
        onSuccess {
            if (it.message == "Archived") {
                return Pair(Archived(true), it.next_page_url != null)
            } else {
                val xy = mutableListOf<ContentEntity>()
                it.data?.forEach { x ->
                    xy.add(x.mapToRoomEntity())
                }
                cacheAction(xy)
                val cachedModel = fetchFromCacheAction()
                val xx = mutableListOf<U>()
                cachedModel.forEach { x ->
                    xx.add(x.mapToDomainModel())
                }

                val yy = mutableListOf<ContentModel>()
                xy.forEach { y ->
                    yy.add(y.mapToDomainModel())
                }
                return Pair(SuccessDouble(DoubleDataPackage(xx, yy)), it.next_page_url != null)
            }
        }
        onFailure {
            return if (it.errorCode == 410) {
                Pair(Logout(true), false)
            } else {
                val cachedModel = fetchFromCacheAction()
                if (cachedModel != null) {
                    val list = mutableListOf<U>()
                    cachedModel.forEach { item ->
                        list.add(item.mapToDomainModel())
                    }
                    Pair(SuccessButConnectionError(HttpErrorPackage(list, it)), false)
                } else {
                    Pair(Failure(HttpError(Throwable(DB_ENTRY_ERROR))), false)
                }
            }
        }
        return Pair(Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR))), false)
    } catch (e: IOException) {
        return Pair(Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR))), false)
    }
}

inline fun <T : DomainMapper<R>, R : Any> Response<T>.updateData(
    cacheAction: (T) -> Boolean
): Boolean {
    try {
        onSuccess {
            return cacheAction(it)
        }
        onFailure {
            return false
        }
        return false
    } catch (e: IOException) {
        return false
    }
}

/**
 * Use this when communicating only with the api service
 */
fun <T : DomainMapper<R>, R : Any> Response<T>.getDataApi(): Result<R> {
    try {
        onSuccess {
            return if (it.message == "Archived") {
                Archived(true)
            } else {
                Success(it.mapToDomainModel())
            }
        }
        onFailure {
            return if (it.errorCode == 410) {
                Logout(true)
            } else {
                Failure(it)
            }
        }
        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
    } catch (e: IOException) {
        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
    }
}