package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.RegisterToGroupModel

@JsonClass(generateAdapter = true)
data class RegisterToGroupResponse(
    val token: String?,
    val group_name: String?,
    override var message: String?
) : DomainMapper<RegisterToGroupModel> {
    override fun mapToDomainModel() = RegisterToGroupModel(token, group_name, message)
}