package uk.co.symec.data.repository

import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.getDataApi
import uk.co.symec.domain.model.RegisterToGroupModel
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.repository.RegisterRepository
import javax.inject.Inject

class RegisterRepositoryImpl @Inject constructor(
    private val api: ConnectionService
) : BaseRepository(),
    RegisterRepository {
    override suspend fun registerIntoGroup(
        assetNo: String,
        fcm: String,
        groupId: String,
        model: String,
        imei: String
    ): Result<RegisterToGroupModel> {
        return fetchData(
            dataProvider = {
               api.registerInTheGroup(assetNo, fcm, groupId, model, imei).getDataApi()
            }
        )
    }
}

