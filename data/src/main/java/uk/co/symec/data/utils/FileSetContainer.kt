package uk.co.symec.data.utils

import uk.co.symec.domain.model.MediaModel

class FileSetContainer(val mediaId: String?, var contentGuid: String, val fileName: String?, var url: String?, var devicePath: String, var mediaModel: MediaModel) {
    var downloadId: Long = 0
}

