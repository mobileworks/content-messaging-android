package uk.co.symec.data.apiservice

const val GENERAL_NETWORK_ERROR = "Something went wrong, please try again."
const val NO_INTERNET_CONNECTION = "No internet connection."