package uk.co.symec.data.db.dao

import androidx.room.*
import uk.co.symec.data.db.entity.MediaEntity
import java.util.*

@Dao
interface MediaDAO {
    @Query("SELECT * FROM media")
    fun getMedias(): List<MediaEntity>

    @Query("SELECT * FROM media WHERE content_id = :contentId")
    fun getMediasForContentId(contentId: String): List<MediaEntity>

    @Query("SELECT * FROM media WHERE id = :guid LIMIT 1")
    fun getMedia(guid: String): MediaEntity?

    @Query("SELECT * FROM media WHERE id = :guid AND content_id = :contentId LIMIT 1")
    fun getMediaWithContentId(guid: String, contentId: String): MediaEntity?

    @Query("SELECT * FROM media WHERE savedPath != \"\" ORDER BY seen_at ASC LIMIT 1")
    fun getLeastUsedMedia(): MediaEntity?

    @Query("SELECT COUNT(*) FROM media WHERE savedPath != \"\"")
    fun getMediaCount(): Int

    @Query("DELETE FROM media WHERE id = (SELECT id FROM media ORDER BY created_at ASC LIMIT 1)")
    fun deleteOldest(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMedia(mediaEntity: MediaEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateMedia(mediaEntity: MediaEntity): Int

    @Delete
    suspend fun deleteMedia(mediaEntity: MediaEntity): Int

    @Transaction
    suspend fun updateMediaQuery(mediaEntity: MediaEntity): Long {
        val mediaFromDb = getMediaWithContentId(mediaEntity.id, mediaEntity.contentId)
        return if (mediaFromDb == null) {
            if (mediaEntity.seenAt == null) {
                mediaEntity.seenAt = Date()
            }
            insertMedia(mediaEntity)
        } else {
            if (mediaEntity.savedPath.isNotBlank()) {
                mediaFromDb.savedPath = mediaEntity.savedPath
            }
            mediaFromDb.score = mediaEntity.score
            updateMedia(mediaFromDb).toLong()
        }
    }

    @Transaction
    suspend fun updateScore(id: String, score: Int): Pair<MediaEntity?, Boolean> {
        getMedia(id)?.let { media ->
            media.score = score
            val updateCount = updateMedia(media)
            return Pair(media, updateCount > 0)
        }
        return Pair(null, false)
    }

    @Transaction
    suspend fun updateSeenAt(id: String): Pair<Date?, Boolean> {
        getMedia(id)?.let { media ->
            media.seenAt = Date()
            val updateCount = updateMedia(media)
            return Pair(media.seenAt, updateCount > 0)
        }
        return Pair(null, false)
    }
}