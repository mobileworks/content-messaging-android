package uk.co.symec.data.repository


import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.StatFs
import android.os.storage.StorageManager
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.withContext
import uk.co.symec.data.apiservice.GENERAL_NETWORK_ERROR
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.entity.MediaEntity
import uk.co.symec.data.di.NetworkModule
import uk.co.symec.data.utils.FileSetContainer
import uk.co.symec.domain.model.*
import uk.co.symec.domain.repository.MediaRepository
import java.io.File
import java.util.*
import javax.inject.Inject

class MediaRepositoryImpl @Inject constructor(
    private val dao: MediaDAO,
    @ApplicationContext private val context: Context
) : BaseRepository(),
    MediaRepository {

    override suspend fun getMedias(content: ContentModel): Result<Boolean> {
        return fetchData { saveFiles(content) }
    }

    override suspend fun getMedia(id: String): Result<MediaModel> {
        return fetchDataFromDb {
            dao.getMedia(id) ?: MediaEntity(
                "",
                "",
                null,
                null,
                null,
                "",
                "",
                Date(),
                Date(),
                null,
                "",
                0
            )
        }
    }

    override suspend fun updateSeenAt(id: String): Result<Boolean> {
        return try {
            val result = dao.updateSeenAt(id)
            if (result.second) {
                Success(true)
            } else {
                Success(false)
            }
        } catch (e: Exception) {
            Success(false)
        }
    }

    override suspend fun saveMedia(media: MediaModel): Result<String> {
        val mediaEntity = MediaEntity(
            media.id,
            media.contentId,
            media.name,
            media.extension,
            media.size,
            media.link,
            media.thumbnail_link,
            media.createdAt,
            media.updatedAt,
            media.seenAt,
            media.savedPath,
            media.score.toInt()
        )

        return withContext(contextProvider.io) {
            val dbResult = dao.updateMediaQuery(mediaEntity)
            if (dbResult > 0) {
                Success(media.name ?: "")
            } else {
                Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
            }
        }
    }

    private suspend fun saveFiles(content: ContentModel): Result<Boolean> {
        //check space loop
        var saveToSdCard = false
        if (checkIsSdCardAvailable()) {
            var check = !checkSdCardFreeSpace()
            while (check) {
                dao.getLeastUsedMedia()?.let { mediaEntity ->
                    val fileToDelete = File(mediaEntity.savedPath)
                    if (fileToDelete.exists()) {
                        if (fileToDelete.delete()) {
                            println("file Deleted :" + mediaEntity.savedPath)
                            dao.deleteMedia(mediaEntity)
                        } else {
                            println("file not Deleted :" + mediaEntity.savedPath)
                        }
                    }
                }
                if (dao.getMediaCount() == 0) {
                    freeSpaceCheck()
                    check = false
                } else {
                    check = !checkSdCardFreeSpace()
                    if (!check) {
                        saveToSdCard = true
                    }
                }
            }
        } else {
            freeSpaceCheck()
        }

        println(content)
        content.media.forEach { item ->
            val mediaItem = dao.getMediaWithContentId(item.id, content.id)
            println(mediaItem)
            println(item)

            var newMedia = true

            NetworkModule.getFileSetContainerList().forEach { value ->
                if (value.contentGuid == content.id && value.mediaId == item.id) {
                    newMedia = false
                }
            }

            if (newMedia) {
                if (mediaItem == null) {
                    //no database entry file hasn't been downloaded yet
                    val xx = download(item, content.id, saveToSdCard)
                    NetworkModule.getFileSetContainerList().add(xx)
                } else {
                    //media item has been created but not fully downloaded
                    if (mediaItem.savedPath.isBlank()) {
                        val xx = download(item, content.id, saveToSdCard)
                        NetworkModule.getFileSetContainerList().add(xx)
                    }
                    //media item has been updated
                    if (mediaItem.name != item.name || mediaItem.size != item.size || mediaItem.extension != item.extension) {
                        val xx = download(item, content.id, saveToSdCard)
                        NetworkModule.getFileSetContainerList().add(xx)
                    }
                }
            }
        }
        return Failure(HttpError(Throwable(GENERAL_NETWORK_ERROR)))
    }

    private suspend fun freeSpaceCheck() {
        while (!checkFreeSpace()) {
            dao.getLeastUsedMedia()?.let { mediaEntity ->
                val fileToDelete = File(mediaEntity.savedPath)
                if (fileToDelete.exists()) {
                    if (fileToDelete.delete()) {
                        println("file Deleted :" + mediaEntity.savedPath)
                        dao.deleteMedia(mediaEntity)
                    } else {
                        println("file not Deleted :" + mediaEntity.savedPath)
                    }
                }
            }
        }
    }

    private fun download(
        item: MediaModel,
        contentId: String,
        saveToExternal: Boolean = false
    ): FileSetContainer {

        val downloadsFolder = if (saveToExternal) {
            NetworkModule.getDownloadsSDFolder(context)
        } else {
            NetworkModule.getDownloadsFolder(context)
        }
        val directoryToSave = "${downloadsFolder}/${item.id}${contentId}"
        val direcToSave = File(directoryToSave)
        if (direcToSave.exists()) {
            direcToSave.deleteRecursively()
        }

        direcToSave.mkdirs()

        val fileSet = FileSetContainer(
            item.id,
            contentId,
            item.name,
            item.link,
            "${direcToSave.absolutePath}/${item.name}",
            item
        )

        val request = DownloadManager.Request(Uri.parse(fileSet.url))
            .setTitle("Downloading new " + fileSet.fileName) // Title of the Download Notification
            .setDescription("Downloading") // Description of the Download Notification
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE) // Visibility of the download Notification
            .setDestinationUri(Uri.parse("file://${fileSet.devicePath}")) // Uri of the destination file
            .setAllowedOverMetered(true) // Set if download is allowed on Mobile network
            .setAllowedOverRoaming(false) // Set if download is allowed on roaming network

        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as? DownloadManager
        try {
            val downloadId = downloadManager?.enqueue(request) ?: 0L
            if (downloadId > 0) {
                fileSet.downloadId = downloadId
            }
            return fileSet
        } catch (ex: java.lang.Exception) {
            println(ex.localizedMessage)
            return fileSet
        }
    }

    private fun checkFreeSpace(): Boolean {
        val stat = StatFs(context.filesDir.path)
        val total = stat.totalBytes.toFloat() / (1024 * 1024)
        val free = stat.availableBytes.toFloat() / (1024 * 1024)
        val ratio = free / total

        return ratio >= 0.1
    }

    private fun checkSdCardFreeSpace(): Boolean {
        val files = context.getExternalFilesDirs(null)

        return if (files.size == 2) {
            val stat = StatFs(files[1].path)
            val total = stat.totalBytes.toFloat() / (1024 * 1024)
            val free = stat.availableBytes.toFloat() / (1024 * 1024)
            val ratio = free / total
            ratio >= 0.04
        } else {
            false
        }
    }

    private fun checkIsSdCardAvailable(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val storageManager = context.getSystemService(Context.STORAGE_SERVICE) as StorageManager
            val primary = storageManager.primaryStorageVolume.uuid
            storageManager.storageVolumes.forEach {
                if (it.uuid != primary) {
                    return it.isRemovable
                }
            }
        } else {
            val dirs = context.getExternalFilesDirs(null)
            if (dirs.size == 2) {
                if (Environment.isExternalStorageRemovable(dirs[1])) {
                    return true
                }
            }
        }
        return false
    }
}

