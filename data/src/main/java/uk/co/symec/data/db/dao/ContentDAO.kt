package uk.co.symec.data.db.dao

import androidx.room.*
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.data.db.entity.ContentWithMediaEntity

@Dao
interface ContentDAO {
    @Query("SELECT * FROM content")
    fun getAllContents(): List<ContentEntity>

    @Transaction
    @Query("SELECT * FROM content WHERE is_internal IS :isInternal ORDER BY bookmark DESC, updated_at DESC")
    fun getContents(isInternal: Boolean): List<ContentEntity>

    @Query("SELECT * FROM content ORDER BY updated_at DESC LIMIT 5")
    fun getLatestContents(): List<ContentEntity>

    @Query("SELECT COUNT(id) FROM content WHERE is_internal IS :isInternal AND status == 0")
    fun checkIfNewStatusOnList(isInternal: Boolean): Int

    @Query("SELECT * FROM content WHERE id = :guid LIMIT 1")
    fun getContent(guid: String): ContentEntity?

    @Query("SELECT * FROM content WHERE id = :guid LIMIT 1")
    fun getContentWithMedia(guid: String): ContentWithMediaEntity?

    @Query("SELECT * FROM content WHERE title = :title LIMIT 1")
    fun getContentByTitle(title: String): ContentEntity?

    @Query("UPDATE content SET status = :status WHERE id = :id")
    fun updateStatus(id: String, status: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertContent(contentEntity: ContentEntity): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateContent(contentEntity: ContentEntity): Int

    @Delete
    suspend fun deleteContent(contentEntity: ContentEntity): Int

    @Transaction
    suspend fun updateContent(id: String, status: Int): Pair<ContentEntity?, Boolean> {
        getContent(id)?.let { content ->
            var updated = false
            if (content.status != status) {
                content.status = status
                updateContent(content)
                updated = true
            }
            return Pair(content, updated)
        }
        return Pair(null, false)
    }

    @Transaction
    suspend fun updateSignature(id: String, sign: String): Pair<ContentEntity?, Boolean> {
        getContent(id)?.let { content ->
            var updated = false
            if (content.signature != sign) {
                content.signature = sign
                updateContent(content)
                updated = true
            }
            return Pair(content, updated)
        }
        return Pair(null, false)
    }

    @Transaction
    suspend fun updateContentFromList(contentEntity: ContentEntity) {
        val contentFromDb = getContent(contentEntity.id)
        if (contentFromDb == null) {
            insertContent(contentEntity)
        } else {
            if (contentFromDb.status > contentEntity.status) {
                contentEntity.status = contentFromDb.status
            }
            updateContent(contentEntity)
        }
    }
}