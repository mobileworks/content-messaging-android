package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.RoomMapper
import uk.co.symec.data.db.entity.ContentEntity
import java.util.*

@JsonClass(generateAdapter = true)
data class GetContentResponse(
    val id: String?,
    val title: String?,
    val text: String?,
    val is_internal: Int?,
    val send_notification: Int?,
    val created_at: Date?,
    val updated_at: Date?,
    val status: Int?,
    val signature: String?,
    val bookmark: Int?,
    val confirmation_required: Int?,
    val medias: List<GetMediaResponse>?,
    override var message: String?
) : RoomMapper<ContentEntity> {

    override fun mapToRoomEntity() =
        ContentEntity(id ?: "", title, text, (is_internal ?: 0) > 0, (send_notification ?: 0) > 0, created_at ?: Date(), updated_at ?: Date(), status ?: 0, bookmark ?: 0, signature, (confirmation_required ?: 0) > 0, medias?.map { x -> x.mapToRoomEntity() } ?: mutableListOf())
}