package uk.co.symec.data.repository

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.updateData
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.domain.model.*
import uk.co.symec.domain.repository.SignatureRepository
import java.io.File
import javax.inject.Inject


class SignatureRepositoryImpl @Inject constructor(
    private val api: ConnectionService
) : BaseRepository(),
    SignatureRepository {
    override suspend fun sendSignature(
        file: File,
        signName: String,
        deviceId: String,
        contentId: String
    ): Result<Boolean> {
        return fetchData(
            dataProvider = {
                val filePart = MultipartBody.Part.createFormData(
                    "signature",
                    file.name,
                    RequestBody.create(MediaType.parse("image/*"), file)
                )
                if (api.sendSignature(filePart, signName, deviceId, contentId).updateData(
                        cacheAction = {
                            return@updateData it.message == "Success"
                        }
                    )
                ) {
                    Success(true)
                } else {
                    Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                }
            }
        )
    }
}

