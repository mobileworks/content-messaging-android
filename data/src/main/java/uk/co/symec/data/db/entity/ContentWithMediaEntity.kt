package uk.co.symec.data.db.entity

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.ContentModel

data class ContentWithMediaEntity(
    @Embedded val content: ContentEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "content_id"
    )
    val medias: List<MediaEntity>
): DomainMapper<ContentModel> {

    override fun mapToDomainModel() = ContentModel(
        content.id,
        content.title ?: "",
        content.text ?: "",
        content.isInternal,
        content.sendNotification,
        content.createdAt,
        content.updatedAt,
        content.status,
        content.signature,
        content.bookmark,
        content.confirmationRequired,
        medias.map { x -> x.mapToDomainModel() })

    @Ignore
    override var message: String? = null
}