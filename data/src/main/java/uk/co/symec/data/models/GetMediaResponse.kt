package uk.co.symec.data.models

import com.squareup.moshi.JsonClass
import uk.co.symec.data.apiservice.RoomMapper
import uk.co.symec.data.db.entity.MediaEntity
import java.util.*

@JsonClass(generateAdapter = true)
data class GetMediaResponse(
    val id: String?,
    val name: String?,
    val extension: String?,
    val size: String?,
    val link: String?,
    val thumbnail_link: String?,
    val created_at: Date?,
    val updated_at: Date?,
    val score: Int? = 0,
    override var message: String?
) : RoomMapper<MediaEntity> {

    override fun mapToRoomEntity() = MediaEntity(
        id ?: "", "", name, extension, size, link ?: "", thumbnail_link, created_at ?: Date(), updated_at ?: Date(), null, "", score ?: 0)
}