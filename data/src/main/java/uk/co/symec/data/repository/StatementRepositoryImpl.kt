package uk.co.symec.data.repository

import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.getDataApi
import uk.co.symec.domain.model.Result
import uk.co.symec.domain.model.StatementModel
import uk.co.symec.domain.repository.StatementRepository
import javax.inject.Inject

class StatementRepositoryImpl @Inject constructor(
    private val api: ConnectionService
) : BaseRepository(),
    StatementRepository {
    override suspend fun getStatement(deviceId: String): Result<StatementModel> {
        return fetchData(
            dataProvider = {
                api.getStatement(deviceId).getDataApi()
            }
        )
    }
}

