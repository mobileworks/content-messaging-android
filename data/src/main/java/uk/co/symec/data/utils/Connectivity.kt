package uk.co.symec.data.utils

interface Connectivity {
  fun hasNetworkAccess(): Boolean
}