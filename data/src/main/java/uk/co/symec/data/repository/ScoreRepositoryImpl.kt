package uk.co.symec.data.repository

import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.updateData
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.domain.model.*
import uk.co.symec.domain.repository.ScoreRepository
import javax.inject.Inject

class ScoreRepositoryImpl @Inject constructor(
    private val api: ConnectionService,
    private val daoMedia: MediaDAO
) : BaseRepository(),
    ScoreRepository {
    override suspend fun sendScore(
        score: Int,
        deviceId: String,
        mediaId: String
    ): Result<Boolean> {
        return fetchData(
            dataProvider = {
                if (api.sendScore(deviceId, mediaId, score).updateData(
                    cacheAction = {
                        return@updateData it.message == "Success"
                    }
                )) {
                    daoMedia.updateScore(mediaId, score)
                    Success(true)
                } else {
                    Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                }
            }
        )
    }
}

