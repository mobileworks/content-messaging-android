package uk.co.symec.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import uk.co.symec.data.apiservice.DomainMapper
import uk.co.symec.domain.model.MediaModel
import java.util.*

@Entity(
    tableName = "media",
    primaryKeys = ["id", "content_id"]
)
data class MediaEntity(
    @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "content_id") var contentId: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "extension") val extension: String?,
    @ColumnInfo(name = "size") val size: String?,
    @ColumnInfo(name = "link") val link: String,
    @ColumnInfo(name = "thumbnail_link") val thumbnailLink: String?,
    @ColumnInfo(name = "created_at") val createdAt: Date,
    @ColumnInfo(name = "updated_at") val updatedAt: Date,
    @ColumnInfo(name = "seen_at") @Transient var seenAt: Date? = null,
    @ColumnInfo(name = "savedPath") var savedPath: String,
    @ColumnInfo(name = "score") var score: Int = 0,
) : DomainMapper<MediaModel> {
    override fun mapToDomainModel() = MediaModel(
        id,
        contentId,
        name,
        extension,
        size,
        link,
        thumbnailLink,
        createdAt,
        updatedAt,
        seenAt,
        savedPath,
        score.toFloat()
    )

    @Ignore
    override var message: String? = null
}