package uk.co.symec.data.utils

class Constants {
    companion object {
        const val DATABASE_NAME = "content-messaging-db"
        const val GLOBAL = "global"
        const val FCM = "FCM"
        const val TOKEN = "token"
        const val ANDROID_ID = "androidId"
        const val ASSET_ID = "assetId"
        const val GROUP_ID = "groupId"
        const val GROUP_NAME = "groupName"
        const val IS_DEVELOPER = "isDeveloper"
        const val IS_OFFLINE = "isOffline"

        const val IS_INTERNAL = "isInternal"
        const val CONTENT_ID = "contentId"
    }
}