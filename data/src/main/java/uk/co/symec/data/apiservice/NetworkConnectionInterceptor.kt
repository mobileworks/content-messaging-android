package uk.co.symec.data.apiservice

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Protocol
import okhttp3.Response
import okhttp3.ResponseBody
import uk.co.symec.data.di.NetworkModule
import java.io.IOException

class NetworkConnectionInterceptor(private val context: Context) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
            .addHeader("Accept", "application/json")
            .addHeader("Content-Type", "application/json")
        chain.request().apply {
            if (!url().encodedPathSegments().contains("token") &&
                NetworkModule.provideBaseUrl().contains(url().host())
            ) {
                NetworkModule.getToken(context)?.let { token ->
                    requestBuilder.addHeader("Authorization", "Bearer $token")
                }
            }
        }
        val request = requestBuilder.build()
        return try {
            chain.proceed(request)
        } catch (e: IOException) {
            print(e.localizedMessage)
            val customResponse = Response.Builder()
                .code(600)
                .message(e.message ?: "Error")
                .body(ResponseBody.create(null, e.message ?: ""))
                .protocol(Protocol.HTTP_1_0)
                .request(request)
            customResponse.build()
        }
    }
}