package uk.co.symec.data.repository

import kotlinx.coroutines.withContext
import uk.co.symec.data.apiservice.*
import uk.co.symec.data.db.DB_ENTRY_ERROR
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.dao.UpdatedStatusDAO
import uk.co.symec.data.db.entity.ContentEntity
import uk.co.symec.domain.model.*
import uk.co.symec.domain.repository.ContentRepository
import java.io.File
import javax.inject.Inject

class ContentRepositoryImpl @Inject constructor(
    private val api: ConnectionService,
    private val daoContent: ContentDAO,
    private val daoMedia: MediaDAO,
    private val daoUpdateStatus: UpdatedStatusDAO
) : BaseRepository(), ContentRepository {

    override suspend fun getContent(guid: String): Result<ContentModel> {
        return fetchDataFromDb { daoContent.getContentWithMedia(guid) }
    }

    override suspend fun deleteContentAndReturnList(
        guid: String,
        isInternal: Boolean
    ): Result<List<ContentModel>> {
        return withContext(contextProvider.io) {
            daoContent.getContent(guid)?.let {
                daoContent.deleteContent(it)
                deleteMedias(it)
            }
            Success(daoContent.getContents(isInternal).map { x -> x.mapToDomainModel() })
        }
    }

    private suspend fun deleteMedias(it: ContentEntity) {
        var directoryToDownload = ""
        daoMedia.getMediasForContentId(it.id).forEach { mediaToRemove ->
            val fileToDelete = File(mediaToRemove.savedPath)
            directoryToDownload = fileToDelete.parent ?: ""
            if (fileToDelete.exists()) {
                if (fileToDelete.delete()) {
                    println("file deleted: ${mediaToRemove.savedPath}")
                } else {
                    println("file not deleted: ${mediaToRemove.savedPath}")
                }
            }
            daoMedia.deleteMedia(mediaToRemove)
        }
        val directory = File(directoryToDownload)
        if (directory.exists()) {
            if (directory.delete()) {
                println("dir deleted: $directory")
            } else {
                println("dir not deleted: $directory")
            }
        }
    }

    override suspend fun updateContentAndReturnList(
        deviceId: String,
        guid: String,
        isInternal: Boolean
    ): Result<List<ContentModel>> {
        fetchData(
            apiDataProvider = {
                api.getContent(deviceId, guid).getData(
                    cacheAction = {
                        daoContent.insertContent(it)
                    },
                    fetchFromCacheAction = {
                        daoContent.getContent(guid)
                    }
                )
            },
            dbDataProvider = {
                daoContent.getContent(guid)
            }
        )
        return withContext(contextProvider.io) {
            Success(daoContent.getContents(isInternal).map { x -> x.mapToDomainModel() })
        }
    }

    override suspend fun getContents(
        deviceId: String,
        isInternal: Boolean,
        page: Int
    ): Pair<Result<List<ContentModel>>, Boolean> {
        return fetchContentList(
            apiDataProvider = {
                val result = api.getContents(deviceId, page).getContentList(
                    cacheAction = {
                        it.forEach { ce ->
                            daoContent.updateContentFromList(ce)
                            ce.mediaList.forEach { media ->
                                media.contentId = ce.id
                                daoMedia.updateMediaQuery(media)
                            }
                            val map = ce.mediaList.map { it.id }
                            daoMedia.getMediasForContentId(ce.id).forEach { me ->
                                if (!map.contains(me.id)) {
                                    daoMedia.deleteMedia(me)
                                }
                            }
                        }
                    },
                    fetchFromCacheAction = {
                        daoContent.getContents(isInternal)
                    }
                )
                result
            }
        ) {
            daoContent.getContents(isInternal)
        }
    }

    override suspend fun getContentsFromDb(isInternal: Boolean): Result<List<ContentModel>> {
        return fetchDataListFromDb { daoContent.getContents(isInternal) }
    }

    override suspend fun getAllContentsFromDb(): Result<List<ContentModel>> {
        return fetchDataListFromDb { daoContent.getAllContents() }
    }

    override suspend fun deleteContentFromDb(list: List<String>): Result<Boolean> {
        return fetchData { deleteContent(list) }
    }

    private suspend fun deleteContent(list: List<String>): Result<Boolean> {
        return try {
            var deletedCounter = 0
            daoContent.getAllContents().forEach { savedItem ->
                if (!list.contains(savedItem.id)) {
                    deletedCounter++
                    daoContent.deleteContent(savedItem)
                    deleteMedias(savedItem)
                }
            }
            println("Deleted $deletedCounter entries")
            Success(true)
        } catch (e: Exception) {
            Success(false)
        }
    }

    override suspend fun getLatestContents(
    ): Result<List<ContentModel>> {
        return fetchDataListFromDb { daoContent.getLatestContents() }
    }

    override suspend fun updateStatusContent(
        deviceId: String,
        contentId: String,
        status: Int
    ): Result<ContentModel> {
        return fetchData(
            apiDataProvider = {
                if (api.contentStatusUpdate(deviceId, contentId, status).updateData(
                        cacheAction = {
                            return@updateData it.message == "Success"
                        }
                    )
                ) {
                    val result = daoContent.updateContent(contentId, status)
                    if (result.first != null) {
                        if (result.second) {
                            Success(result.first!!.mapToDomainModel())
                        } else {
                            SuccessButNoInternet(result.first!!.mapToDomainModel())
                        }
                    } else {
                        Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                    }
                } else {
                    val content = daoContent.getContent(contentId)
                    if (content != null) {
                        SuccessButNoInternet(content.mapToDomainModel())
                    } else {
                        Failure(HttpError(Throwable(DB_ENTRY_ERROR)))
                    }
                }
            },
            dbDataProvider = {
                //offline mode save to update status first
                daoUpdateStatus.updateStatus(deviceId, contentId, status)
                daoContent.updateContent(contentId, status).first
            }
        )
    }

    override suspend fun updateSignatureContent(
        contentId: String,
        signLink: String
    ): Result<ContentModel> {
        return fetchDataFromDb {
            daoContent.updateSignature(contentId, signLink)
            daoContent.getContentWithMedia(contentId)
        }
    }

    override suspend fun getUpdatedStatuses(): Result<List<UpdatedStatusModel>> {
        return fetchDataListFromDb { daoUpdateStatus.getAllUpdatedStatus() }
    }

    override suspend fun deleteUpdatedStatusFromDb(contentId: String): Result<Boolean> {
        return fetchData { deleteUpdatedStatus(contentId) }
    }

    private suspend fun deleteUpdatedStatus(contentId: String): Result<Boolean> {
        return try {
            daoUpdateStatus.getUpdatedStatus(contentId)?.let { savedItem ->
                daoUpdateStatus.deleteUpdateStatus(savedItem)
            }
            Success(true)
        } catch (e: Exception) {
            Success(false)
        }
    }
}

