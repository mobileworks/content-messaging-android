package uk.co.symec.data.di

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import uk.co.symec.data.db.AppDatabase
import uk.co.symec.data.db.dao.ContentDAO
import uk.co.symec.data.db.dao.MediaDAO
import uk.co.symec.data.db.dao.UpdatedStatusDAO
import uk.co.symec.data.utils.Constants
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            Constants.DATABASE_NAME)
            .addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4)
            .build()
            .apply { AppDatabase.moshi = NetworkModule.moshi }
    }

    @Provides
    fun provideContentDao(appDatabase: AppDatabase): ContentDAO {
        return appDatabase.contentDao()
    }

    @Provides
    fun provideMediaDao(appDatabase: AppDatabase): MediaDAO {
        return appDatabase.mediaDao()
    }

    @Provides
    fun provideUpdatedStatusDao(appDatabase: AppDatabase): UpdatedStatusDAO {
        return appDatabase.updatedStatusDao()
    }

    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                """ALTER TABLE media ADD COLUMN score INTEGER NOT NULL DEFAULT 0
                """.trimIndent()
            )

            database.execSQL(
                """ALTER TABLE content ADD COLUMN signature TEXT
                """.trimIndent()
            )

            database.execSQL(
                """ALTER TABLE content ADD COLUMN confirmation_required INTEGER NOT NULL DEFAULT TRUE
                """.trimIndent()
            )
        }
    }

    val MIGRATION_2_3 = object : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                """ALTER TABLE media ADD COLUMN seen_at INTEGER NULL DEFAULT NULL
                """.trimIndent()
            )
        }
    }

    val MIGRATION_3_4 = object : Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL(
                """ALTER TABLE content ADD COLUMN bookmark INTEGER NOT NULL DEFAULT 0
                """.trimIndent()
            )
        }
    }
}