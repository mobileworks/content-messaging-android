package uk.co.symec.data.apiservice

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*
import uk.co.symec.data.models.*

interface ConnectionService {
    @FormUrlEncoded
    @POST("api/token")
    suspend fun registerInTheGroup(
        @Query("asset_number") assetId: String,
        @Field("fcm") fcm: String,
        @Field("group_id") groupId: String,
        @Field("model") model: String,
        @Field("imei") imei: String
    ): Response<RegisterToGroupResponse>

    @PUT("api/device/{deviceId}/content/{contentId}/{status}")
    suspend fun contentStatusUpdate(
        @Path("deviceId") deviceId: String,
        @Path("contentId") contentId: String,
        @Path("status") status: Int
    ): Response<MessageResponse>

    @PUT("api/token")
    suspend fun updateDeviceInfo(
        @Query("deviceId") deviceId: String,
        @Field("fcmToken") fcmToken: String
    ): Response<MessageResponse>

    @GET("api/device/{deviceId}/content/{contentId}")
    suspend fun getContent(
        @Path("deviceId") deviceId: String,
        @Path("contentId") id: String
    ): Response<GetContentResponse>

    @GET("api/device/{deviceId}")
    suspend fun getContents(
        @Path("deviceId") deviceId: String,
        @Query("page") page: Int = 1
    ): Response<GetContentsResponse>

    @FormUrlEncoded
    @POST("api/score/device/{deviceId}/media/{mediaId}")
    suspend fun sendScore(
        @Path("deviceId") deviceId: String,
        @Path("mediaId") mediaId: String,
        @Field("score") score: Int
    ): Response<MessageResponse>

    @Multipart
    @POST("api/signature/device/{deviceId}/content/{contentId}")
    suspend fun sendSignature(
        @Part filePart: MultipartBody.Part,
        @Part("signature_name") signatureName: String,
        @Path("deviceId") deviceId: String,
        @Path("contentId") id: String
    ): Response<MessageResponse>

    @GET("api/device/{deviceId}/statement")
    suspend fun getStatement(
        @Path("deviceId") deviceId: String
    ): Response<GetStatementResponse>
}