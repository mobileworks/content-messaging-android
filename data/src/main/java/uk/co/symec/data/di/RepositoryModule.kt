package uk.co.symec.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import uk.co.symec.data.repository.*
import uk.co.symec.data.utils.Connectivity
import uk.co.symec.data.utils.ConnectivityImpl
import uk.co.symec.domain.repository.*
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideContentRepository(repository: ContentRepositoryImpl): ContentRepository = repository

    @Provides
    @Singleton
    fun provideMediaRepository(repository: MediaRepositoryImpl): MediaRepository = repository

    @Provides
    @Singleton
    fun provideRegisterRepository(repository: RegisterRepositoryImpl): RegisterRepository = repository

    @Provides
    @Singleton
    fun provideScoreRepository(score: ScoreRepositoryImpl): ScoreRepository = score

    @Provides
    @Singleton
    fun provideSignatureRepository(sign: SignatureRepositoryImpl): SignatureRepository = sign

    @Provides
    @Singleton
    fun provideStatementRepository(statement: StatementRepositoryImpl): StatementRepository = statement

    @Provides
    @Singleton
    fun provideConnectivity(connectivity: ConnectivityImpl): Connectivity = connectivity
}