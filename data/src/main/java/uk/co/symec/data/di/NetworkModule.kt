package uk.co.symec.data.di

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import uk.co.symec.data.BuildConfig
import uk.co.symec.data.apiservice.ConnectionService
import uk.co.symec.data.apiservice.NetworkConnectionInterceptor
import uk.co.symec.data.utils.Constants
import uk.co.symec.data.utils.FileSetContainer
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private val downloadList = mutableListOf<FileSetContainer>()

    @Provides
    fun provideBaseUrl() = BuildConfig.URL

    @Singleton
    @Provides
    fun provideOkHttpClient(@ApplicationContext context: Context): OkHttpClient =
        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(NetworkConnectionInterceptor(context))
                .build()
        } else {
            OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(NetworkConnectionInterceptor(context))
                .build()
        }

    @Singleton
    val moshi: Moshi = Moshi.Builder()
        .addLast(KotlinJsonAdapterFactory())
        .add(Date::class.java, Rfc3339DateJsonAdapter())
        .build()

    @Singleton
    fun getToken(@ApplicationContext context: Context): String? {
        return provideSharedPreferences(context).getString(Constants.TOKEN, "")
    }

    @Singleton
    fun getAssetId(@ApplicationContext context: Context): String? {
        return provideSharedPreferences(context).getString(Constants.ASSET_ID, "")
    }

    @Singleton
    fun getGroupId(@ApplicationContext context: Context): String? {
        return provideSharedPreferences(context).getString(Constants.GROUP_ID, "")
    }

    @Singleton
    fun getGroupName(@ApplicationContext context: Context): String? {
        return provideSharedPreferences(context).getString(Constants.GROUP_NAME, "")
    }

    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ConnectionService = retrofit.create(ConnectionService::class.java)

    @Singleton
    fun getDownloadsFolder(@ApplicationContext context: Context) = "${context.getExternalFilesDir("sharable")}/downloads/"

    @Singleton
    fun getDownloadsSDFolder(@ApplicationContext context: Context) = "${context.getExternalFilesDirs("sharable")[1]}/downloads/"

    @Singleton
    fun getSignatureFolder(@ApplicationContext context: Context) = "${context.getExternalFilesDir("sharable")}/signature/"

    @Provides
    @Singleton
    fun getFileSetContainerList() = downloadList

    @Provides
    fun provideSharedPreferences(
        @ApplicationContext context: Context
    ): SharedPreferences {
        return EncryptedSharedPreferences.create(
            context,
            Constants.GLOBAL,
            MasterKey.Builder(context)
                .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                .build(),
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }
}